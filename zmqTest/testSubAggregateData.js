var zmq = require('zmq'),
  config = require('../config/config'),
  publishDataConfig = require('../config/publishDataConfig');

function startSubscribeing(portName) {
  var zmqSocketsubscibe = zmq.socket('sub');
  zmqSocketsubscibe.subscribe(publishDataConfig.config.aggregateData.pubSubFilter);
  var zmqPortPart = 'tcp://' + config.zmq.sendHost + ':' //+config.zmq.port
  zmqSocketsubscibe.connect(zmqPortPart + config.zmq[portName], function (err) {
    if (err)console.log(err.stack)
  });
  console.log(zmqPortPart + config.zmq[portName])
  zmqSocketsubscibe.on("message", function (topic, message) {
    console.log(message.toString());
  })
}

startSubscribeing("vehicleWholeZoneAggregationData");

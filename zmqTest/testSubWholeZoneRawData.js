/**
 * Created by zendynamix on 03-06-2016.
 */

var zmq = require('zmq'),
  config = require('../config/config'),
  publishDataConfig = require('../config/publishDataConfig').config;

function  startSubscribeing(portName) {

  var zmqSocketsubscibe = zmq.socket('sub');
  zmqSocketsubscibe.subscribe(publishDataConfig.rawData.pubSubFilter.wholeZoneAnalyticsRawData);
  var zmqURI = 'tcp://' + config.zmq.sendHost + ':'+ publishDataConfig.rawData.zmqPubPort
  zmqSocketsubscibe.connect(zmqURI, function (err) {
    if (err)console.log(err.stack)
  });

  zmqSocketsubscibe.on("message", function (topic,message) {
    console.log("*****")
    console.log(message.toString());
    console.log("*****")
  })
}

startSubscribeing("vehicleWholeZoneAggregationData");

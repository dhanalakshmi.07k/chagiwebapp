prerequisites
mongodb      :      v3.0.3
nodejs       :      v 4.3.2
zmq          :      2.14.0

Instructions to run the application
1: Navigate to the folder where application is present and type the command
 npm install
2: Please check  config.js which is in config folder of the   application to change the configuration of  following :
* Database where the application is pointing to
* ZMQ port numbers
3: To create a new environment if needed for the application, add a new entry of the configuration in config.js  as shown below 
 EnvironmentName: {
 root: rootPath,
 app: {
 name: name of the application // eg:'changi'
      },
port: port number where the application is 	           running,//eg:3000
db: mongodb url where the application pioints to//eg:'mongodb://Test:test@ds011268.mongolab.com:11268/smrt'
zmq:{
sendHost:Ip address of the server,//eg:"212.47.249.75",
recHost:"*",
examplePortName1: ZMQ port number of examplePortName1 ,//eg:port:'3001'
examplePortName2:ZMQ port number of examplePortName2 ,//eg:portNotification:'3002'
 }
},
};


sample Configuration:
integration: {
  root: rootPath,
  app: {
    name: 'changi'
  },
  port: 3000,
  db: 'mongodb://changIntigration:changIntigration@ds037175.mongolab.com:37175/changi',
  zmq:{
     sendHost:"212.47.249.75",
    recHost:"*",
     port:'3001',
    portNotification:'3004',
    portVehicleAnalyticsByLane:'3002',
    portVehicleWholeZoneAnalytics:'3003'
  }



4: Navigate to the folder app ->generator in the application and run the following generators by type the command :
As per the sample above 
NODE_ENV="EnvironmentName" node terminalZonesLanesGenerator.js
which will provide the settings requried for terminal lane zone mapping 
NODE_ENV="EnvironmentName" node cameraMarkerPlotsGenerator.js 
which will provide the plots requried for plotting the markers on map
NODE_ENV="EnvironmentName"  node zoneSegmentInLaneGenerator.js
which will provide the plots requried for showing the notification on the map


5:Finally to run the application navigate to the application folder and type the command
NODE_ENV=EnvironmentNamenode app.js
As per the sampel above 
NODE_ENV=integration node app.js









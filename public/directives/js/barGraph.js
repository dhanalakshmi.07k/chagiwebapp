/**
 * Created by MohammedSaleem on 22/12/15.
 */


(function () {

    var areaGraph= angular.module('columnChart',[]);

    var link= function (scope, element) {
        $(element).find(".columnChart").highcharts({
            chart: {
                type: 'column',
                marginTop: 60
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: scope.x,
                labels: {
                    style: {
                        color: '#595959',
                        fontFamily: 'robotolight'
                    }
                },
                tickLength:0,
                title: {
                    text: 'Time',
                    align: 'high'
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Speed',
                    align: 'low'
                },
                labels: {
                    style: {
                        color: '#9a9a9a',
                        fontFamily: 'robotolight'
                    }
                }
            },
            tooltip: {
                enabled: false
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    borderWidth: 0,
                    events: {
                        legendItemClick:function(){
                            var name = this.name,
                                series = this.chart.series;

                            $.each(series, function(i,serie){
                                if(serie.name == name) {
                                    if(serie.visible)
                                        serie.hide();
                                    else
                                        serie.show();
                                }
                            });

                            return false;
                        }
                    }
                },
                series: {

                }
            },
            series: scope.y,
            credits: {
                enabled: false
            },

            legend: {
                align: 'center',
                verticalAlign: 'top',
                layout: 'horizontal',
                //x: 0,
                //y: -20,
                //itemMarginBottom: 10,
                itemStyle: {
                    color: '#595959',
                    fontFamily: 'robotoregular'
                }
            },
            colors: scope.color
        });
    };

    areaGraph.directive('columnChart', function () {
        return {
            templateUrl: 'directives/templates/barGraph.html',
            link: link,
            scope:{
                x:"=",
                y:"=",
                color:"="
            }
        }
    });


})();

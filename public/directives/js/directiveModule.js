/**
 * Created by rranjan on 11/13/15.
 */
(function () {

    var sliderModule = angular.module('directiveModule', []);

    //..... side nav directive......
    sliderModule.directive('sideNav', function () {
        return {
            replace: true,
            templateUrl: 'directives/templates/sideNavigation.html'
        }
    });

    //..... notification directive.....
    sliderModule.directive('notification', function () {
        return {
            replace: true,
            templateUrl: 'directives/templates/notifications.html'
        }
    });

    //..... notification directive.....

    var map= function (scope, element, attribute) {

       var laneMapInit=function() {
            var mapLatLng = {lat: 21.172642, lng: 72.832447};
            var mapOptions = {
                center: mapLatLng,
                scrollwheel: false,
                zoom: 12
            };
            var addressMap = new google.maps.Map(document.getElementById('map'), mapOptions);
        };
       laneMapInit();
    };

    sliderModule.directive('areaMap', function () {
        return {
            replace: true,
            templateUrl: 'directives/templates/areaMap.html',
            link: map
        }
    });

    //...... time slider directive ........
    sliderModule.directive("timeSlider", function () {

        var link = function (scope, element, attribute) {

            //..... default settings.....
            var width = scope.width;
            var slider = $(element).find(".slider");
            var sliderWidth = slider.width();
            var container = slider.find(".sliderData");

            var label,labelWidth,totalLabels,containerWidth,maxSlideVal;

            var construct= function(){
                for(i=1;i<=24;i++){
                    $('<div class="label">'+i+'</div>').appendTo(container);
                }
            };


            var sliderInitializer= function () {
                label = container.find(".label");
                labelWidth = label.first().width();
                totalLabels = container.find(".label").length;

                containerWidth = labelWidth * totalLabels;
                maxSlideVal = containerWidth - sliderWidth;
                container.css({
                    width: containerWidth
                });
                container.draggable({
                    axis: "x",
                    stop: function (event, ui) {
                        container = $(this);
                        var leftVal = container.position().left;
                        if (leftVal > 0) {
                            container.animate({
                                left: 0
                            }, 400)
                        }
                        else if (leftVal < -maxSlideVal) {
                            container.animate({
                                left: -maxSlideVal
                            }, 400)
                        }
                    }
                });
            };

            //sliderInitializer();



            //...... settings based on slider type 'range or select or hour'......

            var sliderType=(scope.sliderType).trim();
            if(sliderType){
                if(sliderType=='range'){
                    $('<div class="selector"></div>').prependTo(container);

                    var firstLabel;
                    var lastLabel;
                    var selector = container.find(".selector");

                    selector.css({
                        width: labelWidth * width,
                        left: labelWidth
                    });


                    var showSelection = function () {
                        var selectorLeftVal = Math.round(selector.position().left);
                        var selectorWidth = selector.outerWidth();

                        firstLabel = selectorLeftVal / labelWidth;
                        lastLabel = (selectorLeftVal + selectorWidth) / labelWidth;

                        label.removeClass("selected");

                        for (var i = firstLabel; i < lastLabel; i++) {
                            label.eq(i).addClass("selected");
                        }
                        scope.rangeStart = firstLabel;
                        scope.rangeEnd = lastLabel;
                    };

                    var resizing = function () {
                        selector.resizable({
                            handles: "e, w",
                            distance: 5,
                            grid: [labelWidth, 0],
                            containment: "parent",
                            resize: function (event, ui) {
                                showSelection();
                                scope.$apply();
                                if (scope.callbackOnSelection()) {
                                    scope.callbackOnSelection()(scope.rangeStart, scope.rangeEnd)
                                }
                            }
                        });
                    };
                    showSelection();
                    resizing();
                }

                else if(sliderType=="hour"){
                    $(element).find(".dateSlider").addClass("timer");
                    var indicator='<div class="hourHolder roboRegular">'+
                                        '<div class="timeBox smallest">'+
                                            '<div class="midBlock hour">09:15</div>'+
                                            '<div class="midBlock now">Now</div>'+
                                        '</div>'+
                                        '<div class="holder"></div>'+
                                    '</div>';


                    construct();
                    sliderInitializer();
                    $(indicator).appendTo(container);
                    $('<span class="clear"></span>').appendTo(container);
                }

                else if(sliderType=="select"){

                    construct();
                    sliderInitializer();

                    var activeIndex=0;
                    container.find(".label").eq(activeIndex).addClass("active");

                    container.find(".label").click(function () {
                        container.find(".label").removeClass("active");
                        $(this).addClass("active");

                        activeIndex=$(this).index();
                        scope.activeDate= activeIndex;

                        if (scope.callbackOnSelection()) {
                            scope.callbackOnSelection()(scope.activeDate)
                        }
                    });
                }
            }
            else{
                console.log("slider type is not defined");
            }


            //........ add Slider navigation ex: navigation= 'true or false' .......

            var navigation=scope.navigation;
            if(scope.navigation){
                if(navigation=='true'){
                    var navEle='<div class="arrow leftArrow">'+
                        '<img src="images/leftArrowGrey.png" alt="">'+
                        '</div>'+
                        '<div class="arrow rightArrow">'+
                        '<img src="images/rightArrowGrey.png" alt="">'+
                        '</div>';


                    $(element).addClass("navigate")
                        .find(".dateSlider").append(navEle);
                }

            }
        };
        return {
            templateUrl: "directives/templates/timeSliderTemplate.html",
            scope: {
                width: '@',
                sliderType:'@',
                rangeStart: '=',
                rangeEnd: '=',
                callbackOnSelection: '&',
                activeDate:'=',
                navigation:'@',
                arr:"="
            },
            link: link
        }
    })

})();
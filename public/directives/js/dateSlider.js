/**
 * Created by MohammedSaleem on 20/11/15.
 */

(function () {

    var appSlider= angular.module('appSlider',[]);


    appSlider.directive("dateSlider", function () {

        var link = function (scope, element, attribute) {

            //..... default settings.....
            var width = scope.width;
            var slider = $(element).find(".slider");
            var container = slider.find(".sliderData");

            var currentYear= moment().year();
            var currentMonth= moment().month();
            var currentDay=moment().date();
            var label,labelWidth,totalLabels,sliderWidth,containerWidth,maxSlideVal;


            var sliderDropDown= scope.dropDown;

            if (sliderDropDown){
                if(sliderDropDown=='true'){
                    $(element).find(".dateSliderMain").addClass("dependent");
                    var dateDropDown='<div class="dropDown midBlock small year">'+
                        '<div class="head" data-init="0">'+
                        '<div class="headTitle">'+currentYear+'</div>'+
                        '<img src="directives/images/DownArrow.png" alt="down" class="sign">'+
                        '</div>'+
                        '<ul class="list">'+
                        '<li>'+(currentYear-1)+'</li>'+
                        '<li>'+(currentYear)+'</li>'+
                        '<li>'+(currentYear+1)+'</li>'+
                        '</ul>'+
                        '</div>'+

                        '<div class="dropDown midBlock small month">'+
                        '<div class="head" data-init="0">'+
                        '<div class="headTitle">'+moment().month(currentMonth).format("MMM")+'</div>'+
                        '<img src="directives/images/DownArrow.png" alt="down" class="sign">'+
                        '</div>'+
                        '<ul class="list">'+
                        '<li>Jan</li>'+
                        '<li>Feb</li>'+
                        '<li>Mar</li>'+
                        '<li>Apr</li>'+
                        '<li>May</li>'+
                        '<li>Jun</li>'+
                        '<li>Jul</li>'+
                        '<li>Aug</li>'+
                        '<li>Sep</li>'+
                        '<li>Oct</li>'+
                        '<li>Nov</li>'+
                        '<li>Dec</li>'+
                        '</ul>'+
                        '</div>';

                    $(dateDropDown).prependTo($(element).find(".dateSliderMain"));
                }
            }

            var calendar= scope.calendar;

            if (calendar){
                $(element).find(".dateSliderMain").addClass("calendarSlider");
                var calendarBox='<div class="calendarBox">' +
                    '<input type="text" class="datepicker roboRegular small"></div>';


                $(calendarBox).prependTo($(element).find(".dateSliderMain"));

                $(element).find(".dateSliderMain .calendarBox .datepicker").datepicker({
                    dateFormat: "dd-mm-yy",
                    dayNamesMin: [ "S", "M", "T", "W", "T", "F", "S" ]
                    //beforeShow: function(input, inst) {
                    //    alert("test "+$('#ui-datepicker-div').css("left"))
                    //    $('#ui-datepicker-div').appendTo($(this).parent())
                    //        .css({
                    //            background:"red",
                    //            marginTop:"0",
                    //            left:"200px"
                    //        });
                    //}
                });
                $(element).find(".dateSliderMain .calendarBox .datepicker").datepicker("setDate", new Date());
            }


            var constructDay_DropDown= function () {
                var addDays= function () {
                    var year=$(element).find(".dropDown.year .headTitle").text().trim();
                    var month=$(element).find(".dropDown.month .headTitle").text().trim();
                    var numberOfDays=moment(year+"-"+month, "YYYY-MMM").daysInMonth();

                    month=""; // to remove the month from date label

                    for(i=1;i<=numberOfDays;i++){
                        if(i<10){
                            i="0"+i;
                        }
                        $('<div class="label">'+i+" "+month+'</div>').appendTo(container);
                    }
                    $('<span class="clear"></span>').appendTo(container);
                };
                addDays();

                $(element).find(".dropDown .list li").click(function () {
                    setTimeout(function () {
                        container.html("");
                        typeBaseFunctionality();
                        sliderInitializer();
                    },50);
                });
            };




            var constructHour= function(){
                for(i=1;i<=24;i++){
                    $('<div class="label">'+i+'</div>').appendTo(container);
                }
                $('<span class="clear"></span>').appendTo(container);


                //...... get time.....
                var date = new Date();
                var hour = date.getHours();

                label = container.find(".label");
                labelWidth = label.first().width();
                sliderWidth = slider.width();
                totalLabels = label.length;
                containerWidth = labelWidth * totalLabels;

                var remainingHours=totalLabels-hour;
                var remainingSliderWidth=remainingHours*labelWidth;

                var timeBox= container.find(".timeBox");

                setInterval(function(){
                    var date = new Date();
                    var minute= date.getMinutes();

                    if(minute<10){
                        minute="0"+minute;
                    }
                    if(remainingSliderWidth>sliderWidth){
                        container.animate({
                            left:-(labelWidth*(hour-1))
                        },500)
                    }
                    else{
                        container.animate({
                            left:-(containerWidth-sliderWidth)
                        },500)
                    }

                    container.find(".timeBox").animate({
                        left: ((date.getHours()-1)*labelWidth)
                    },500)
                        .find(".hour").text(date.getHours()+":"+minute);
                },1000);

            };



            var sliderInitializer= function () {
                label = container.find(".label");
                labelWidth = label.first().width();
                totalLabels = container.find(".label").length;
                sliderWidth = slider.width();

                containerWidth = labelWidth * totalLabels;



                container.css({
                    width: containerWidth
                });
                var minSlideVal;
                container.draggable({
                    axis: "x",
                    stop: function (event, ui) {
                        container = $(this);
                        minSlideVal = container.position().left;

                        sliderWidth = slider.width();

                        containerWidth = labelWidth * totalLabels;
                        maxSlideVal = containerWidth - sliderWidth;


                        if (minSlideVal > 0) {
                            container.animate({
                                left: 0
                            }, 400)
                        }
                        else if (minSlideVal < (-maxSlideVal)) {
                            container.animate({
                                left: -maxSlideVal
                            }, 400)
                        }
                    }
                });
            };




            //........ add Slider navigation ex: navigation= 'true or false' .......

            var navigation=scope.navigation;
            if(scope.navigation){
                if(navigation=='true'){
                    var navEle='<div class="arrow leftArrow">'+
                        '<img src="directives/images/LeftNav.png" alt="">'+
                        '</div>'+
                        '<div class="arrow rightArrow">'+
                        '<img src="directives/images/RightNav.png" alt="">'+
                        '</div>';


                    $(element).addClass("navigate")
                        .find(".dateSlider").append(navEle);
                }

            }

            //...... settings based on slider type 'range or select or hour'......


            var typeBaseFunctionality= function () {
                var sliderType=(scope.sliderType).trim();
                if(sliderType){
                    if(sliderType=='range'){
                        constructDay_DropDown();
                        sliderInitializer();



                        $('<div class="selector"></div>').prependTo(container);

                        var firstLabel;
                        var lastLabel;
                        var selector = container.find(".selector");

                        selector.css({
                            width: labelWidth * width,
                            left: labelWidth
                        });


                        var showSelection = function () {
                            var selectorLeftVal = Math.round(selector.position().left);
                            var selectorWidth = selector.outerWidth();

                            firstLabel = selectorLeftVal / labelWidth;
                            lastLabel = (selectorLeftVal + selectorWidth) / labelWidth;

                            label.removeClass("selected");

                            for (var i = firstLabel; i < lastLabel; i++) {
                                label.eq(i).addClass("selected");
                            }
                            scope.rangeStart = firstLabel;
                            scope.rangeEnd = lastLabel;
                            var year=$(element).find(".dropDown.year .headTitle").text().trim();
                            var month=$(element).find(".dropDown.month .headTitle").text().trim();
                            var startDate = moment().month(month).date(firstLabel).hours(0).minutes(0).seconds(0).milliseconds(0).format();
                            var endDate = moment().month(month).date(lastLabel).hours(0).minutes(0).seconds(0).milliseconds(0).format();
                            scope.startDate=startDate;
                            scope.endDate=endDate;
                        };

                        var resizing = function () {
                            selector.resizable({
                                handles: "e, w",
                                distance: 5,
                                grid: [labelWidth, 0],
                                containment: "parent",
                                resize: function (event, ui) {
                                    showSelection();
                                    scope.$apply();
                                    if (scope.callbackOnSelection()) {
                                        scope.callbackOnSelection()(scope.rangeStart, scope.rangeEnd,scope.startDate,scope.endDate)
                                    }
                                }
                            });
                        };
                        showSelection();
                        resizing();
                    }



                    else if(sliderType=="hour"){
                        $(element).find(".dateSlider").addClass("timer");
                        var indicator='<div class="timeBox small">'+
                            '<div class="midBlock hour">07:15</div>'+
                            '</div>';


                        constructHour();
                        sliderInitializer();
                        $(indicator).appendTo(container);
                        $('<span class="clear"></span>').appendTo(container);
                    }



                    else if(sliderType=="rangeHour"){
                        constructHour();
                        sliderInitializer();


                        $('<div class="selector"></div>').prependTo(container);

                        var firstLabel;
                        var lastLabel;
                        var selector = container.find(".selector");

                        selector.css({
                            width: labelWidth * width,
                            left: labelWidth
                        });


                        var showSelection = function () {
                            var selectorLeftVal = Math.round(selector.position().left);
                            var selectorWidth = selector.outerWidth();

                            firstLabel = selectorLeftVal / labelWidth;
                            lastLabel = (selectorLeftVal + selectorWidth) / labelWidth;

                            label.removeClass("selected");

                            for (var i = firstLabel; i < lastLabel; i++) {
                                label.eq(i).addClass("selected");
                            }
                            scope.rangeStart = firstLabel;
                            scope.rangeEnd = lastLabel;
                        };

                        var resizing = function () {
                            selector.resizable({
                                handles: "e, w",
                                distance: 5,
                                grid: [labelWidth, 0],
                                containment: "parent",
                                resize: function (event, ui) {
                                    showSelection();
                                    scope.$apply();
                                    if (scope.callbackOnSelection()) {
                                        scope.callbackOnSelection()(scope.rangeStart, scope.rangeEnd)
                                    }
                                }
                            });
                        };
                        showSelection();
                        resizing();
                    }

                    else if(sliderType=="select"){

                        constructDay_DropDown();
                        sliderInitializer();


                        var numberOfDays=moment(currentYear+"-"+currentMonth, "YYYY-MM").daysInMonth();
                        var totalVisibleLabels=Math.round(sliderWidth/labelWidth);
                        var remainLabels=numberOfDays-currentDay;


                        console.log("total visi "+ totalVisibleLabels);
                        console.log("total remainig "+ remainLabels);

                        if(totalVisibleLabels>remainLabels){
                            container.animate({
                                left:-(containerWidth-sliderWidth)
                            })
                        }
                        else{
                            container.animate({
                                left:-((currentDay-1)*labelWidth)
                            })
                        }
                        var activeIndex=0;
                        container.find(".label").eq(currentDay-1).addClass("active");


                        container.find(".label").click(function () {
                            container.find(".label").removeClass("active");
                            $(this).addClass("active");

                            activeIndex=$(this).index();
                            scope.activeDate= activeIndex;
                            var year=$(element).find(".dropDown.year .headTitle").text().trim();
                            var month=$(element).find(".dropDown.month .headTitle").text().trim();
                            var startDate = moment().month(month).year(year).date(activeIndex+1).hours(0).minutes(0).seconds(0).milliseconds(0).format();
                            var currDate = new Date();/*
                             console.log(startDate)*/currDate.setFullYear(year);
                            currDate.setDate(activeIndex+1);
                            currDate.setMonth(11);
                            /*
                             currDate.setMonth(month);
                             currDate.setHours(0);*/
                            //var endDate = moment().month(month).date(lastLabel).hours(0).minutes(0).seconds(0).milliseconds(0).format();
                            scope.startDate=startDate;
                            //scope.endDate=endDate;
                            if (scope.callbackOnSelection()) {
                                scope.callbackOnSelection()(startDate)
                            }
                        });
                    }
                }
                else{
                    console.log("slider type is not defined");
                }
            };

            typeBaseFunctionality();

        };
        return {
            templateUrl: "directives/templates/timeSliderTemplate.html",
            scope: {
                width: '@',
                sliderType:'@',
                rangeStart: '=',
                rangeEnd: '=',
                callbackOnSelection: '&',
                activeDate:'=',
                navigation:'@',
                dropDown:'@',
                calendar:'@',
                arr:"=",
                startDate:"=",
                endDate:"="
            },
            link: link
        }
    })

})();


/**
 * Created by MohammedSaleem on 20/11/15.
 */

(function () {

    var verticalNav= angular.module('verticalNav',[]);

    var link= function (scope,element) {
        var tab=$(element).find(".sideNav li");
        tab.on("click",function(){
            tab.removeClass("active");
            $(this).addClass("active");
        });
    };

    verticalNav.directive('sideNav', function () {
        return {
            replace: true,
            templateUrl: 'directives/templates/sideNavigation.html'
        }
    });

})();

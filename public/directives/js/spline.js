/**
 * Created by MohammedSaleem on 21/11/15.
 */

(function () {

    var areaGraph= angular.module('spline',[]);

    var link= function (scope, element, attribute) {

        var  marginTop=20;
        var start=0;
        if(scope.ylabel){
            start=1;
            if(scope.legend){
                marginTop=70;
            }
        }


        $(element).find(".spline").highcharts({
            chart: {
                type: 'spline',
                marginTop: marginTop
            },
            legend: {
                enabled: scope.legend,
                align: 'left',
                verticalAlign: 'top',
                layout: 'horizontal',
                itemStyle: {
                    color: '#595959',
                    fontFamily: 'robotoregular'
                },
                symbolHeight: 12,
                symbolWidth: 12,
                symbolRadius: 6
            },
            title: {
                text: ''
            },

            xAxis: {
                //categories: ['26 Oct','27 Oct','28 Oct','29 Oct','30 Oct'],
                categories: scope.x,
                title: {
                    text: '',
                    align: 'high'
                },
                gridLineWidth: scope.grid,
                gridLineColor: '#f0f0f0',
                min: start
            },
            yAxis: {
                title: {
                    text: '',
                    align: 'low'
                },
                //floor: 0,
                //ceiling: 100,
                //gridLineDashStyle: 'Dot',
                gridLineWidth: scope.grid,
                gridLineColor: '#f0f0f0'
                //labels: {
                //    formatter: function () {
                //        return this.value + ' %';
                //    }
                //}
            },
            tooltip: {
                shared: true,
                useHTML: true,
                headerFormat: '<table>',
                pointFormat: '<tr><td style="color: {series.color}">{series.name}  </td>' +
                '<td style="text-align: right">Time: <b>{point.x} hour, </b></td><td style="text-align: right">Speed: <b>{point.y} km/hr</b></td></tr>',
                footerFormat: '</table>',
                valueDecimals: 2
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                areaspline: {
                    fillOpacity: 0.2
                },
                series: {
                    lineWidth: 2,
                    marker: {
                        fillColor: '#FFFFFF',
                        lineWidth: 1,
                        lineColor: '#525b62',
                        symbol: 'circle',
                        enabled: false
                    },
                    states: {
                        hover: {
                            enabled: false
                        }
                    }
                }

            },
            series: scope.y,
            //series:{

            //},

            colors: ['#81de97', '#87aff5', '#f9b343', '#e87294', '#5bc3c2']
        });
    };

    areaGraph.directive('splineGraph', function () {
        return {
            templateUrl: 'directives/templates/spline.html',
            link: link,
            scope:{
                x:"=",
                y:"=",
                legend:"=",
                xlabel:"=",
                ylabel:"=",
                grid:"="
            }
        }
    });


})();

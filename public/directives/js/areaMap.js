/**
 * Created by MohammedSaleem on 20/11/15.
 */

(function () {

    var appMap= angular.module('appMap',[]);

    var link= function (scope, element, attribute) {

        var suratMapInit=function() {
            var mapLatLng = {lat: 21.172642, lng: 72.832447};
            var mapOptions = {
                center: mapLatLng,
                scrollwheel: false,
                zoom: 12
            };
            var addressMap = new google.maps.Map(document.getElementById('map'), mapOptions);
        };
        suratMapInit();
    };

    appMap.directive('areaMap', function () {
        return {
            templateUrl: 'directives/templates/map.html',
            link: link,
            scope:{
                tabs:"="
            }
        }
    });


})();


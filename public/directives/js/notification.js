/**
 * Created by rranjan on 11/15/15.
 */
(function() {

    var notificationModule = angular.module("notificationModule", [])

    notificationModule.directive("notifications", function () {

        var link = function(scope,element,atrrs){
            console.log(scope.messagesDataModel)
        }
        return {
            //require:"?ngModel",
            templateUrl: "directives/templates/notifications.html",
            scope: {
                messagesDataModel: "=",
                clearMe:"=",
                notify:"=",
            },
            link: link

        }
    })

})()

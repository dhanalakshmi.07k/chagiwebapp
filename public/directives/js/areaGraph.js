/**
 * Created by MohammedSaleem on 21/11/15.
 */

(function () {

    var areaGraph= angular.module('areaGraph',[]);

    var link= function (scope, element, attribute) {


        //if(scope.xlabel){
        //
        //    alert(!scope.xlabel);
        //}
        //else{
        //    scope.xlabel=true;
        //}
        //
        //if(scope.ylabel){
        //}
        //else{
        //    scope.ylabel=true;
        //}


        var  marginTop=10;
        var start=0;
        if(scope.ylabel){
            start=1;
            if(scope.legend){
                marginTop=70;
            }
        }

        if(scope.grid!=0){
        }


        $(element).find(".areaSpline").highcharts({
            chart: {
                type: 'area',
                //marginRight: 0
                marginTop: marginTop
            },
            legend: {
                enabled: scope.legend,
                align: 'left',
                verticalAlign: 'top',
                layout: 'horizontal',
                itemStyle: {
                    color: '#595959',
                    fontFamily: 'robotoregular'
                },
                color: 'red',
                symbolHeight: 12,
                symbolWidth: 12,
                symbolRadius: 6
                //itemHiddenStyle: {
                //    color: 'green'
                //}
            },
            title: {
                text: ''
            },

            xAxis: {
                //categories: ['26 Oct','27 Oct','28 Oct','29 Oct','30 Oct'],
                categories: scope.x,
                title: {
                    text: 'Time',
                    align: 'high'
                },
                gridLineWidth: scope.grid,
                gridLineColor: '#f0f0f0',
                tickLength: 0,/*
                min: start,*/
                labels:
                {
                    enabled: scope.xlabel
                }
            },
            yAxis: {
                title: {
                    text: 'Speed (km/hr)',
                    align: 'high'
                },
                //floor: 0,
                //ceiling: 100,
                //gridLineDashStyle: 'Dot',
                gridLineWidth: scope.grid,
                gridLineColor: '#f0f0f0',
                //labels: {
                //    formatter: function () {
                //        return this.value + ' %';
                //    }
                //}
                 startOnTick: true,
                labels:
                {
                    enabled: scope.ylabel
                },
                tickWidth: 0
            },
            tooltip: {
                shared: false,
                valueSuffix: '',
                formatter: function() {
                    var seriesTitle=this.point.series.yAxis.axisTitle.textStr;
                    var scale="";
                    if(seriesTitle=="Speed (km/hr)"){/*Speed(km/hr)*/
                        scale="km/hr"
                    }
                    else if(seriesTitle=="Flow"){/*Flow(vehicles/hr)*/
                        scale="vehicles/hour"
                    }
                    else if(seriesTitle=="Time Headway (sec)"){/*Time Headway(seconds)*/
                        scale="seconds"
                    }
                    else if(seriesTitle=="Space Headway (mtr)"){/*Space Headway(meters)*/
                        scale="meters"
                    }
                    else if(seriesTitle=="Occupancy (%)"){
                        scale="%"
                    }else if(seriesTitle=="Volume (vehicles/hr)"){/*Volume(vehicles/hr)*/
                      scale="vehicles/hour"
                    }

                   /* var actualTime="";
                    if(this.point.actualTime==null|| this.point.actualTime=="undefined"){
                        actualTime=this.key;
                    }
                    else{
                        actualTime= this.point.actualTime;
                    }*/

                    return /*'Time: <b>' + actualTime + ' Hour, </b> <br> '+*/this.point.series.yAxis.axisTitle.textStr+': <b>' + this.y + ' '+ scale +'</b>';
                }
            },

            credits: {
                enabled: false
            },
            plotOptions: {
              //  areaspline: {
              //  fillOpacity: 0.2
              //},
              area: {
                fillOpacity: 0.07,
                  pointStart: 0
              },
                series: {
                    lineWidth: 2,
                    marker: {
                        /*fillColor: '#FFFFFF',
                        lineWidth: 1,
                        lineColor: '#525b62',*/
                        symbol: 'circle',
                        enabled: true
                    },
                    states: {
                        hover: {
                            enabled: false
                        }
                    }
                }

            },
            series: scope.y,
            //series:{

            //},

            colors: ['#81de97', '#87aff5', '#f9b343', '#e87294', '#5bc3c2']
        });
    };

    areaGraph.directive('areaSpline', function () {
        return {
            templateUrl: 'directives/templates/areaGraph.html',
            link: link,
            scope:{
                x:"=",
                y:"=",
                legend:"=",
                xlabel:"=",
                ylabel:"=",
                grid:"="
            }
        }
    });


})();

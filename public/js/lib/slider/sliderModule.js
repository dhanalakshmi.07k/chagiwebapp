/**
 * Created by rranjan on 11/13/15.
 */
(function(){

var sliderModule= angular.module('sliderModule',[]);

sliderModule.directive("timeSlider",function(){

    var link = function(scope,element,attribute){

            var width = scope.width;
            var slider = $(element, ".selectTime").find(".rangeSlider");
            var sliderWidth = slider.width();
            var container = slider.find(".ContWrapper");
            var label = container.find(".label");
            var labelWidth = label.first().width();
            var totalLabels = container.find(".label").length;

            var containerWidth = labelWidth * totalLabels;
            var maxSlideVal = containerWidth - sliderWidth;

            var firstLabel;
            var lastLabel;

            container.css({
                width: containerWidth
            });

            var selector= container.find(".selector");

            selector.css({
                width: labelWidth*width,
                left: labelWidth
            });

            container.draggable({
                axis: "x",
                stop: function (event, ui) {
                    container = $(this);
                    var leftVal = container.position().left;
                    if (leftVal > 0) {
                        container.animate({
                            left: 0
                        }, 400)
                    }
                    else if (leftVal < -maxSlideVal) {
                        container.animate({
                            left: -maxSlideVal
                        }, 400)
                    }
                }
            });

            var showSelection =function (){
                var selectorLeftVal=Math.round(selector.position().left);
                var selectorWidth= selector.outerWidth();

                console.log("selectorLeftVal=%s selectorWidth=%s",selectorLeftVal,selectorWidth);

                firstLabel= selectorLeftVal/labelWidth;
                lastLabel=(selectorLeftVal+selectorWidth)/labelWidth;

                label.removeClass("selected");

                for(var i=firstLabel;i<lastLabel;i++){
                    label.eq(i).addClass("selected");
                }
                //console.log(" old scope.firstSelectedTime=%s scope.lastSelectedTime=%s",scope.firstSelectedTime,scope.lastSelectedTime)
                //console.log("firstLabel=%s lastLabel=%s",firstLabel,lastLabel)
                scope.firstSelectedTime = firstLabel;
                scope.lastSelectedTime = lastLabel;
                //console.log(" new scope.firstSelectedTime=%s scope.lastSelectedTime=%s",scope.firstSelectedTime,scope.lastSelectedTime)
            };

            var resizing = function (){
                selector.resizable({
                    handles: "e, w",
                    distance: 5,
                    grid: [ labelWidth, 0 ],
                    containment: "parent",
                    resize: function( event, ui ) {
                        showSelection();
                        scope.$apply();
                        if(scope.callbackOnSelection()){
                         scope.callbackOnSelection()(scope.firstSelectedTime,scope.lastSelectedTime)
                        }
                    }
                });
            };

            showSelection();
            resizing();
    };
    return{
        templateUrl:"js/lib/slider/timeSliderTemplet.html",
        scope:{
            width:'@',
            firstSelectedTime:'=',
            lastSelectedTime:'=',
            callbackOnSelection:'&'
        },
        link:link
    }
})

})()
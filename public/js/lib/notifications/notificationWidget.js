/**
 * Created by rranjan on 11/15/15.
 */
(function() {

    var notificationModule = angular.module("notificationModule", [])

    notificationModule.directive("notifications", function () {

        var link = function(scope,element,atrrs){
            console.log(scope.messagesDataModel)
        }
        return {
            //require:"?ngModel",
            templateUrl: "js/lib/notifications/notificationTemplate.html",
            scope: {
                messagesDataModel: "=",
            },
            link: link

        }
    })

})()
/**
 * Created by MohammedSaleem on 12/11/15.
 */

$(document).ready(function () {

    var parentEle=$("#appWrapper");
    fun={
        notificationList: function(){
            parentEle.on("click","#header .notificationAlert",function (e) {
                e.stopPropagation();
                var init= $(this).data("init");

                if(init=='0'){
                    $(".notifications").fadeIn(0, function () {
                        $(".notifications").removeClass("bounceOutRightTime").addClass("bounceInRightTime");


                    });
                    $(this).data({
                        'init':'1'
                    }).addClass("active");

                    setTimeout(function () {
                        $("#contWrapper .contentBox").animate({
                            paddingRight:"320px"
                        },300).addClass("compressed");
                    },600)
                }
                else{
                    $("#contWrapper .contentBox").animate({
                        paddingRight: "0"
                    },300).removeClass("compressed");

                    setTimeout(function () {
                        $(".notifications").removeClass("bounceInRightTime").addClass("bounceOutRightTime");
                    },300);
                    $(this).data({
                        'init':'0'
                    }).removeClass("active");

                    setTimeout(function () {
                        $(".notifications").fadeOut(0)
                    },800);
                }

            });



            parentEle.on("click",".notificationList li",function () {
                var init= $(this).data("init");

                if(init=='0'){
                    $(this).find(".actionBtn").slideDown(200);
                    $(this).data({
                        'init':'1'
                    });
                }
                else{
                    $(this).find(".actionBtn").slideUp(200);
                    $(this).data({
                        'init':'0'
                    })
                }
            });

            parentEle.on("click",".notificationList li .actionBtn .btn",function () {
                $(".notificationList li .actionBtn").slideUp(200);
                $(".notificationList li").data({
                    'init':'0'
                });
                $(this).parents().eq(2).addClass("notified");
            })

        },
        analyticsCam: function () {
            parentEle.on("click",".camPopup.laneCam .close",function (e) {
                var cam=$(".camPopup.laneCam");

                cam.fadeOut(300)
                //cam.removeClass("bounceInRight").addClass("bounceOutRight");
                //setTimeout(function () {
                //    cam.fadeOut(0)
                //},800);
            });
        },
        dropDown: function () {
            parentEle.on("click",".dropDown .head",function (e) {
                e.stopPropagation();
                var init= $(this).data("init");
                if(init=="0"){
                    $(this).parent().find(".list").slideDown(200);
                    $(this).data({
                        "init":"1"
                    })
                }
                else{
                    $(this).parent().find(".list").slideUp(200);
                    $(this).data({
                        "init":"0"
                    })
                }
            });
            parentEle.on("click",".dropDown .list li",function (e) {
                var value=$(this).text();
                $(this).parents().eq(1).find(".headTitle").text(value);
            })
        },
        cameraVideos: function () {
            var myVideo = document.getElementById("camVideo");
            parentEle.on("click",".cameraSec .cameras li",function (e) {
                $(".background").fadeIn(300, function () {
                    $(".camPopup").fadeIn(200);
                    //myVideo.pause();
                })
            });

            $(".camPopup .close,.background").click(function () {
                $(".camPopup").fadeOut(300, function () {
                    $(".background").fadeOut(200);
                })
            });
        },
        mapMenu: function () {
            $(".dropDown.lanes .head").click(function () {
                var init= $(this).data("init");
                if(init=="0"){
                    $(".mapMenu").slideDown(300);
                }
                else{
                    $(".mapMenu").slideUp(500);
                }
            });

            $(".mapMenu ul li").click(function () {
                var menuName= $(this).data("name");
                $(".mapMenu ul li").removeClass("active");
                $(this).addClass("active");

                $(".dropDown.lanes .head .headTitle").text(menuName);
            })
        },
        mapMinMax: function () {
            var minMax=0;
            parentEle.on("click",".laneMapBox .minMax",function (e) {
                var btn=$(".laneMapBox .minMax");
                if(minMax==0){
                    $(".laneMapBox #laneMap").animate({
                        height: "700px"
                    },400);
                    btn.find("img").attr({
                        src:"images/min.png"
                    });
                    minMax=1;
                }
                else{
                    $(".laneMapBox #laneMap").animate({
                        height: "400px"
                    },400);
                    minMax=0;
                    btn.find("img").attr({
                        src:"images/max.png"
                    })
                }
            });
        },
        zoneSelection: function () {
            parentEle.on("click",".statusBox label",function (e) {
                e.stopPropagation();
                $(".zoneStatus .statusMain").removeClass("active");
                $(".zoneStatus .checkbox input").prop({
                    checked: false
                });

                $(this).find(".checkbox input").prop({
                    checked: true
                });

                var ele= $(this).attr("id");
                //alert()
                $("."+ele).addClass("active");

                //var ele=$(this).attr("id");
                //var init=$(this).data("init");
                //
                //$(".zoneStatus .statusMain").removeClass("active");
                //if(init==0){
                //    $(this).addClass("active")
                //    .data({
                //        "init":1
                //    });
                //}
                //else{
                //    $(this).removeClass("active")
                //    .data({
                //        "init":0
                //    });
                //}

            });

            parentEle.on("click",".zoneStatus .statusMain",function (e) {
                e.stopPropagation();
                $(".zoneStatus .statusMain").removeClass("active");
                $(this).addClass("active");
                $(".zoneStatus .checkbox input").prop({
                    checked: false
                });
            });


            parentEle.on("click",".analytics",function (e) {
             /* $("#trigger").trigger("click");
                alert("hi")*/
              /*angular.element($(".analytics")).scope().fetchRealTimeDataForAllZoneAndLane();*/
                //zone select All
            });
        },
        popup: function (btn,popup) {
            parentEle.on("click",btn,function (e) {
                e.stopPropagation();
                $(".background").fadeIn(300, function () {
                    $(popup).fadeIn(300);
                });
            });

            parentEle.on("click",".popup .close",function (e) {
                e.stopPropagation();
                $(".popup").fadeOut(300, function () {
                    $(".background").fadeOut(300);
                });
            });
        },
        dataTypeSwap: function () {
          parentEle.on("click",".swapType .type.live", function () {
              $(".swapType .type").removeClass("active");
              $(this).addClass("active");
              $(this).parent().find(".activeBar").removeClass("predictive").addClass("live");
          });
          parentEle.on("click",".swapType .type.predictive", function () {
              $(".swapType .type").removeClass("active");
              $(this).addClass("active");
              $(this).parent().find(".activeBar").removeClass("live").addClass("predictive");
          });
        },
        saveSettings: function () {
            var userMsg,hideMsg;
            parentEle.on("click",".formBtns .btn.save", function () {
                //bounceInDownTime
                clearTimeout(userMsg);
                clearTimeout(hideMsg);
                $(".formMain .userMsg").fadeIn(0).removeClass("fadeOutUpTime").addClass("fadeInUpTime");

                userMsg= setTimeout(function () {
                    $(".formMain .userMsg").removeClass("fadeInUpTime").addClass("fadeOutUpTime");
                    var hideMsg=setTimeout(function () {
                        $(".formMain .userMsg").fadeOut(0)
                    },500);
                },3000)
            });
        },
        defaultClick: function(){
            $(document).click(function () {
                //$(".notifications").removeClass("bounceInRight").addClass("bounceOutRight");
                //$("#header .notificationAlert").data({
                //    'init':'0'
                //}).removeClass("active");
                //setTimeout(function () {
                //    $(".notifications").fadeOut(0)
                //},800);


                $(".dropDown .list").slideUp(200);
                $(".mapMenu").slideUp(200);
                $(".dropDown .head").data({
                    "init":"0"
                });

                $(".notificationList .actionBtn").slideUp(200);
                $(".notificationList li").data({
                    'init':'0'
                });

                $(".zoneStatus .statusMain").addClass("active");
                $(".zoneStatus .checkbox input").prop({
                    checked: true
                });

                $(".popup").fadeOut(300, function () {
                    $(".background").fadeOut(300);
                });

            })
        },
        preventDefaultClicks: function () {
            var selectors='.notificationList,.popup, .notificationList .actionBtn,.zoneStatus';

            parentEle.on("click",selectors,function (e) {
                e.stopImmediatePropagation()
            })
        }
    };
    fun.notificationList();
    fun.analyticsCam();
    fun.dropDown();
    fun.cameraVideos();
    fun.mapMenu();
    fun.mapMinMax();
    fun.zoneSelection();
    fun.dataTypeSwap();
    fun.saveSettings();
    fun.popup(".dataTable.usersTable li.delete .deleteBtn",".DeleteUserAccount.popup");
    fun.popup(".dataTable.usersTable li.edit .editBtn",".newUsers.popup");
    fun.popup(".newUserAccountBtn",".newUsers.popup");
    fun.defaultClick();
    fun.preventDefaultClicks();
});

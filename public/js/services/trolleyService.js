/**
 * Created by Suhas on 1/17/2016.
 */
changiServices.factory("trolleyService",function($http,$state,$http,$rootScope){
  var trolleyTestData={
    x:100,
    y:200
  };
  var latestTrolleyDetails =[];
  var trolleyUpdateRefreshTime = 5*60*1000;
  var trolleyPlots= [];

  /*socket.on("updatedTrolleyLocation", function (data) {
    trolleysNewCoordinates(data);
  })*/

  var trolleyEvents= {
    New_TROLLEY_LOCATION:"newTrolleyLocation"
  }
  var trolleySettings ={
    trolleyIntervalId:0,
    refreshPeriod:6000
  };

  var trolleysNewCoordinates= function(trolleyData){
    $rootScope.$emit(trolleyEvents.New_TROLLEY_LOCATION,trolleyData);
  }
  var trolleyDetails={
    "apiID":"csp_ichangi",
    "apiPwd":"a&&X^X;3<j2>Hbd",
    "action":"whereAmI",
    "macAddr":"2c:1f:23:43:43:a9"
  }
  var updateTrolleyRefreshTime = function(seconds){
    $http.get('/api/trolleyLocation/setRefreshTime'+seconds);
  }
  var getRefreshTimeOfTrolleyLocation = function(){
    return $http.get('/api/trolleyLocation/refreshTime');
  }
  return{
    /*startGettingTrolleyInfo:startGettingTrolleyInfo,*/
    setMacAddressOfTheTrolley:function(macAddress){
      trolleyDetails.macAddr = macAddress;

    },
    getMacAddressOfTheTrolley:function(){
    return trolleyDetails.macAddr;
  },
    trolleyEvents:trolleyEvents,
    getTrolleyData:function(){
      return trolleyTestData;
    },
    getTrolleyDataFOrFirstTime:function(){
      $http.get('/api/trolleyLocation');
      $http.get('/api/trolleyLocation/refreshTime').then(function(result){
        trolleyUpdateRefreshTime= result.data.refreshTime;
        console.log(trolleyUpdateRefreshTime)
      });
    },
   /* setTrolleyDetatils:function(trolleyDetails){

     trolleyPlots=trolleyDetails;
     },
     getTrolleyDetatils:function(){
     return trolleyPlots;
     },*/
    getLatestTrolleyDetails:latestTrolleyDetails,
    updateTrolleyRefreshTime:updateTrolleyRefreshTime,
    getUpdateRefreshTime:trolleyUpdateRefreshTime,
    getRefreshTimeToGetTrolleyLocation:getRefreshTimeOfTrolleyLocation,
    getPolygonDetails:function(){
      return $http.get('/api/trolley/polygon');
    }



  }

})

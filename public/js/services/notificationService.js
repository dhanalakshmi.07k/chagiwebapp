/**
 * Created by rranjan on 1/15/16.
 */
"use strict";
//
changiServices.factory("notificationService",function($http,$rootScope){
  var notificationEvents= {
    NEW_NOTIFICATION_ARRIVED:"newNotificationArrived"
  }
  var NOTIFICATION_DEPTH=50;
  var PREVIOUS_TROLLEY_COUNT=0;
  var PREVIOUS_THRESHOLD_VAL=0;

  var notifications=[];
  socket.on('notificationMsg', function (notification) {
    newNotification(notification);
    console.log("inside vehicle analytics notification")
  });
  socket.on('trolleyNotification', function(data) {
    if(PREVIOUS_TROLLEY_COUNT!=data.trolleyCount || PREVIOUS_THRESHOLD_VAL!=data.thershold){
      PREVIOUS_TROLLEY_COUNT=data.trolleyCount;
      PREVIOUS_THRESHOLD_VAL=data.thershold;
      newNotification(data);
      console.log("inside trolley notification")
    }
  });
  var newNotification= function(notification){
    /*console.log("new Notification Service ....")*/
    if(notification.notificationType=="STOPPED_VEHICLE"){
      /*console.log("STOPPED_VEHICLE")*/
      /*console.log(notification)*/
    }else{
      /*console.log("trolley")*/
    }
    if(notifications.length > NOTIFICATION_DEPTH){
      notifications.pop();
    }
    notifications.unshift(notification);
    $rootScope.$emit(notificationEvents.NEW_NOTIFICATION_ARRIVED, notification);
  };
  var getNotifications= function(){
    return notifications;
  };
  var clearNotification = function(){
    notifications=[]
    $rootScope.$emit("clearMapNotificationMarker", "clear");
  };
  var sendNotificationSms = function(msg){
    var data = {
      msg:msg
    }
    $http.post('/api/sendNotificationSms',data);
  }
  var toggleSmsNotificationNotifier = function(flag){
    $http.get('/api/toggleSmsNotificationNotifier/'+flag);
  }
var getToggleSMSNotifierStatus = function(){
  return $http.get('/api/getToggleSmsNotificationNotifier/flag');;
}
  return {
    notificationEvents:notificationEvents,
    getNotifications:getNotifications,
    clearNotification:clearNotification,
    sendSmsNotification:sendNotificationSms,
    toggleSmsNotificationNotifier:toggleSmsNotificationNotifier,
    getAUtoNotifierStatus:getToggleSMSNotifierStatus
  }

})

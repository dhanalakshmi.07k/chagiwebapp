/**
 * Created by zendynamix on 20-04-2016.
 */
changiControllers.factory("userService", function ($http) {
  var userCount;
  var getTotalUserCount = function(){
    return $http.get('/changi/userDetails/count');
  }
  var createNewUser = function(userData){
    return $http.post('/signup',userData)
  }
  var getUserCount = function(){
    return userCount;
  }
  var setUserCount = function(val){
    userCount=val;
  }
  var getUserDetailsByRange = function(skip,limit){
    return $http.get('/api/userDetails/get/'+skip+'/'+limit)
  }
  var getUserDetailsByUpdatedDate= function(skip,limit){
    return $http.get('/api/userDetails/get/UpdatedDate'+skip+'/'+limit)
  }
  var updateUserDetails = function(userData){
    return $http.post('/api/userDetails/update',userData)
  }
  var deleteUser = function(id){
    return $http.get('/api/userDetails/delete/'+id);
  }
  return{
    createNewUser:createNewUser,
    getTotalUserCount:getTotalUserCount,
    getUserCount:getUserCount,
    setUserCount:setUserCount,
    getUserDetailsByRange:getUserDetailsByRange,
    updateUserDetails:updateUserDetails,
    getUserDetailsByUpdatedDate:getUserDetailsByUpdatedDate,
    deleteUser:deleteUser

  /*

    getAllUsers:getAllUsers,
    deleteUser:deleteUser,*/
  }
})

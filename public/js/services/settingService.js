/**
 * Created by Suhas on 11/24/2015.
 */
changiServices.factory("settingService",function($http,$state){
  var notificationSimulator
  var mapDrawSimulator
  var vehicleDrawSimulator

  var intervalValues = [1000,1500,2000,2500,3000,3500];
  var currentSimulationSetting= {
    isNotificationSimulation:false,
    isMapDrawSimulation:false,
    isvehicleSimulation:false,
    isLineChartvehicleSimulation:false,
    isLaneWiseDataSimulation:false,
    isZoneWiseDataSimulation:false,
    notificationSimulationInterval:10000,
    mapDrawSimulationInterval:10000,
    vehicleSimulationInterval:intervalValues[Math.floor(Math.random()*intervalValues.length)],
    lineChartvehicleSimulationInterval:5000,
    laneWiseSimulationInterval:5000,
    laneZoneSimulationInterval:5000,
    lineChartSecondsRange:30,
    realTimeLaneChartSimulatorInterval:5000,
    isRealTimeLAneGraphSimulator:false,
    isWholeZoneDataGeneratorSimulation:false
  }
  var realTimeDataGraphSettings={
      laneSelected:0,
      zoneSelected:0,
      seconds:30,
      roadAnalyticsGraph:{
        /*road analytics realTime Graph*/
        type:"speed",
        zone:1,
        seconds:60
      }
  }
  var simulatorId = {
    laneZoneSimulationInterval:0,
    IndividualLaneAndZoneSimulationInterval:0,
    laneWiseSimulationInterval:0,
    lineChartvehicleDrawSimulator:0,
    realTimeLaneChartSimulatorId:0,
    wholeZoneAnalyticsDataGenerator:0,
    realTimeGraphDataFromWholeZoneAnalytics:0
  }
  var getPhoneNoForSms = function(){
    return $http.get("/api/phoneNoForSms");
  }
  var setPhoneNoForSms = function(phoneNo){
    return $http.get("/api/phoneNoForSms/"+phoneNo);
  }
  var setPolygonlatLong = function(polygonObj){
    return $http.post("/api/setTrolleyRegion",polygonObj);
  }
  var setThresholdPolygonlatLong = function(threshold){
    return $http.get("/api/setTrolleyThreshold/"+threshold);
  }
  var getPolygonlatLong = function(){
    return $http.get("/api/TrolleyRegion");
  }
  var getThresholdPolygonlatLong = function(){
    return $http.get("/api/TrolleyThreshold");
  }
  return{
    getNotificationSimulation: function(){
      return currentSimulationSetting.isNotificationSimulation
    },
    getMapDrawSimulation:function(){
      return currentSimulationSetting.isMapDrawSimulation

    },
    getVechicleDrawSimulation:function(){
      return currentSimulationSetting.isvehicleSimulation
    },
    getLineChartvehicleDrawSimulation:function(){
    return currentSimulationSetting.isLineChartvehicleSimulation
    },
    getLaneWiseDataSimulation:function(){
    return currentSimulationSetting.isLaneWiseDataSimulation
    },
    getZoneWiseDataSimulation:function(){
      return currentSimulationSetting.isZoneWiseDataSimulation
    },
    setNotificationSimulation: function(val){
      currentSimulationSetting.isNotificationSimulation = val;
    },
    setMapDrawSimulation:function(val){
      currentSimulationSetting.isMapDrawSimulation = val;
    },
    setVechicleDrawSimulation:function(val){
      currentSimulationSetting.isvehicleSimulation = val;
    },
    setLineChartvehicleDrawSimulation:function(val){
      currentSimulationSetting.isLineChartvehicleSimulation = val;
    },
    setZoneWiseDataSimulation:function(val){
      currentSimulationSetting.isZoneWiseDataSimulation = val;
    },
    setLaneWiseDataSimulation:function(val){
      currentSimulationSetting.isLaneWiseDataSimulation = val;
    },

    startNotificationSimulation:function(flag){
      notificationSimulator =setInterval(function() {
       $http.get('/api/newMsg').then(function(result){

        });
      },currentSimulationSetting.notificationSimulationInterval);
    },
    cancelNotificationSimulation:function(flag){
      clearInterval(notificationSimulator);
    },

    startMapDrawSimulation:function(flag){
      mapDrawSimulator =setInterval(function() {
        $http.get('/api/newPlotMsg').then(function(result){
        });
      },currentSimulationSetting.mapDrawSimulationInterval);
    },
    cancelMapDrawSimulation:function(){
      clearInterval(mapDrawSimulator);
    },


    /*This Simulator Will Start pushing Vehicle-Detection Analytics Data into the database*/
    startvehicleSimulation:function(flag){
      console.log("!!!!startvehicleSimulation!!!!!!")
      vehicleDrawSimulator =setInterval(function() {
        $http.get('/api/randomAnalytics');
      },currentSimulationSetting.vehicleSimulationInterval);
    },
    cancelvehicleSimulation:function(){
      clearInterval(vehicleDrawSimulator);
    },


    /*At first all zones and all lanes data will be pushed [For Dials]*/
    startLineChartvehicleSimulation:function(flag){
      console.log("!!!!startvehicleSimulationLine!!!!!!")

      var roadAnalyticsChartGraphSec = realTimeDataGraphSettings.seconds;
      simulatorId.lineChartvehicleDrawSimulator =setInterval(function() {
        var seconds = realTimeDataGraphSettings.seconds;
        if($state.is('map.laneMap')){
          $http.get('/api/realTime/wholeZoneAnalyticsLevelOfService/'+seconds);
        }
      },currentSimulationSetting.lineChartvehicleSimulationInterval);
    },
    cancelLineChartvehicleSimulation:function(){
      clearInterval(simulatorId.lineChartvehicleDrawSimulator);
    },

    /*
    /!*On selecting of this simulator data of particular lanes of all zones will be pushed [For Dials]*!/
    startLaneWiseDataSimulation:function(laneData,seconds){
      console.log("!!!!startLaneWiseDataSimulation!!!!!!")
      var seconds = realTimeDataGraphSettings.seconds;

      simulatorId.laneWiseSimulationInterval =setInterval(function() {
        var data = {
          laneSelected:laneData,
          seconds:realTimeDataGraphSettings.seconds
        }
          if($state.is('analytics.analyticsRT')) {
           /!* $http.post('/api/realTime/wholeZoneAnalyticsData/laneIndex/type', data);*!/
            /!*$http.post('/api/roadAnalyticsParametersForLanes', data);*!/
          }
          if($state.is('analytics.analyticsRT') || $state.is('map.laneMap')){
            $http.get('/api/realTime/wholeZoneAnalyticsLevelOfService/' + seconds);
          }
      },currentSimulationSetting.laneWiseSimulationInterval);
    },
    cancelLaneWiseDataSimulation:function(){
      clearInterval(simulatorId.laneWiseSimulationInterval);
    },


    /!*On selecting of this simulator data of particular zones[all lanes] be pushed [For Dials]*!/
    startZoneWiseDataSimulation:function(zoneData,seconds){
      console.log("!!!!startZoneWiseDataSimulation!!!!!!")
      simulatorId.laneZoneSimulationInterval =setInterval(function() {
        var data = {
          zoneSelected:zoneData,
          seconds:realTimeDataGraphSettings.seconds
        }
          if($state.is('analytics.analyticsRT')) {
            /!*$http.get('/api/wholeZoneAnalyticsData/' + seconds);*!/
           /!* $http.post('/api/roadAnalyticsParametersForZones', data);*!/
          }
        if($state.is('analytics.analyticsRT') || $state.is('map.laneMap')){
          $http.get('/api/realTime/wholeZoneAnalyticsLevelOfService/' + seconds);
        }
      },currentSimulationSetting.laneZoneSimulationInterval);
    },
    cancelZoneWiseDataSimulation:function(){
      clearInterval(simulatorId.laneZoneSimulationInterval);
    },


    /!*On selecting of this simulator data of particular zones and particular lanes will  be pushed [For Dials]*!/
    startLaneAndZoneWiseDataSimulation:function(zoneData,laneData,seconds){
      console.log("!!!!startZoneWiseDataSimulation!!!!!!")
      simulatorId.IndividualLaneAndZoneSimulationInterval =setInterval(function() {
        var data = {
          zoneSelected:zoneData,
          laneSelected:laneData,
          seconds:realTimeDataGraphSettings.seconds
        }
          if($state.is('analytics.analyticsRT')) {
            /!*$http.post('/api/realTime/wholeZoneAnalyticsData/zoneIndex/laneIndex', data);*!/
            /!*$http.post('/api/roadAnalyticsParametersForIndividualPoints', data);*!/
          }
        if($state.is('analytics.analyticsRT') || $state.is('map.laneMap')){
          $http.get('/api/realTime/wholeZoneAnalyticsLevelOfService/' + seconds);
        }
      },currentSimulationSetting.laneZoneSimulationInterval);
    },
    cancelLaneAndZoneWiseDataSimulation:function(){
      clearInterval(simulatorId.IndividualLaneAndZoneSimulationInterval);
    },*/

    /*on calling this function all the simulators related in refreshing data for dials in analytics page will be stopped [For Dials]*/
    cancelAllRealTimeSimulatorLaneAndZoneWiseDataSimulation:function(){
    /*  clearInterval(simulatorId.laneWiseSimulationInterval);
      clearInterval(simulatorId.laneZoneSimulationInterval);
      clearInterval(simulatorId.IndividualLaneAndZoneSimulationInterval);*/
      clearInterval(simulatorId.lineChartvehicleDrawSimulator);
      currentSimulationSetting.isLineChartvehicleSimulation = false;
    },


    setSelectedZoneForRealTimeData:function(zoneIndex){
      realTimeDataGraphSettings.zoneSelected = zoneIndex;
    },
    setSelectedLaneForRealTimeData:function(zoneIndex){
      realTimeDataGraphSettings.laneSelected = zoneIndex;
    },
    setSelectedSecondsForRealTimeData:function(seconds){
      realTimeDataGraphSettings.seconds = seconds;
    },
    getSelectedZoneForRealTimeData:function(){
      return realTimeDataGraphSettings.zoneSelected;
    },
    getSelectedLaneForRealTimeData:function(){
      return realTimeDataGraphSettings.laneSelected;
    },
    getSelectedSecondsForRealTimeData:function(){
      return realTimeDataGraphSettings.seconds;
    },


    setSelectedZoneForRoadAnalyticsRealTimeGraph:function(zone){
      realTimeDataGraphSettings.roadAnalyticsGraph.zone = zone;
    },
    setSelectedTypeForRoadAnalyticsRealTimeGraph:function(type){
      realTimeDataGraphSettings.roadAnalyticsGraph.type = type;
    },
    setSelectedSecondsForRoadAnalyticsRealTimeGraph:function(seconds){
      realTimeDataGraphSettings.roadAnalyticsGraph.seconds = seconds;
    },
    getSelectedZoneForRoadAnalyticsRealTimeGraph:function(){
      return realTimeDataGraphSettings.roadAnalyticsGraph.zone;
    },
    getSelectedTypeForRoadAnalyticsRealTimeGraph:function(){
      return realTimeDataGraphSettings.roadAnalyticsGraph.type;
    },
    getSelectedSecondsForRoadAnalyticsRealTimeGraph:function(){
      return realTimeDataGraphSettings.roadAnalyticsGraph.seconds;
    },


    /*This Simulator Will get last 60 seconds average of types(speed,time headway,space headway,volume)data from vehicle analytics for every 5 seconds*/
    startRealTimeLaneChartSimulator:function(){
      simulatorId.realTimeLaneChartSimulatorId =setInterval(function() {
        var dataForRoadAnalyticsRealTimeGraph = {
          zoneSelected:realTimeDataGraphSettings.roadAnalyticsGraph.zone,
          type:realTimeDataGraphSettings.roadAnalyticsGraph.type,
          seconds:realTimeDataGraphSettings.roadAnalyticsGraph.seconds/*default 1 minutes*/
        }
          if($state.is('analytics')) {
            /*$http.post('/api/realTime/roadAnalyticsData/type/zoneId', dataForRoadAnalyticsRealTimeGraph);*/
          }
      },currentSimulationSetting.realTimeLaneChartSimulatorInterval);
    },
    cancelRealTimeLaneChartSimulator:function(){
      clearInterval(simulatorId.realTimeLaneChartSimulatorId);
    },
    getRealTimeLaneChartSimulator:function(){
      return currentSimulationSetting.isRealTimeLAneGraphSimulator
    },
    setRealTimeLaneChartSimulator: function(val){
      currentSimulationSetting.isRealTimeLAneGraphSimulator = val;
    },

    /*This Simulator Will get last 60 seconds average of types(zone Speed,zone Occupancy,level Of Service)data from Whole zone analytics for every 5 seconds*/
    startRealTimeLaneChartSimulatorToGetWholeZoneAnalyticsData:function(){
      console.log("!!!!WZD S!!!!!!")
      var dataForRTGraph = {
        zoneSelected:realTimeDataGraphSettings.roadAnalyticsGraph.zone,
        type:realTimeDataGraphSettings.roadAnalyticsGraph.type,
        seconds:realTimeDataGraphSettings.roadAnalyticsGraph.seconds/*default 10 seconds*/
      }

      simulatorId.realTimeGraphDataFromWholeZoneAnalytics =setInterval(function() {
          if($state.is('analytics')) {
            /*$http.post('/api/realTime/wholeZoneAnalyticsData/type/zoneId', dataForRTGraph);*/
          }
      },currentSimulationSetting.realTimeLaneChartSimulatorInterval);
    },
    cancelRealTimeLaneChartSimulatorOfWholeZoneAnalyticsData:function(){
      clearInterval(simulatorId.realTimeGraphDataFromWholeZoneAnalytics);
    },


    /*This Simulator Will Start pushing wholeZone Analytics Data into the datatbase*/
    startWholeZoneAnalyticsdataGenertaorSimulation:function(flag){
      console.log("!!!!whole ZOne!!!!!!")
      simulatorId.wholeZoneAnalyticsDataGenerator =setInterval(function() {
        $http.get('/api/randomWholeZoneAnalytics');
      },currentSimulationSetting.vehicleSimulationInterval);
    },
    cancelWholeZoneAnalyticsDataGenertaorSimulation:function(){
      clearInterval(simulatorId.wholeZoneAnalyticsDataGenerator);
    },
    getWholeZoneAnalyticsDataGenertaorSimulation:function(){
      return currentSimulationSetting.isWholeZoneDataGeneratorSimulation
    },
    setWholeZoneAnalyticsDataGenertaorSimulation: function(val){
      currentSimulationSetting.isWholeZoneDataGeneratorSimulation = val;
      console.log(currentSimulationSetting)
    },


    /*This method will stop all the simulators related to real time graph in analytics*/
    cancelAllRealTimeLaneChartSimulator:function(){
      clearInterval(simulatorId.realTimeGraphDataFromWholeZoneAnalytics);
      clearInterval(simulatorId.realTimeLaneChartSimulatorId);
      currentSimulationSetting.isRealTimeLAneGraphSimulator = false;
    },
    getPhoneNoToSendNotification:getPhoneNoForSms,
    setPhoneNoToSendNotification:setPhoneNoForSms,
    setPolygonlatLong:setPolygonlatLong,
    setThresholdPolygonlatLong:setThresholdPolygonlatLong,
    getPolygonlatLong:getPolygonlatLong,
    getThresholdPolygonlatLong:getThresholdPolygonlatLong,
  }

})


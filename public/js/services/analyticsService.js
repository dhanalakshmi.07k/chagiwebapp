/**
 * Created by Suhas on 11/24/2015.
 */
changiServices.factory("analyticsService",function($http,$rootScope,$state){
  var roadAnalyticsData = {
    speed:[],
    timeHeadway:[],
    spaceHeadway:[],
    flow:[]
  };
  /*new*/
  var vehicleAnalytics = {
    DIAL_EVENT:"averagedDataForDials"
  }
  function analyticsDataForDials(data){
    $rootScope.$emit(vehicleAnalytics.DIAL_EVENT, data);
  }
  var setTypeForRTGraphOfVehicleDetection = function (parameter){
    $http.get('/api/setTheTypeOfParametersNeeded/'+parameter);
    $http.get('/api/setTheZoneTypeOfParametersNeeded/'+parameter);
  };
  var setZoneIdForRTGraphOfVehicleDetection = function (zoneId){
    $http.get('/api/setTheZoneTypeSelectedForRTGraph/'+zoneId);
    $http.get('/api/setTheZoneTypeSelectedForRTGraph/'+zoneId);

    /*$http.get('/getWholeZoneAnalyticsForDials');*/
  };
  var wholeZoneData;
  /*socket.on('dialDataForWholeZone', function (data) {
    newWholeZoneData(data);
  })*/
  var getVehicleAnalyticsHistoricalData = function(data){
  return $http.post("/historicalData/vehicleAnalytics",data)
}
  var getWholeZoneHistoricalData = function(data){
    return $http.post("/historicalData/wholeZoneAnalytics/secondWiseData",data)
  }
  var getVehicleHistoricalDataSecondWiseData = function(data){
    return $http.post("/historicalData/vehicleAnalytics/secondWiseData",data)
  }
  var newWholeZoneData= function(data){
    /*console.log("new Notification Service ....")*/
    wholeZoneData = data;
    $rootScope.$emit("wholeZoneDataForTerminals", data);

  }
  return{
    getVehicleAnalyticsForGivenSecondsOfCurrentDate:function(seconds){
      $http.get('/api/roadAnalyticsByAverageSpeed/'+seconds).then(function(result){

      });
    },
    getHistoricalDataForWholeZoneAnalyticsForSelectedLaneAndType: function(startDate,endDate,type,laneArra){
      var data ={
        startDate:startDate,
        endDate:endDate,
        type:type,
        lanesSelected:laneArra
      }
      return $http.post("/api/wholeZoneAnalyticsByDateTypeLanesSelected",data);
    },
    getRealTimeDataForRoadAnalyticsForSelectedZoneDurationAndType: function(zone,seconds,type){
      var dataForRoadAnalyticsRealTimeGraph = {
        zoneSelected:zone,
        type:type,
        seconds:seconds/*default 15 minutes*/
      }
       $http.post('/api/realTime/roadAnalyticsData/type/zoneId',dataForRoadAnalyticsRealTimeGraph);
    },
    getHistoricalRoadAnalyticsDtaForSelectedZoneDateRangeAndType:function(startDate,endDate,zone,type){
      var data = {
          startDate:startDate,
          endDate:endDate,
          zoneSelected:zone,
          type:type
      }
       return $http.post('/api/historicalData/roadAnalyticsByZoneAndType',data)
    },
    getRealTimeRoadAnalyticsDtaForSelectedLane:function(laneData,seconds){
      var data = {
        laneSelected:laneData,
        seconds:seconds
      }
       $http.post('/api/roadAnalyticsParametersForLanes',data)
    },
    getRealTimeVehicleDetectionDataForSelectedLaneZoneAndLastSecondsData:function(lane,zone,seconds){
      var data = {
        zoneSelected:zone,
        laneSelected:lane,
        seconds:seconds
      }
      $http.post('/api/roadAnalyticsParametersForIndividualPoints',data)
    },
    getRealTimeVehicleDetectionDataOfAllZones:function(seconds){
       $http.get('/api/roadAnalyticsParametersForAllLanesAndZones/'+seconds);
    },
    getRealTimeLevelOfServiceDataForSelectedLastSeconds:function(seconds){
      $http.get('/api/realTime/wholeZoneAnalyticsLevelOfService/'+seconds)
    },
    getRealTimeWholeZoneAnalyticsDataForSelectedLane:function(laneData,seconds){
      var data = {
        laneSelected:laneData,
        seconds:seconds
      }
      $http.post('/api/realTime/wholeZoneAnalyticsData/laneIndex/type',data)
    },
    getRealTimeWholeZoneAnalyticsDataForSelectedLaneZoneAndLastSecondsData:function(lane,zone,seconds){
      var data = {
        zoneSelected:zone,
        laneSelected:lane,
        seconds:seconds
      }
      $http.post('/api/realTime/wholeZoneAnalyticsData/zoneIndex/laneIndex',data)
    },
    getRealTimeWholeZoneAnalyticsDataOfAllZones:function(seconds){
      $http.get('/api/realTime/wholeZoneAnalyticsDataForAllLanesAndZones/'+seconds);
    },
    /*to get occupancy data for lane chart graph*/
    getRealTimeWholeAnalyticsDataForSelectedLanesForLaneGraphChart:function(zone,seconds){
      var data = {
        zoneSelected:zone,
        seconds:seconds
      }
      $http.post('/api/realTime/wholeZoneAnalyticsData/type/zoneId',data)
    },
    /*New*/
    setSecondsForRealTimeAnalyticsDials:function(seconds){
      console.log("time");
      $http.get('/api/changeAverageDataOfSeconds/'+seconds);
      $http.get('/api/changeAverageDataOfSecondsForWholeZone/'+seconds);
  },
    startGettingDataForRealTimeDials:function(){
      console.log("start");
      /*$http.get('/api/startAnalytics');
      $http.get('/api/startWholeZoneAnalytics');*/
      /*$http.get('/api/startGettingLOSFromWholeZoneAnalytics');*/
  },
    stopGettingDataForRealTimeForRTDials:function(){
      /*$http.get('/api/stopAnalytics');
      $http.get('/api/stopWholeZoneAnalytics');*/
     /* $http.get('/api/stopGettingLOSFromWholeZoneAnalytics');*/
  },
    setTheTerminalSelectedForRTDials:function(terminal){
      $http.get('/api/selectedTerminals/'+terminal);
      $http.get('/api/selectedTerminalsForWholeZone/'+terminal);
  },
    getTheAnalyticsForRTDials:function(){
      $http.get('/api/getVehicleAnalyticsForDials');
      $http.get('/api/getWholeZoneAnalyticsForDials');
    },
    vehicleAnalytics:vehicleAnalytics,
    setTypeForRTGraph:function(type){
      setTypeForRTGraphOfVehicleDetection(type);
    },
    setZoneIdForRTGraph:function(zoneId){
      setZoneIdForRTGraphOfVehicleDetection(zoneId);
    },
    getWholeZoneDataTerminalWise:function(){
      return wholeZoneData;
    },
    getVehicleAnalyticsHistoricalData:getVehicleAnalyticsHistoricalData,
    getWholeZoneHistoricalData:getWholeZoneHistoricalData,
    getVehicleHistoricalDataSecondWiseData:getVehicleHistoricalDataSecondWiseData
  }
})

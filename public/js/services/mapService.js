/**
 * Created by zendynamix on 12-01-2016.
 */
changiServices.factory("mapService",function($http){
  var cameraPlotArray=[];
  var map={
    liveMapData:'',
    mapZoneData:{
      los:[]
    }
  }
  //sstter for map object
  var setMapObject= function (obj) {
    map.liveMapData = obj;
  }
  var getMapObject= function () {
    return map.liveMapData;
  }
  var  setMapZoneData=function(data){
    map.mapZoneData.los = data;
    /* console.log(map.mapZoneData.los);*/
  }
  var getMapZoneData=function(){
    return map.mapZoneData.los;
    console.log(map.mapZoneData.los);
  }


  var getCameraMarkerDetails=function(){
    return $http.get("/api/cameraMarkersDetails");
  }
  var  getZoneSegmentInLaneDetails=function(){
    return $http.get("/api/zoneSegmentInLaneDetails");
  }


  var setCameraPlotArray= function (obj) {
    cameraPlotArray = obj;
  }
  var getCameraPlotArray= function () {
    return cameraPlotArray;
  }

  return {
    setMapObject:setMapObject,
    getMapObject:getMapObject,
    setMapZoneData:setMapZoneData,
    getMapZoneData:getMapZoneData,
    setCameraPlotArray:setCameraPlotArray,
    getCameraPlotArray:getCameraPlotArray,
    getCameraMarkerDetails:getCameraMarkerDetails,
    getZoneSegmentInLaneDetails:getZoneSegmentInLaneDetails,
    "geoJsonArray":[
      {
        object: {
          "type": "FeatureCollection",
          "features": [
            {
              "type": "Feature",
              "properties": {},
              "geometry": {
                "type": "LineString",
                "coordinates": [
                  [
                    103.98410975933075,
                    1.3470743673411147
                  ],
                  [
                    103.985117,
                    1.349542
                  ]
                ]
              }
            }
          ]
        }
      },
      {
        object: {
          "type": "FeatureCollection",
          "features": [
            {
              "type": "Feature",
              "properties": {},
              "geometry": {
                "type": "LineString",
                "coordinates": [
                  [
                    103.985117,
                    1.349542
                  ],
                  [
                    103.987266,
                    1.354569
                  ]
                ]
              }
            }
          ]
        }
      },
      {
        object: {
          "type": "FeatureCollection",
          "features": [
            {
              "type": "Feature",
              "properties": {},
              "geometry": {
                "type": "LineString",
                "coordinates": [
                  [
                    103.987266,
                    1.354569
                  ],
                  [
                    103.98874461650848,
                    1.3579986423496342
                  ]
                ]
              }
            }
          ]
        }
      }
    ],
    "layerArray":[{
      source: {
        type: 'OSM',
        url: 'http://{a-c}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png',
        attribution: 'All maps &copy; <a href="http://www.opencyclemap.org/">OpenCycleMap</a>'
      }
    }]

  }
})

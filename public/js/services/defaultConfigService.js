/**
 * Created by Suhas on 4/29/2016.
 */
changiServices.factory("defaultConfigService",function($http,$window){
  var getEnvironmentType=function(){
    return $http.get("/api/getEnvType");
  }
  var getSettingsConfiguration = function(){
    return $http.get("/api/getSettingsDetails");
  }
  var getAdminStatusByToken = function(){
    if($window.sessionStorage.token){
      var data = {
        token:$window.sessionStorage.token
      }
      return $http.post('/userDetails/get/adminStatus',data);
    }else{
      return false
    }

  }
  return{
    getEnvironmentType:getEnvironmentType,
    getSettingsConfiguration:getSettingsConfiguration,
    getAdminStatusByToken:getAdminStatusByToken
  }
})

/**
 * Created by MohammedSaleem on 11/11/15.
 */

//var changi=angular.module("changi",['ngRoute','changiControllers']);


var dependencies = ['ui.router', 'changiControllers', 'notificationModule', 'changiServices',
    'areaGraph', 'appSlider', 'verticalNav', 'circleGauge', 'columnChart', 'spline',
  'checklist-model','openlayers-directive','customDatePickerModule'];/*,'custom-date-picker-module'*/
var changi = angular.module("changi", dependencies);


changi.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider.
        state('userPage', {
            url: "/userPage",
            templateUrl: 'Unsecure/user.html'
        }).
        state('userPage.login', {
            url: "/login",
            templateUrl: 'Unsecure/login.html',
            controller: 'loginCtrl'
        }).
        state('userPage.signUp', {
            url: "/signUp",
            templateUrl: 'Unsecure/signUp.html',
            controller: 'loginCtrl'
        }).

        state('traffic', {
            url: "/traffic",
            templateUrl: 'templates/traffic/traffic.html'
        }).
        state('traffic.laneMap', {
            url: "/laneMap",
            templateUrl: 'templates/traffic/laneMap.html'

        }).
        state('traffic.cam', {
            url: "/cam",
            templateUrl: 'templates/traffic/cam.html',
            controller: 'cam'
        }).
        state('traffic.analytics', {
            url: "/analytics",
            templateUrl: 'templates/traffic/analytics.html',
            controller: 'analytics'
        }).
        state('traffic.trafficSetting', {
            url: "/trafficSetting",
            templateUrl: 'templates/traffic/trafficSetting.html'
        }).
        state('traffic.trafficSetting.trafficSimulator', {
            url: "/trafficSimulator",
            templateUrl: 'templates/traffic/trafficSimulator.html'
        }).
        state('traffic.trafficSetting.trafficConfig', {
            url: "/trafficConfig",
            templateUrl: 'templates/traffic/trafficConfig.html'
        }).
        state('traffic.trafficSetting.data', {
            url: "/data",
            templateUrl: 'templates/traffic/data.html',
            controller: 'dataDownload'
        }).
        state('traffic.trafficSetting.users', {
            url: "/users",
            templateUrl: 'templates/traffic/users.html'
        }).




        state('terminal', {
            url: "/terminal",
            templateUrl: 'templates/terminal/terminal.html'
        }).
        state('terminal.terminalMap', {
            url: "/terminalMap",
            templateUrl: 'templates/terminal/terminalMap.html'
        }).
        state('terminal.terminalSettings', {
            url: "/terminalSettings",
            templateUrl: 'templates/terminal/terminalSettings.html'
        }).
        state('terminal.terminalSettings.trolleySettings', {
            url: "/trolleySettings",
            templateUrl: 'templates/terminal/trolleySettings.html'
        }).

        //state('analytics.analyticsRT', {
        //    url: "/analyticsRT",
        //    templateUrl: 'templates/analyticsRT.html'
        //}).
        //state('analytics.analyticsHis', {
        //    url: "/analyticsHis",
        //    templateUrl: 'templates/analyticsHis.html'
        //}).
        //state('analytics.analyticsPeriod', {
        //    url: "/analyticsPeriod",
        //    templateUrl: 'templates/analyticsPeriod.html'
        //}).
        //state('analytics1', {
        //    url: "/analytics1",
        //    templateUrl: 'templates/analytics1.html',
        //    controller: 'analytics'
        //}).
        //state('analytics2', {
        //    url: "/analytics2",
        //    templateUrl: 'templates/analytics2.html',
        //    controller: 'analytics'
        //}).
        state('settings', {
            url: "/settings",
            templateUrl: 'templates/settings.html',
            controller: 'settingsController'
        }).
        state('settings.accessCont', {
            url: "/accessCont",
            templateUrl: 'templates/settings/accessCont.html'
        }).
        state('settings.accessCont.manager', {
            url: "/manager",
            templateUrl: 'templates/settings/manager.html'
        }).
        state('settings.accessCont.staff', {
            url: "/staff",
            templateUrl: 'templates/settings/staff.html'
        }).
        state('settings.accessCont.senManager', {
            url: "/senManager",
            templateUrl: 'templates/settings/senManager.html'
        }).
        state('settings.accessCont.simulator', {
            url: "/simulator",
            templateUrl: 'templates/settings/simulator.html'
        }).
        state('settings.conditions', {
            url: "/conditions",
            templateUrl: 'templates/settings/conditions.html'
        }).
        state('settings.conditions.alerts', {
            url: "/alerts/all",
            templateUrl: 'templates/settings/allAlerts.html'
        }).
        state('settings.conditions.newAlert', {
            url: "/alerts/new",
            templateUrl: 'templates/settings/newAlert.html'
        }).
        state('settings.conditions.notifications', {
            url: "/notifications/all",
            templateUrl: 'templates/settings/notifications.html'
        }).
        state('settings.conditions.newNotifications', {
            url: "/notifications/new",
            templateUrl: 'templates/settings/newNotification.html'
        }).
        state('settings.conditions.actions', {
            url: "/actions/all",
            templateUrl: 'templates/settings/actions.html'
        }).
        state('settings.conditions.newActions', {
            url: "/actions/new",
            templateUrl: 'templates/settings/newAction.html'
        });

    $urlRouterProvider.otherwise("/userPage/login");
})
//changi.config(function ($routeProvider) {
//    $routeProvider.
//        when('/map',{
//            templateUrl: 'templates/map.html',
//            controller: 'map'
//        }).
//        when('/cam',{
//            templateUrl: 'templates/cam.html',
//            controller: 'cam'
//        }).
//        when('/analytics',{
//            templateUrl:'templates/analytics.html',
//            controller: 'analytics'
//        }).
//        otherwise({
//            redirectTo: '/map'
//        })
//});


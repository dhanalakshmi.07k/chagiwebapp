changiControllers.controller("settingsController",function($scope,$http,settingService,trolleyService,
                                                           notificationService) {


  $scope.isNotificationSimulation = settingService.getNotificationSimulation();
  $scope.isMapDrawSimulation = settingService.getMapDrawSimulation();
  $scope.isvehicleSimulation = settingService.getVechicleDrawSimulation();
  $scope.isLineChartvehicleSimulation = settingService.getLineChartvehicleDrawSimulation();
  $scope.isLaneWiseDataSimulation = settingService.getLaneWiseDataSimulation();
  $scope.isRealTimeLaneChartSimulator = settingService.getRealTimeLaneChartSimulator();
  $scope.isWholeZoneDataGeneratorSimulationFlag = settingService.getWholeZoneAnalyticsDataGenertaorSimulation();
  $scope.trolleyDetails={
      macAddress:trolleyService.getMacAddressOfTheTrolley(),
      currentRefreshTrolleyLocationRefreshTime:0
  }
  $scope.realTimeDataLaneChartSimulation =
  $scope.simulateNotification = function(){
    console.log("!!!!!!!!!!!!");
    var flag = settingService.getNotificationSimulation();
    settingService.setNotificationSimulation(!flag)
    $scope.isNotificationSimulation = !flag;

    if(!flag){
      settingService.startNotificationSimulation(flag);
    }else{
      settingService.cancelNotificationSimulation();
    }
  }
  $scope.simulateMapDraw = function(){
      var flag = settingService.getMapDrawSimulation();
      settingService.setMapDrawSimulation(!flag)
      $scope.isMapDrawSimulation = !flag;

      if(!flag){
        settingService.startMapDrawSimulation(flag);
      }else{
        settingService.cancelMapDrawSimulation();
        /**/
      }
    }
  $scope.simulateVehicle = function(){
      var flag = settingService.getVechicleDrawSimulation();
      settingService.setVechicleDrawSimulation(!flag)
      $scope.isvehicleSimulation = !flag;
      if(!flag){
        settingService.startvehicleSimulation(flag);
      }else{
        settingService.cancelvehicleSimulation();
        /**/
      }
    }
  $scope.simulateLineChartVehicle = function(){
      var flag = settingService.getLineChartvehicleDrawSimulation();
      /*Stops all simulator related to dials*/
      settingService.cancelAllRealTimeSimulatorLaneAndZoneWiseDataSimulation();
      settingService.setLineChartvehicleDrawSimulation(!flag);
      $scope.isLineChartvehicleSimulation = !flag;
      if(!flag){
        settingService.startLineChartvehicleSimulation(flag);
      }else{
        settingService.cancelLineChartvehicleSimulation();
        settingService.cancelAllRealTimeSimulatorLaneAndZoneWiseDataSimulation();

      }
    }
  $scope.simulateLaneWiseData = function(){
      var flag = settingService.getLaneWiseDataSimulation();
      settingService.setLaneWiseDataSimulation(!flag)
      $scope.isLaneWiseDataSimulation = !flag;
      if(!flag){
        settingService.startLaneWiseDataSimulation(flag);
      }else{
        settingService.cancelLaneWiseDataSimulation();

      }
    }
  $scope.simulateRealTimeDataLaneChartSimulation = function(){
    var flag = settingService.getRealTimeLaneChartSimulator();
    settingService.cancelAllRealTimeLaneChartSimulator();
    settingService.setRealTimeLaneChartSimulator(!flag)
    $scope.isRealTimeLaneChartSimulator = !flag;
    if(!flag){
      settingService.startRealTimeLaneChartSimulator(flag);
    }else{
      settingService.cancelRealTimeLaneChartSimulator();

    }
  }
  $scope.generateDataForWholeZoneAnalytics = function(){
    var flag = settingService.getWholeZoneAnalyticsDataGenertaorSimulation();
    settingService.setWholeZoneAnalyticsDataGenertaorSimulation(!flag)
    $scope.isWholeZoneDataGeneratorSimulationFlag = !flag;
    if(!flag){
      settingService.startWholeZoneAnalyticsdataGenertaorSimulation();
    }else{
      settingService.cancelWholeZoneAnalyticsDataGenertaorSimulation();
    }
  }

  /*trolley related*/
  /*$scope.setMacAddressOfTrolleyToBeTracked=function(){
    trolleyService.setMacAddressOfTheTrolley($scope.trolleyDetails.macAddress);
    console.log($scope.trolleyDetails.macAddress)
  }*/
  $scope.trolleyRefreshTimeUnit = 'sec';
  $scope.changeUnitsForRefreshTime = function(unit){
    $scope.trolleyRefreshTimeUnit = unit;
    if($scope.trolleyRefreshTimeUnit=='sec'){
      $scope.trolleyDetails.currentRefreshTrolleyLocationRefreshTime = $scope.trolleyDetails.currentRefreshTrolleyLocationRefreshTime*60000;
    }else{
      $scope.trolleyDetails.currentRefreshTrolleyLocationRefreshTime = $scope.trolleyDetails.currentRefreshTrolleyLocationRefreshTime/60000;
    }
  }
  trolleyService.getRefreshTimeToGetTrolleyLocation().then(function(result){
    var timeInSeconds = result.data.refreshTime;
    if($scope.trolleyRefreshTimeUnit=='sec'){
      $scope.trolleyDetails.currentRefreshTrolleyLocationRefreshTime=timeInSeconds/1000
    }else{
      $scope.trolleyDetails.currentRefreshTrolleyLocationRefreshTime=timeInSeconds/60000
    }
    console.log(result)
  });
  settingService.getPolygonlatLong().then(function(result){
    $scope.trolleyRegionTopX =result.data.topLeft.x;
    $scope.trolleyRegionTopY=16000-result.data.topLeft.y;
    $scope.trolleyRegionBottomX =result.data.bottomRight.x;
    $scope.trolleyRegionBottomY=16000-result.data.bottomRight.y;
  })
  settingService.getThresholdPolygonlatLong().then(function(result){
    $scope.trolleyThreshold = result.data.threshold;
  })
  $scope.setUpdateRefreshTimeForTrolleyLocation=function(minutes){
    var seconds;
    if($scope.trolleyRefreshTimeUnit=='sec'){
      seconds = $scope.trolleyDetails.currentRefreshTrolleyLocationRefreshTime*1000;
    }else{
      seconds = $scope.trolleyDetails.currentRefreshTrolleyLocationRefreshTime*60*1000;
    }
    trolleyService.updateTrolleyRefreshTime(seconds);
    setTrolleyRegion();
    setTrolleyThresholdForRegion();
  }
  function setTrolleyRegion(){
    var data = {
      top:{
        x:$scope.trolleyRegionTopX,
        y:16000-$scope.trolleyRegionTopY
      },
      bottom:{
        x:$scope.trolleyRegionBottomX,
        y:16000-$scope.trolleyRegionBottomY
      }
    }
    settingService.setPolygonlatLong(data);
  }
  function setTrolleyThresholdForRegion(){
    var threshold = $scope.trolleyThreshold;
    settingService.setThresholdPolygonlatLong(threshold);
  }

  /*sms related*/
  $scope.getPhoneNoForSendingNotification = function(){
    /*$scope.phoneNoForSendingNotification = */
    settingService.getPhoneNoToSendNotification().then(function(result){
      $scope.phoneNoForSendingNotification = result.data.phoneNo;
      console.log(result)
    })
  }
  $scope.setPhoneNoForSendingNotification=function(){
      settingService.setPhoneNoToSendNotification($scope.phoneNoForSendingNotification);
  }
  $scope.getPhoneNoForSendingNotification();
  $scope.smsNotificationToggleStatus = false;

  /*SMS Notification*/
  notificationService.getAUtoNotifierStatus().then(function(result){
    $scope.smsNotificationToggleStatus = result.data;
  });
  $scope.smsNotificationToggle = function(){
    notificationService.toggleSmsNotificationNotifier($scope.smsNotificationToggleStatus);
  }




  $scope.startAllSimulator = function(){
    $scope.stopAllSimulator();
    $scope.generateDataForWholeZoneAnalytics();
    $scope.simulateVehicle();
  }
  $scope.stopAllSimulator=function(){
    settingService.cancelvehicleSimulation();
    settingService.cancelWholeZoneAnalyticsDataGenertaorSimulation();
    settingService.setWholeZoneAnalyticsDataGenertaorSimulation(false);
    settingService.setVechicleDrawSimulation(false);
    $scope.isWholeZoneDataGeneratorSimulationFlag =false;
    $scope.isvehicleSimulation = false;
  }

  function getSimulatorService(){
    $scope.isWholeZoneDataGeneratorSimulationFlag =settingService.getWholeZoneAnalyticsDataGenertaorSimulation();
    $scope.isvehicleSimulation = settingService.getVechicleDrawSimulation();
  }
  function init(){
    getSimulatorService();
  }
  init();
})

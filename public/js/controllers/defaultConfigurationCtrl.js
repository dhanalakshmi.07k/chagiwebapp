/**
 * Created by Suhas on 4/29/2016.
 */
changiControllers.controller("defaultConfigCtrl", function ($scope,defaultConfigService) {
  $scope.defaultConfigData={
    showSimulator:true,
    showDataDownload:true,
    showUserDetailsTab:true
  }
  function getEnvironmentType(){
    defaultConfigService.getEnvironmentType().then(function(result){
      if(result.data){
        if(result.data==='production'){
          $scope.defaultConfigData.showSimulator=false;
        }
      }
    })
  }
  function getSettingsConfiguration(){
    defaultConfigService.getSettingsConfiguration().then(function(result){
        if(result.data){
          $scope.defaultConfigData.showSimulator=
            result.data.settingTabConfiguration.showSimulatorTab;
          /*$scope.defaultConfigData.showDataDownload=
            result.data.settingTabConfiguration.showDataDownloadTab;
          $scope.defaultConfigData.showUserDetailsTab=
            result.data.settingTabConfiguration.showUserDetailsTab;*/
        }else{

        }
    })
  }
  function getAdminStatus(){
      defaultConfigService.getAdminStatusByToken().then(function(result){
          console.log(result.data);
          console.log(typeof(result.data))
          console.log(result.data)
          $scope.defaultConfigData.showUserDetailsTab=result.data;
      })
  }
  function init(){
    getSettingsConfiguration();
    getAdminStatus();
  }
  init();
})

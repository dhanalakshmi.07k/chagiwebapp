/**
 * Created by zendynamix on 20-04-2016.
 */
changiControllers.controller("userCtrl", function ($scope,authInterceptor,userService,authInterceptor,$http) {
  $scope.userData = {
    newUserData:{},
    userArray:[],
    pageCapacity:10,
    userCountArray:0,
  userFormDetails:{
      formTitle:"New Accounts",
      buttonType:"Save"
    },
    delete:{
      userId:0,
      userIndex:0,
      userName:''
    }

  }
  $scope.clearFormData = function(){
    $scope.userData.newUserData = {};
    $scope.userData.userFormDetails.formTitle="New Account";
    $scope.userData.userFormDetails.buttonType="Save"
  }

  $scope.saveNewUser = function () {
    $http.post('/signup', $scope.userData.newUserData)
      .success(function (data, status, headers, config) {
        getTotalUserCount();
        getUserDataForPagination(1);
        $scope.error ="";
        $scope.successMessage="Successfully registered Please login";
      })
      .error(function (data, status, headers, config) {
        $scope.error =data;
      });
  };


  $scope.editUserDetails = function(userData){
    $scope.userData.newUserData = userData;
    $scope.userData.userFormDetails.formTitle="Update Account";
    $scope.userData.userFormDetails.buttonType="Update"
  }

  $scope.updateUserDetails = function(){
    userService.updateUserDetails($scope.userData.newUserData).then(function(res){
      $scope.userData.newUserData = {};
      getUserDataForPaginationBasedOnUpdatedDate(1);

    })
  }
  $scope.editUserDetails = function(userData){
    $scope.userData.newUserData = userData;
    $scope.userData.userFormDetails.formTitle="Update Account";
    $scope.userData.userFormDetails.buttonType="Update"
  }


 /* function getTotalUserCount(){
    userService.getTotalUserCount().then(function(res){
      $scope.userData.userCountArray = res.userCount;
      userService.setUserCount($scope.userData.userCountArray);
      console.log(userService.getUserCount())
    })
  }*/

  function getTotalUserCount(){
    userService.getTotalUserCount().then(function(res){
      if(res.data.userCount){
        var num=res.data.userCount;
        var quotient=num/10;
        var rem=num%10;
        if(res.data.userCount<10){
          $scope.userData.userCountArray=[1];
        }
        if(rem && res.data.userCount>10){
          formArrayForGivenNumber(quotient);
        }
       /* else if(!rem &&res.data.userCount>10){
          formArrayForGivenNumber(quotient);
        }*/
      }
      userService.setUserCount($scope.userData.userCountArray);
      console.log(userService.getUserCount())
    })
  }
  function formArrayForGivenNumber(number){
    var numberArray=[];
    for(var j=0;j<number;j++){
      numberArray.push(j);
    }
    $scope.userData.userCountArray=numberArray;
  }
  function getUserDataForPagination(pageNo){
    var pageCapacity = $scope.userData.pageCapacity;
    var start = (pageNo-1)*pageCapacity;
    var limit =$scope.userData.pageCapacity
    userService.getUserDetailsByRange(start,limit).then(function(res){
      $scope.userData.userArray=res.data;
    })
  }
  $scope.setUserDataForPagination = function(pageNo) {
    $scope.userData.pageSelected=pageNo;
    var pageCapacity = $scope.userData.pageCapacity;
    var start = (pageNo-1)*pageCapacity;
    var limit =$scope.userData.pageCapacity
    userService.getUserDetailsByRange(start,limit).then(function(res){
      $scope.userData.userArray=res.data;
    })
  }

  function getUserDataForPaginationBasedOnUpdatedDate(pageNo){
    var pageCapacity = $scope.userData.pageCapacity;
    var start = (pageNo-1)*pageCapacity;
    var limit =$scope.userData.pageCapacity
    userService.getUserDetailsByUpdatedDate(start,limit).then(function(res){
      $scope.userData.userArray=res.data;
    })
  }

  $scope.deleteUser = function(){
    var  id = $scope.userData.delete.userId,
      index = $scope.userData.delete.userIndex;
    userService.deleteUser(id).then(function(res){
      getTotalUserCount();
      if (index > -1) {
        $scope.userData.userArray.splice(index, 1);
      }
    })
  }

  $scope.setUserIdForDelete = function(userId,index,name){
    $scope.userData.delete.userId = userId;
    $scope.userData.delete.userName = name;
    $scope.userData.delete.userIndex = index;
  }

  function init(){
    getTotalUserCount();
    getUserDataForPagination(1);
  }
  init();

})

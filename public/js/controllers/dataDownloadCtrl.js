/**
 * Created by MohammedSaleem on 09/03/16.
 */

changiControllers.controller("dataDownload", function ($scope,dataDownloadService) {
   /* var datePicker=$(".datepicker");

    datePicker.datepicker({
        dateFormat: "dd-mm-yy",
        dayNamesMin: [ "S", "M", "T", "W", "T", "F", "S" ]
    });

    datePicker.datepicker("setDate", new Date());
  var eDatePicker=$(".edatepicker");
  eDatePicker.datepicker({
    dateFormat: "dd-mm-yy",
    dayNamesMin: [ "S", "M", "T", "W", "T", "F", "S" ]
  });*/
  /*eDatePicker.datepicker("setDate", new Date());*/

    $scope.dataDownloadDetails ={
      dataType:"vehicle",
       startDate:'',
       endDate:'',
      endDateLimit:31,
      alertMessageFlag:false,
      alertMessage:''
    }
    $scope.setDownloadDataType=function(type){
      $scope.dataDownloadDetails.dataType=type;
      console.log($scope.dataDownloadDetails)
    }
    $scope.startDownload=function(){
      var data = $scope.dataDownloadDetails.dataType;
      if($scope.dataDownloadDetails.endDate!='' && $scope.dataDownloadDetails.startDate!=''){
       var endDate = moment($scope.dataDownloadDetails.endDate,'MM/DD/YYYY').toDate();/*.toDate()*/
        endDate.setHours(23)
       var startDate = moment($scope.dataDownloadDetails.startDate,'MM/DD/YYYY').toDate();/*.toDate()*/
        startDate.setHours(0)
       var timeDiff = Math.abs(endDate.getTime() - startDate.getTime());
       var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        if(endDate.getTime()>startDate.getTime()){
          if(diffDays>$scope.dataDownloadDetails.endDateLimit){
            $scope.dataDownloadDetails.alertMessageFlag=true;
            $scope.dataDownloadDetails.alertMessage="Difference Between Days is limited To "+$scope.dataDownloadDetails.endDateLimit;
          }else if(diffDays==0){
            $scope.dataDownloadDetails.alertMessageFlag=true;
            $scope.dataDownloadDetails.alertMessage="Difference Between Date Should Be Minimum 1 ";
          }else if(diffDays==0){
            $scope.dataDownloadDetails.alertMessageFlag=true;
            $scope.dataDownloadDetails.alertMessage="Difference Between Date Should Be Minimum 1 ";
          }else{
            $scope.dataDownloadDetails.alertMessageFlag=false;
            var stDate = new Date(startDate);
            var edDate = new Date(endDate);
            dataDownloadService.downloadData(data,stDate,edDate);
          }
        }else{
          $scope.dataDownloadDetails.alertMessageFlag=true;
          $scope.dataDownloadDetails.alertMessage="End Date Should Be Greater Than Start Date";
        }
       }else{
       $scope.dataDownloadDetails.alertMessageFlag=true;
       $scope.dataDownloadDetails.alertMessage="Enter Date"
       }
      /*dataDownloadService.downloadData($scope.dataDownloadDetails.dataType,$scope.dataDownloadDetails.endDate,$scope.dataDownloadDetails.endDate);*/
    }
    $scope.validateStartAndEndDate=function(){

    }
});

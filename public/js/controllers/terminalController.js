/**
 * Created by zendynamix on 12-01-2016.
 */

changiControllers.controller("terminalController", function ($scope, $http, olData, trolleyService, $rootScope, $state, settingService) {
  $scope.trollies = [];
  $scope.getTrolleyFlag = false;
  /* $scope.trolliesInRegionCount;*/

  $scope.layers = [{
    source: {
      type: "ImageStatic",
      url: "images/T3L2_16k_terminal3.jpg",
      imageSize: [16000, 16000]
    }
  }];
  angular.extend($scope, {
    center: {
      coord: [8000, 8000],
      zoom: 3
    },
    defaults: {
      view: {
        projection: 'pixel',
        extent: [0, 0, 16000, 16000]
      }

    },
    staticlayer: $scope.layers
  });
  socket.on("updatedTrolleyLocation", function (data) {
    if ($state.is("terminal.terminalMap")) {
      $scope.trollies = [];
      jsonPaylod = data;
      plotTrolleys(jsonPaylod);
    }
    $scope.$apply();
  })
  function plotTrolleys(trolleyArray) {
    var jsonPaylod = trolleyArray;
    var custom_style = {
      image: {
        icon: {
          anchor: [1, 1],
          anchorXUnits: 'fraction',
          anchorYUnits: 'fraction',
          opacity: 0.80,
          src: 'images/TrolleyRound_small.png'
        }
      }
    };
    var mapSize = 16000;
    for (var i = 0; i < jsonPaylod.length; i++) {
      var objMap = {
        coord: [jsonPaylod[i].x, mapSize - (jsonPaylod[i].y)],
        projection: 'pixel',
        label: {
          message: '<div>' + 'MacAddress: ' + jsonPaylod[i].macAddress + '</div>',
          show: false,
          showOnMouseClick: true
        },
        events: {},
        style: custom_style
      };
      $scope.trollies.push(objMap);

    }
    countMarker(trolleyArray);

  }
  var geoJsonArray = [];
  function getPolygonPlots() {
    trolleyService.getPolygonDetails().then(
      function (resultdata) {
        console.log(resultdata);
        for (var k = 0; k < resultdata.data.length; k++) {
          formPolygonObject(resultdata.data[k].polygonobj.corordinat1Lat, resultdata.data[k].polygonobj.corordinat1Lan,
            resultdata.data[k].polygonobj.corordinat2Lat, resultdata.data[k].polygonobj.corordinat2Lan, resultdata.data[k].threshold);
        }
      })
  }
  getPolygonPlots();
  function formPolygonObject(corordinat1Lat, corordinat1Lan, corordinat2Lat, corordinat2Lan, threshold) {
    var obj = {
      object: {
        "type": "FeatureCollection",
        "features": [
          {
            "type": "Feature",
            "geometry": {
              "type": "Polygon",
              "coordinates": [[
                [
                  corordinat1Lat,
                  corordinat1Lan
                ],
                [
                  corordinat2Lat,
                  corordinat1Lan
                ],
                [
                  corordinat2Lat,
                  corordinat2Lan
                ],
                [
                  corordinat1Lat,
                  corordinat2Lan
                ],
                [
                  corordinat1Lat,
                  corordinat1Lan
                ]
              ]
              ]
            }
          }
        ]
      }
    }
    geoJsonArray.push(obj);
    addPolygon(geoJsonArray);

  }
  function addPolygon(geoJsonArray) {
    for (var j = 0; j < geoJsonArray.length; j++) {
      var obj = {
        source: {
          type: 'GeoJSON',
          geojson: geoJsonArray[j]
        },
        style: {
          fill: {
            color: "rgba(235, 145, 0, 0.30)"
          },
          stroke: {
            color: "#EB9100",
            width: 2

          }

        }

      }
      $scope.layers.push(obj);
    }

  }
  function countMarker(trolleyArray) {
    var polygonConter = 0;
    for (k = 0; k < geoJsonArray.length; k++) {
      var markerCounter = 0;
      polygonConter++;
      var coordinate = geoJsonArray[k].object.features[0].geometry.coordinates[0];
      var lat = coordinate[0][0], lan = coordinate[0][1];
      var minLat = 0;
      maxLat = 0;
      minLan = 0;
      maxLan = 0;
      for (i = 0; i < coordinate.length; i++) {
        //latitude
        if (coordinate[i][0] > maxLat) {
          maxLat = coordinate[i][0];
        }
        if (coordinate[i][0] < lat) {
          minLat = coordinate[i][0];
          lat = minLat;
        }
        else {
          minLat = lat;
        }

        if (coordinate[i][1] > maxLan) {
          maxLan = coordinate[i][1];
        }

        if (coordinate[i][1] < lan) {
          minLan = coordinate[i][1];
          lan = minLan;
        }
        else {
          minLan = lan;
        }
      }
      for (j = 0; j < trolleyArray.length; j++) {
        var markerLat = trolleyArray[j].x, markerlan =16000-trolleyArray[j].y;
        if ((markerLat >= minLat && markerLat <= maxLat) && (markerlan >= minLan && markerlan <= maxLan)) {
          markerCounter++;
        }
        $scope.trolliesInRegionCount=markerCounter;
      }

      settingService.getThresholdPolygonlatLong().then(function (result) {
        trolleyCountData = result.data.threshold;
      })
      $scope.trolleysInRegion = markerCounter;
    }
  }
  trolleyService.getTrolleyDataFOrFirstTime();
  settingService.getThresholdPolygonlatLong().then(function (result) {
    $scope.trolleyThreshold = result.data.threshold;
  })
  $scope.$on('$destroy', function (event) {
    socket.removeListener('updatedTrolleyLocation');
    console.log("Stopped All Listeners in Trolley ctrl")
  });
  $scope.$on("NotificationToggled",function(data){
    console.log("Recived  NotificationToggled")
    olData.getMap().then(function(map) {
      setInterval(function(){
        map.updateSize();
      }, 250)
    })
  })
});




















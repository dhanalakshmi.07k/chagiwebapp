changiControllers.controller("notificationController",function($scope,$http,settingService,analyticsService,authInterceptor,mapService,$state,notificationService, $rootScope,olData,$location) {

    $scope.currentNav= function (path) {
        var loc=$location.path();
        $scope.flowType="fullScreen";

        if(loc.includes('content')){
            $scope.flowType="freeFlowMain"
        }

        return loc.includes(path)
    };

    //.... x and y values for real time vehicle analytics graph area graph....
  $scope.messages = [];
  $rootScope.$on(notificationService.notificationEvents.NEW_NOTIFICATION_ARRIVED, function(event, notification){
    $scope.messages = notificationService.getNotifications();
  })

  /*----------------------------Function Related in Updating Notification---------------------------*/
   /* socket.on('notificationMsg', function (data) {
      if($scope.messages.length<=50){
        $scope.messages.unshift(data);
      }
      else{
        $scope.messages.pop();
      }
      $scope.$apply();
      notificationPlotOnMapEmitter(data);
    });
  */

  $scope.mapResize= function () {
   // var laneMap=mapService.getMapObject();
    //console.log(laneMap)
    //laneMap.updateSize();
    console.log("About to broadcast NotificationToggled")
    $scope.$broadcast("NotificationToggled")
  }
  $scope.clearMapData = function(){
  notificationService.clearNotification();
  $scope.messages =[];
}
  $scope.notifyTheNotification = function(msg){
    var message;
    if(msg.notificationType=="STOPPED_VEHICLE"){
       message = "Vehicle Stopped at lane "+msg.laneIndex+" of Zone"+msg.zoneIndex;
    }else{

      message = "Number of trolley in the Region 1 "+msg.trolleyCount+" that is below defined threshold "+msg.thershold;
    }
    notificationService.sendSmsNotification(message);
  }
/*  socket.on('trolleyNotification', function(data) {
    console.log(data);
    $scope.messages.unshift(data);
    $scope.$apply();
  });*/

  $scope.notifyTrolleyNotification = function(msg){
    var message = "trolley threshold reached";
    notificationService.sendSmsNotification(message);
  }

  /*
  function notificationPlotOnMapEmitter(data){
    $scope.$broadcast('notificationPlotDetails',data);
  }
*/
})




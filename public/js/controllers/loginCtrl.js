/**
 * Created by Suhas on 12/5/2015.
 */

changiControllers.controller('loginCtrl', function ($scope,$http,$window,$location,$state,settingService) {
  $scope.checkValidation = function (user) {
    $scope.loginLoader=true;
    $http
      .post('/login', user)
      .success(function (data, status, headers, config) {
        /*console.log(data.token);*/
        $window.sessionStorage.token = data.token;
        $state.go('traffic.laneMap');

      })
      .error(function (data, status, headers, config) {
        // Erase the token if the user fails to log in
          $scope.loginLoader=false;
        delete $window.sessionStorage.token;
        $scope.isAuthenticated = false;
        // Handle login errors here
        $scope.error = 'Error: Invalid user or password';
        $scope.welcome = '';
      });
  };

  $scope.logout = function () {
    delete $window.sessionStorage.token;
    console.log( $window.sessionStorage.token);
    $state.go('userPage.login')
    settingService.cancelMapDrawSimulation();
    /*This line o*/
    settingService.cancelvehicleSimulation();
    settingService.cancelLineChartvehicleSimulation();
    //settingService.cancelLaneWiseDataSimulation();
    //settingService.cancelZoneWiseDataSimulation();
    settingService.setMapDrawSimulation(false);
    settingService.setNotificationSimulation(false);
    settingService.setVechicleDrawSimulation(false);
    settingService.setLineChartvehicleDrawSimulation(false);
    settingService.setLaneWiseDataSimulation(false);
    settingService.setZoneWiseDataSimulation(false);
    /*this will stop all the simulations related to Real Time Lane chart for selected zone */
    settingService.cancelAllRealTimeLaneChartSimulator();
    /*this will stop all the simulations related to dials*/
    settingService.cancelAllRealTimeSimulatorLaneAndZoneWiseDataSimulation();
  };
  $scope.signUp = function (user) {
    console.log("_____inside login_____________");
    console.log(user);
    $http
      .post('/signup', user)
      .success(function (data, status, headers, config) {
        $scope.successMessage="Successfully registered Please login";
        $state.go('userPage.login');
      })
      .error(function (data, status, headers, config) {
        $scope.error =data;
      });
  };
  //console.log(socket)
  var counter = 0;
  socket.on('test11',function(data){
   console.log("hello")
   console.log(data)
   });
  console.log(socket);
});


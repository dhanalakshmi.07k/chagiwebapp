/**
 * Created by zendynamix on 19-01-2016.
 */
changiControllers.controller('openLayerMap', function ($scope, notificationService,
               $rootScope, $sce, olData, mapService,$state) {
  $scope.cameras = [];
  $scope.camerasPlotDetails = [];
  $scope.notificaion = [];
  var vectorLayer;
  var vectorSource;
  var mapData = {
    zoneData: {
      levelOfServiceData: [0, 0, 0]
    },
    notificationCoordinates: {
      coordinates: []
    }
  };
  var trustedInputHtml = function(){
    return $sce.trustAsHtml('<video class="video" id="camVideo" src="videos/lane1.mp4" type="video/mp4" muted loop controls width="250" height="250" preload="none"></video>');
  }
  function addCamera() {
    var camerasPlotDetail=[];
    mapService.getCameraMarkerDetails().then(function(res){
      for(var k=0;k<res.data[0].cameraMarkerArray.length;k++){
        var obj={};
        obj.lat=res.data[0].cameraMarkerArray[k].lat;
        obj.lng=res.data[0].cameraMarkerArray[k].lng;
        camerasPlotDetail.push(obj);
      }
      mapService.setCameraPlotArray(camerasPlotDetail);
      var custom_style = {
        image: {
          icon: {
            anchor: [0.5, 1],
            anchorXUnits: 'fraction',
            anchorYUnits: 'fraction',
            opacity: 0.90,
            src: 'images/Cam_Right2.png'
          }
        }
      };
      var markers =   mapService.getCameraPlotArray();
      for (var i = 0; i < markers.length; i++) {
        var objMap = {
          lat: markers[i].lat,
          lon: markers[i].lng,
          label: {
            message: trustedInputHtml(),
            show: false,
            showOnMouseClick: true
          },
          style: custom_style
        };
        $scope.cameras.push(objMap);
      }
    })
  }
  function addNotification(lat, lng, incidentImage, segmentInLane) {
    var customNotificatonStyle = {
      image: {
        icon: {
          anchor: [0.5, 1],
          anchorXUnits: 'fraction',
          anchorYUnits: 'fraction',
          opacity: 0.90,
          src: 'images/StoppedCarSmall.png'
        }
      }
    };
    var notificationImage = "data:image/png;base64," + incidentImage;
    var objMap = {
      lat: lat,
      lon: lng,
      label: {
        message: '<img src="' + notificationImage + '" width="300" height="auto"/>',
        show: false,
        showOnMouseClick: true
      },
      style: customNotificatonStyle
    };
    if ($scope.notificaion.length > 10) {
      $scope.notificaion.pop();
      $scope.notificaion.push(objMap);
    } else {
      $scope.notificaion.push(objMap);
    }

  }
  var geoJsonArray =mapService.geoJsonArray;
  $scope.layerArray = mapService.layerArray;
  var color = ['#61ae33', '#61ae33', '#61ae33']
  function addPolylineColor(color) {
    $scope.layerArray= mapService.layerArray;
    for (var j = 0; j < geoJsonArray.length; j++) {
      var obj = {
        source: {
          type: 'GeoJSON',
          geojson: geoJsonArray[j]
        },
        style: {
          stroke: {
            color: color[j],
            width: 6
          }
        }
      }
      $scope.layerArray.push(obj);
    }

  }

  function getZoneSegmentInLane(){
    mapService.getZoneSegmentInLaneDetails().then(function(res){
      for(var n=0;n<res.data.length;n++){
        mapData.notificationCoordinates.coordinates.push(res.data[n].zoneSegmentArray)
      }
      initialLoadingOfMarkers();
    })
  }
  //declaration of map
  angular.extend($scope, {
    center: {
      lat: 1.353199,
      lon: 103.986908,
      zoom: 16.5
    },
    view: {
      rotation: 68 * Math.PI / 180
    },
    layers: $scope.layerArray
  });
  //getting the map to perfrom reszie of map
  var mapObject = olData.getMap();
  var cleanUpFunc1 = $scope.$on("NotificationToggled",function(data){
    mapResize();
  })
  function mapResize(){
    olData.getMap().then(function(map) {
      setInterval(function(){
        map.updateSize();

      }, 150)
      //map.updateSize();
    })
  }
  $scope.mapResize= function () {
    olData.getMap().then(function(map) {
      setInterval(function(){
        map.updateSize();

      }, 150)

    })
  }
  var previousLOSValue = 0;
  var cleanUpFunc2 = $rootScope.$on(notificationService.notificationEvents.NEW_NOTIFICATION_ARRIVED, function (event, notificationData) {
    if($state.is('traffic.analytics') || $state.is('traffic.laneMap')){
      if (notificationData.notificationType != "trolley" && mapData.notificationCoordinates.coordinates) {
        var zoneIndex = notificationData.zoneIndex;
        var segmentInLane = notificationData.segmentInLane;
        var incidentImage = notificationData.snapShot;
        var lat = mapData.notificationCoordinates.coordinates[zoneIndex - 1][segmentInLane - 1].lat;
        var lng = mapData.notificationCoordinates.coordinates[zoneIndex - 1][segmentInLane - 1].lng;
        addNotification(lat, lng, incidentImage, segmentInLane);
        mapResize();
      }
    }
  });
  function initialLoadingOfMarkers(){
    var notificationArray = notificationService.getNotifications();
    var length;
    if(notificationArray.length>=10){
      length=5
    }else{
      length=notificationArray.length;
    }
    for(var j=0;j<length;j++){
      var notifyObj = notificationArray[j]
      if(notifyObj.notificationType!="trolley"){
        var zoneIndex = notifyObj.zoneIndex;
        var segmentInLane = notifyObj.segmentInLane;
        var incidentImage = notifyObj.snapShot;
        var lat = mapData.notificationCoordinates.coordinates[zoneIndex - 1][segmentInLane - 1].lat;
        var lng = mapData.notificationCoordinates.coordinates[zoneIndex - 1][segmentInLane - 1].lng;
        addNotification(lat, lng, incidentImage, segmentInLane);
        if(j==length-1){
          mapResize();
        }
      }
    }
  }
  socket.on("losDataForWholeZone", function (data) {
    if($state.is("traffic.laneMap")) {

      var losArray = [];
      for (var k = 0; k < data.length; k++) {
        losArray.insert(data[k]._id, data[k].levelOfService);
      }

      var losColorArray = ['#61ae33', '#adff2f', '#ffff00', '#ffa500', '#ff0000']
      var colorArray = ['#61ae33', '#61ae33', '#61ae33'];
      for (var p = 0; p < losArray.length; p++) {
        var los = losArray[p];
        colorArray.splice(p, 1);
        colorArray.insert(p, losColorArray[los]);
      }
      addPolylineColor(colorArray);
      $scope.$apply();
    }
  })
  socket.on("dialDataForWholeZone", function (data) {
    if($state.is('traffic.analytics')) {
      var losColorArray=['#61ae33','#adff2f','#ffff00','#ffa500','#ff0000']
      var colorArray=['#61ae33', '#61ae33', '#61ae33'];
      for(var p=0;p<data.levelServiceArray.length;p++){
        var los = data.levelServiceArray[p];
        colorArray.splice(p, 1);
        colorArray.insert(p,losColorArray[los]);
      }
      addPolylineColor(colorArray);
      $scope.$apply();
    }
  })
  $rootScope.$on("clearMapNotificationMarker", function (event, notificationData) {
    $scope.notificaion = []
  })
  $scope.$on('$destroy', function (event){
    socket.removeListener('dialDataForWholeZone');
    socket.removeListener('losDataForWholeZone');
    cleanUpFunc2();
    cleanUpFunc1();

  });
  Array.prototype.insert = function (index, item) {
    this.splice(index, 0, item);
  };
  function init(){
    getZoneSegmentInLane();
    addCamera();
  }
  init();

});


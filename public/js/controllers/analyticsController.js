//..... Analytics controller.....
changiControllers.controller("analytics", function ($scope, analyticsService, settingService
  , $rootScope, $state, olData,configService) {

    //..... Wow js init......
    new WOW().init();

  $scope.realTimeGraphFeautures = {/*$scope.realTimeGraphFeautures.lanesSelectedForRTGraph*/
    typeSelected: "all",
    seconds: 300,
    selectedZone: 1,
    selectedLane: 1,
    flowName: "Volume",
    metrics: {
      occupancy: 0,
      volume: 0,
      speed: 0,
      timeHeadway: 0,
      spaceHeadway: 0
    },
    laneGraphChart: {
      type: "speed",
      zone: 2,
      selectedDropdown: {
        selectedZone: "Zone 2",
        selectedType: "Speed"
      }
    },
    dial: {
      metrics: {
        flow: {
          units: "vehicles/5 min"
        }
      },
      selectedDropdown: {
        selectedRefreshTime: " 5 minutes"
      },
      loader: false
    },
    levelOfService: [
      ["normal", "normal", "normal", "normal", "normal"],
      ["normal", "normal", "normal", "normal", "normal"],
      ["normal", "normal", "normal", "normal", "normal"]
    ],
    vehicleAnalyticsCardSubscriberName:"t130",
    realTimeDataStatus:true,
    historicalGraphDetails:{
      unitSelected:60,
      loader:false
    },
    graphTypeSelected:"Spline",
    graphSeriesColors:['#d6e586', '#7edd83', '#f9b343', '#e87294', '#5bc3c2'],
    lanesSelectedForRTGraph:[1,2,3,4,5]
  }
    /*loader scope Values for graphs*/
    $scope.loader = {
        vehicleAnalyticsLaneGraph: false,
        wholeZoneAnalyticsZoneGraph: false
    }
    $scope.colors = ['#000000', '#7edd83', '#f9b343', '#e87294', '#5bc3c2'];
    $scope.xAxisFull = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];





    /*For historical-graph lane chart*/
    /*$scope.laneYAxis1 = [{
        name: 'Lane 1',
        data: [67, 87, 98, 65, 65, 76, 87, 78, 87, 54, 42, 54, 67, 87, 98, 65, 65, 76, 87, 78, 87, 54, 42, 54],
        color: $scope.colors[0]
    }];
    $scope.laneYAxis2 = [{
        name: 'Lane 2',
        data: [67, 87, 98, 65, 65, 76, 87, 78, 87, 54, 42, 54, 67, 87, 98, 65, 65, 76, 87, 78, 87, 54, 42, 54],
        color: $scope.colors[1]
    }];
    $scope.laneYAxis3 = [{
        name: 'Lane 3',
        data: [67, 87, 98, 65, 65, 76, 87, 78, 87, 54, 42, 54, 67, 87, 98, 65, 65, 76, 87, 78, 87, 54, 42, 54],
        color: $scope.colors[2]
    }];
    $scope.laneYAxis4 = [{
        name: 'Lane 4',
        data: [67, 87, 98, 65, 65, 76, 87, 78, 87, 54, 42, 54, 67, 87, 98, 65, 65, 76, 87, 78, 87, 54, 42, 54],
        color: $scope.colors[3]
    }];
    $scope.laneYAxis5 = [{
        name: 'Lane 5',
        data: [67, 87, 98, 65, 65, 76, 87, 78, 87, 54, 42, 54, 67, 87, 98, 65, 65, 76, 87, 78, 87, 54, 42, 54],
        color: $scope.colors[4]
    }];*/
  /*Analytics Configuration*/



    $scope.mapInit = function () {
        var mapOptions = {
            center: {lat: 1.35, lng: 103.987},
            scrollwheel: false,
            zoom: 17
        };
        var map = new google.maps.Map(document.getElementById('laneMap'), mapOptions);
    };
    /*  $scope.mapInit();*/



    /******************************************************************************RT-Graph Data**********************************************************************************************************************/
    /*RT-Graph Data*/

    /******************************************************************************RT-Graph DIALS**********************************************************************************************************************/


  /*require to filter data coming from socket to update solid guage data*/
  $scope.filters = {
    wholeZoneAnalytics: {
      index: 1,
      criteria: 'allZones',
      data: ''
    }
  }

  var analyticsConfiguration={
    terminalDetails:{
      timeSelected:$scope.realTimeGraphFeautures.seconds,
      terminalSelected:$scope.activeTerminal,
    },
    realTimeGraphDetails:{
      parameterSelected:$scope.realTimeGraphFeautures.laneGraphChart.type,
      zoneSelected:$scope.realTimeGraphFeautures.laneGraphChart.zone
    }
  }
  /*----------------------------Function Related in getting metrics data dependings whether zone ,lane or both selected   (FOR DIALS)----------------------------*/
    $scope.setSecondsForRealTimeData = function (seconds) {
        /*settingService.setLineChartvehicleDrawSimulation(true);*/
        $scope.realTimeGraphFeautures.dial.loader = true;
        $scope.realTimeGraphFeautures.seconds = seconds;
        settingService.setSelectedSecondsForRealTimeData(seconds);
        if (seconds === 30) {
            $scope.realTimeGraphFeautures.flowName = "Volume"
            $scope.realTimeGraphFeautures.dial.selectedDropdown.selectedRefreshTime = "Now (30 sec)";
            $scope.realTimeGraphFeautures.dial.metrics.flow.units = "vehicles/30 sec";
        }
        else if (seconds === 300) {
            $scope.realTimeGraphFeautures.flowName = "Volume"
            $scope.realTimeGraphFeautures.dial.selectedDropdown.selectedRefreshTime = "5 Minutes";
            $scope.realTimeGraphFeautures.dial.metrics.flow.units = "vehicles/5 min";
        }
        else if (seconds === 900) {
            $scope.realTimeGraphFeautures.flowName = "Volume"
            $scope.realTimeGraphFeautures.dial.selectedDropdown.selectedRefreshTime = "15 Minutes";
            $scope.realTimeGraphFeautures.dial.metrics.flow.units = "vehicles/15 min";
        }
        else if (seconds === 1800) {
            $scope.realTimeGraphFeautures.flowName = "Volume"
            $scope.realTimeGraphFeautures.dial.selectedDropdown.selectedRefreshTime = "30 Minutes";
            $scope.realTimeGraphFeautures.dial.metrics.flow.units = "vehicles/30 min";
        }
        else if (seconds === 3600) {
            $scope.realTimeGraphFeautures.flowName = "Flow"
            $scope.realTimeGraphFeautures.dial.selectedDropdown.selectedRefreshTime = "1 Hour";
            $scope.realTimeGraphFeautures.dial.metrics.flow.units = "vehicles/hour";
        }
      setConfigurationForAnalytics();
       /* analyticsService.getRealTimeVehicleDetectionDataOfAllZones(seconds);
        analyticsService.getRealTimeLevelOfServiceDataForSelectedLastSeconds(seconds);
        analyticsService.getRealTimeWholeZoneAnalyticsDataOfAllZones(seconds);
        /!*New*!/
        analyticsService.setSecondsForRealTimeAnalyticsDials(seconds);
        analyticsService.getTheAnalyticsForRTDials();*/

    }
    /*starts fetching data for Dials of vehicle-detection*/
    $scope.fetchRealTimeDataForAllZoneAndLane = function () {
        var flag = true;
        settingService.startLineChartvehicleSimulation(flag);
    }
    $scope.fetchRealTimeDataForAllZoneAndLane();
    $scope.realTimeGraphFeautures.dial.loader = true;

    /*-------------------------------------------------new-------------------------------------------------*/
    $scope.RTGraph = {
        speed: 0,
        occupancy: 0,
        los: 0,
        volume: 0,
        timeHeadway: 0,
        spaceHeadway: 0,
        losClass:"los1"
    }
    $scope.analyticsSubTab = 2;
    $scope.analyticsType = function (tabNUm) {
        $scope.analyticsSubTab = tabNUm;
        //{
        //  selected:{
        //    realTime:true,
        //      historical:false,
        //      predictive:false
        //  }
        //}

    }
    $scope.activeTerminal = 1;
    $scope.setTerminalForRTDials = function (terminal) {
        $scope.realTimeGraphFeautures.dial.loader = true;
        $scope.activeTerminal = terminal;
        setConfigurationForAnalytics();
    }
    setConfigurationForAnalytics();/*vehicleCardData*/
    socket.on("vehicleCardData", function (data) {
        $scope.realTimeGraphFeautures.dial.loader = false;
        setDialsValueForVehicle(data);
        $scope.$apply();
    })
    socket.on("wholeZoneCardData", function (data) {
      /*$scope.realTimeGraphFeautures.dial.loader = false;*/
      setDialsValueForWholeZone(data);
      $scope.$apply();
    })
    /*socket.on("t130", function (data) {
    /!*$scope.realTimeGraphFeautures.dial.loader = false;*!/
    console.log("new data");
    console.log(data);
    $scope.$apply();
  })*/
    var losColorClass=['los0','los1','los2','los3','los4'];
    function setDialsValueForWholeZone(val) {
        var dialData = val;
        $scope.RTGraph.occupancy = Math.round(dialData.occupancy);
        $scope.RTGraph.los = Math.round(dialData.levelOfService);
        $scope.losClass = losColorClass[$scope.RTGraph.los];
    }
    function setDialsValueForVehicle(val) {
        var dialData = val;
        $scope.RTGraph.speed = Math.round(dialData.speed);
        $scope.RTGraph.timeHeadway = Math.round(dialData.timeHeadway);
        $scope.RTGraph.spaceHeadway = Math.round(dialData.spaceHeadway);
        $scope.RTGraph.volume = Math.round(dialData.volume);
    }
    function init() {
      /*setConfigurationForAnalytics()*/
     /* analyticsService.getTheAnalyticsForRTDials();
      analyticsService.startGettingDataForRealTimeDials();
      analyticsService.setSecondsForRealTimeAnalyticsDials(30);
      analyticsService.setTheTerminalSelectedForRTDials(1);*/
      /*analyticsService.setTypeForRTGraph("speed");
      analyticsService.setZoneIdForRTGraph(2);*/
      $scope.losClass='los0';
      updateSeriesByConfiguration();
      console.log("Init function of Analytics started")
    }
    init();
    /*setInterval(function() {
      setConfigurationForAnalytics();
    },5000);*/
    function setConfigurationForAnalytics(){
      var publisherName = $scope.realTimeGraphFeautures.vehicleAnalyticsCardSubscriberName;
      /*destroyPreviousSocket(publisherName);*/
      $scope.realTimeGraphFeautures.vehicleAnalyticsCardSubscriberName=
        "t"+$scope.activeTerminal+""+$scope.realTimeGraphFeautures.seconds;
      analyticsConfiguration={
      terminalDetails:{
        timeSelected:$scope.realTimeGraphFeautures.seconds,
        terminalSelected:$scope.activeTerminal,
        previousRoom:publisherName
      },
      realTimeGraphDetails:{
        parameterSelected:$scope.realTimeGraphFeautures.laneGraphChart.type,
        zoneSelected:$scope.realTimeGraphFeautures.laneGraphChart.zone,
        laneWisDataCurrent:[
          {val:0,time:''},
          {val:0,time:''},
          {val:0,time:''},
          {val:0,time:''},
          {val:0,time:''}
        ]
      }
    }
      socket.emit("setAnalyticsConfiguration",analyticsConfiguration)
  }
    socket.on("getTerminalValue", function (data) {
    })
    socket.on("roadAnalyticsTest", function (data) {
      console.log("curr")
      console.log(data)
    })
    $scope.$on('$destroy', function (event) {
        socket.removeListener('vehicleDetectionEventNotifier');
        socket.removeListener('dialDataForVehicle');
        socket.removeListener('dialDataForWholeZone');
        socket.removeListener('levelOfServiceForMetrics');
        socket.removeListener('solidGuageAnalyticsData');
        socket.removeListener('analyticsData');
        settingService.cancelAllRealTimeLaneChartSimulator();
        settingService.cancelAllRealTimeSimulatorLaneAndZoneWiseDataSimulation();
        analyticsService.stopGettingDataForRealTimeForRTDials();
        analyticsService.setSecondsForRealTimeAnalyticsDials(30);
        analyticsService.setTheTerminalSelectedForRTDials(1);
        console.log("Stopped All Listeners in Analytics ctrl")
    });
   /* function destroyPreviousSocket(publisherName){
        /!*socket.removeListener(publisherName);
        console.log("removed socket        "+publisherName)
        socket.removeListener("t130");*!/
    }*/

  /*HIstorical Data*/
  /***************************************************RT_GRAPH******************************************************/

  $scope.xAxis = [];
  var data = [];
  var Time = new Date();
  Time.setSeconds(Time.getSeconds() - 60);
  for (var j = 0; j < 12; j++) {
    var minute = Time.getMinutes();
    var seconds = Time.getSeconds();
    Time.setSeconds(seconds + 5);
    if (seconds < 10) {
      seconds = "0" + seconds;
    }
    if (minute < 10) {
      minute = "0" + minute;
    }
    var time = Time.getHours() + ":" + minute + ":" + seconds;
    $scope.xAxis.push(time)
    data.push(0)
    if (j == 79) {

    }
  }
  $scope.yAxis = [
    {
      name: 'Lane1',
      data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      colour:$scope.realTimeGraphFeautures.graphSeriesColors[0]
    }, {
      name: 'Lane2',
      data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      colour:$scope.realTimeGraphFeautures.graphSeriesColors[1]
    }, {
      name: 'Lane3',
      data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      colour:$scope.realTimeGraphFeautures.graphSeriesColors[2]
    }, {
      name: 'Lane4',
      data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      colour:$scope.realTimeGraphFeautures.graphSeriesColors[3]
    }
  ];

  /*****************Event Notifier*****************/
  socket.on("vehicleDetectionEventNotifier", function (data){
    if($state.is("traffic.analytics") && $scope.realTimeGraphFeautures.realTimeDataStatus) {
      if($scope.realTimeGraphFeautures.laneGraphChart.type!='occupancy'){
        $scope.realTimeAnalyticsGraphUpdater12(data);
      }
      $scope.$apply();
    }
  })
  socket.on("wholeZoneVehicleDetectionEventNotifier", function (data){
    if($state.is("traffic.analytics") && $scope.realTimeGraphFeautures.realTimeDataStatus) {
      if($scope.realTimeGraphFeautures.laneGraphChart.type=='occupancy'){
        $scope.realTimeAnalyticsGraphUpdater12(data);
        $scope.$apply();
      }
    }
  })

  $scope.realTimeAnalyticsGraphUpdater12 = function (val) {
    var chart = $("#realTimeChartGraph .areaSpline").highcharts();
    if (chart) {
      var Time = new Date();
      var seconds = Time.getSeconds();
      var minute = Time.getMinutes();
      if (seconds < 10) {
        seconds = "0" + seconds;
      }
      if (minute < 10) {
        minute = "0" + minute;
      }
      var time = Time.getHours() + ":" + minute + ":" + seconds;
      if(chart.series.length == val.length){
        updatingSeries(chart,val,time)
      }else{
        var chartLength = chart.series.length;
        for (var i = 0; i < chartLength; i++) {
          while(chart.series.length > 0){
            chart.series[0].remove();
          }
          if(chart.series.length==0){
            for(var j=0;j<val.length;j++){
              var name = j+1;
              var seriesName = 'Lane'+name;
              chart.addSeries({name:seriesName,data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]});
              if(j==val.length-1){
                updatingSeries(chart,val,time);
              }
            }
          }
        }
      }
    }
    else {
      console.log("chart not defined")
    }

  }
  function updatingSeries(chartObject,dataObj,time){
    var chart = chartObject;
    var val = dataObj;
    for (var i = 0; i < chart.series.length; i++) {
      if(!val[i]){
        chart.series[i].remove();
      }else{
        var data = val[i].val;
        var chartObj = chart.series[i];
        $scope.realTimeGraphFeautures.lanesSelectedForRTGraph[i] = chart.series[i].visible
        chartObj.addPoint([time, parseInt(data.toFixed(2))], true, true);
        if(chartObj.data[11]){
          chartObj.data[11].update({actualTime: val[i].time});
        }
      }
    }
  }
  function updateSeriesByConfiguration(){
    configService.getMaxLaneNoOfSelectedZone($scope.realTimeGraphFeautures.laneGraphChart.zone).then(function(result){
      var maxLane = result.data.maxLane;
      var chart = $("#realTimeChartGraph .areaSpline").highcharts();
      var flagForRmv = true;
      if (chart) {
        var chartLength = chart.series.length;
        for (var i = 0; i < chartLength; i++) {
          while(chart.series.length > 0 && flagForRmv){
            chart.series[0].remove();
          }
          if(chart.series.length==0){
            flagForRmv = false;
            var dataArray = new Array(12);
            dataArray.fill(null)
            for(var j=0;j<maxLane;j++){
              var name = j+1;
              var seriesName = 'Lane'+name;
              chart.addSeries({name:seriesName,data: dataArray,color:$scope.realTimeGraphFeautures.graphSeriesColors[j]});
              if(j==maxLane-1){
                setSelectedGraphTypeForRT();
              }
            }
          }
        }
      }
    })
  }
  $scope.clearChartData = function () {
    var chart = $("#realTimeChartGraph .areaSpline").highcharts();
    if (chart) {
      var type = $scope.realTimeGraphFeautures.laneGraphChart.type;
      var title = '';
      if (type == "speed") {
        title = "Speed (km/hr)";
        chart.yAxis[0].setTitle({text: title});
      } else if (type == "timeHeadway") {
        title = "Time Headway (sec)";
        chart.yAxis[0].setTitle({text: title});
      } else if (type == "spaceHeadway") {
        title = "Space Headway (mtr)";
        chart.yAxis[0].setTitle({text: title});
      } else if (type == "volume") {
        title = "Volume (vehicles/hr)";
        chart.yAxis[0].setTitle({text: title});
      } else if (type == "occupancy") {
        title = "Occupancy (%)";
        chart.yAxis[0].setTitle({text: title});
      }
      var val = new Array(12);
      val.fill(null)
      for (var i = 0; i < chart.series.length; i++) {
        chart.series[i].show();
        chart.series[i].setData(val);
        if(i==chart.series.length-1){
          /*setSelectedGraphTypeForRT();*/
        }
      }
    }
  }
  $scope.setTypeOfVehicleAnalyticsDataForRealTimeGraph = function (type) {
    setSelectedGraphTypeForRT();
    $scope.realTimeGraphFeautures.laneGraphChart.type = type;
    $scope.clearChartData();
    /*var zone = $scope.realTimeGraphFeautures.laneGraphChart.zone;*/
    if (type == "speed") {
      $scope.realTimeGraphFeautures.laneGraphChart.selectedDropdown.selectedType = "Speed";
    }
    else if (type == "volume") {
      $scope.realTimeGraphFeautures.laneGraphChart.selectedDropdown.selectedType = "Volume";
    }
    else if (type == "timeHeadway") {
      $scope.realTimeGraphFeautures.laneGraphChart.selectedDropdown.selectedType = "Time Headway";
    }
    else if (type == "spaceHeadway") {
      $scope.realTimeGraphFeautures.laneGraphChart.selectedDropdown.selectedType = "Space Headway";
    }
    else if (type == "occupancy") {
      $scope.realTimeGraphFeautures.laneGraphChart.selectedDropdown.selectedType = "Occupancy";
    }
    if (type == "occupancy") {
      /*settingService.cancelAllRealTimeLaneChartSimulator();
       settingService.startRealTimeLaneChartSimulatorToGetWholeZoneAnalyticsData();
       analyticsService.getRealTimeWholeAnalyticsDataForSelectedLanesForLaneGraphChart(zone,selectedSeconds);*/
    }
    else {
      /*settingService.cancelAllRealTimeLaneChartSimulator();
       settingService.startRealTimeLaneChartSimulator();
       analyticsService.getRealTimeDataForRoadAnalyticsForSelectedZoneDurationAndType(zone,selectedSeconds,type);*/
    }
    /*analyticsService.setTypeForRTGraph(type);*/
    if($scope.realTimeGraphFeautures.realTimeDataStatus){
      updateSeriesByConfiguration();
    }else{
      $scope.historicalLaneWiseData($scope.realTimeGraphFeautures.historicalGraphDetails.unitSelected);
    }
    setConfigurationForAnalytics();
  }
  $scope.setZoneOfVehicleAnalyticsDataForRealTimeGraph = function (zone) {
    /*$scope.chartTypeUpdate('areaspline','.currentGraph','.areaSpline')*/
    setSelectedGraphTypeForRT();
    var selectedType = $scope.realTimeGraphFeautures.laneGraphChart.type;
    $scope.realTimeGraphFeautures.laneGraphChart.zone = zone;
    $scope.clearChartData();
    $scope.realTimeGraphFeautures.laneGraphChart.selectedDropdown.selectedZone = "Zone " + zone;
    /*analyticsService.setZoneIdForRTGraph(zone);*/
    setConfigurationForAnalytics();
    if($scope.realTimeGraphFeautures.realTimeDataStatus){
      updateSeriesByConfiguration();
    }else{
      $scope.historicalLaneWiseData($scope.realTimeGraphFeautures.historicalGraphDetails.unitSelected);
    }
  }

  /****Method to change type of graph(etc column,sparkline etc)****/
  $scope.chartTypeUpdate = function (type, container, graph) {
    //var chart=$(container).find(graph).highcharts();
    var chart = $(container).find(graph).each(function () {
      var chart = $(this).highcharts();
      var seriesLen = chart.series.length;
      for (i = 0; i < seriesLen; i++) {
        chart.series[i].update({
          type: type
        });
      }
    });

    /*chart.series[1].update({
     type: type
     });
     chart.series[2].update({
     type: type
     });
     chart.series[3].update({
     type: type
     });*/
  };

  /****************historical data****************/
  $scope.historicalLaneWiseData = function(val){
    clearRTGraphDetails();
    $scope.realTimeGraphFeautures.historicalGraphDetails.unitSelected = val;
    var data = {
        zoneId :$scope.realTimeGraphFeautures.laneGraphChart.zone,
        parameterSelected:$scope.realTimeGraphFeautures.laneGraphChart.type,
        seriesSelected:$scope.realTimeGraphFeautures.historicalGraphDetails.unitSelected,
        startDate:new Date()
    }
    if($scope.realTimeGraphFeautures.laneGraphChart.type!='occupancy'){
        analyticsService.getVehicleHistoricalDataSecondWiseData(data).
        then(function(result){
          constructHistoricalChart(result.data)
          console.log(result.data)
          $scope.realTimeGraphFeautures.historicalGraphDetails.loader = false;
          setSelectedGraphTypeForRT();
        })
    }else{
      data.parameterSelected = 'zoneOccupancy';
        analyticsService.getWholeZoneHistoricalData(data).then(function(result){
          constructHistoricalChart(result.data)
          console.log(result.data)
          $scope.realTimeGraphFeautures.historicalGraphDetails.loader = false;
          setSelectedGraphTypeForRT();
        })
    }
    stopRealTime();
  }
  $scope.startRealTime = function(){
    generateXAxisForRT()
    $scope.realTimeGraphFeautures.realTimeDataStatus = true;
    $scope.setZoneOfVehicleAnalyticsDataForRealTimeGraph($scope.realTimeGraphFeautures.laneGraphChart.zone)
  }
  function generateXAxisForRT(){
    var data = [];
    var Time = new Date();
    Time.setSeconds(Time.getSeconds() - 60);
    for (var j = 0; j < 12; j++) {
      var minute = Time.getMinutes();
      var seconds = Time.getSeconds();
      Time.setSeconds(seconds + 5);
      if (seconds < 10) {
        seconds = "0" + seconds;
      }
      if (minute < 10) {
        minute = "0" + minute;
      }
      var time = Time.getHours() + ":" + minute + ":" + seconds;
      data.push(time)
      if (data.length == 11) {
        var chart = $("#realTimeChartGraph .areaSpline").highcharts();
        if (chart) {
          chart.xAxis[0].setCategories(data);
        }
      }
    }
  }
  function stopRealTime(){
    $scope.realTimeGraphFeautures.realTimeDataStatus = false;
    $scope.realTimeGraphFeautures.historicalGraphDetails.loader = true;
    clearRTGraphDetails();
    $scope.clearChartData();
  }
  function clearRTGraphDetails(){
    var chart = $("#realTimeChartGraph .areaSpline").highcharts();
    if (chart) {
      var chartLength = chart.series.length;
      for (var i = 0; i < chartLength; i++) {
        while (chart.series.length > 0) {
          chart.series[0].remove();
        }
        if(chart.series.length==0){
          /*chart.redraw();*/
          var emptyArray = [];
          chart.xAxis[0].setCategories(emptyArray);
        }
      }
    }
  }
  function constructHistoricalChart(historicalChartData){
    var categoryArray = [];
    var chart = $("#realTimeChartGraph .areaSpline").highcharts();
    for(var seriesLth=0;seriesLth<=$scope.realTimeGraphFeautures.historicalGraphDetails.unitSelected;seriesLth++){
      categoryArray[seriesLth]=seriesLth;
      if(categoryArray.length+1==$scope.realTimeGraphFeautures.historicalGraphDetails.unitSelected){

        if (chart) {
          //chart.xAxis[0].setCategories(categoryArray);
          for(var j=0;j<historicalChartData.length;j++){
            var name = j+1;
            var seriesName = 'Lane'+name;
            chart.addSeries({name:seriesName,data: historicalChartData[j],color:$scope.realTimeGraphFeautures.graphSeriesColors[j]});
            if(!$scope.realTimeGraphFeautures.lanesSelectedForRTGraph[j]){
              chart.series[j].hide()
            }
          }
        }
      }
      }

    var l = divideTimeRangeIn60Part($scope.realTimeGraphFeautures.historicalGraphDetails.unitSelected)
    chart.xAxis[0].setCategories(l,true);
    }
  function divideTimeRangeIn60Part(noOfMinutes) {
    var labels=[];
    var interval = 60;
    var totalSeconds = noOfMinutes*60;
    var inc= totalSeconds/interval;
    var rightNow = moment()
    //var nowString = rightNow.format('MMMM Do YYYY, h:mm:ss a');
    var inPast = rightNow.subtract(totalSeconds, 'seconds')

    for (var i = 0; i<interval ; i++){
      var label = inPast.add(inc,'second').format('h:mm:ss')
      /*console.log(`${i}=${label}`);*/
      labels.push(label);

    }
    return labels;

  }
  $scope.setGraphName = function(graphName){
    $scope.realTimeGraphFeautures.graphTypeSelected = graphName;
  }
  function setSelectedGraphTypeForRT(){
    var graph = '';
    if($scope.realTimeGraphFeautures.graphTypeSelected==='Area Spline'){
      graph='areaspline';
    }else if($scope.realTimeGraphFeautures.graphTypeSelected==='Column'){
      graph='column';
    }else if($scope.realTimeGraphFeautures.graphTypeSelected==='Spline'){
      graph='spline';
    }else if($scope.realTimeGraphFeautures.graphTypeSelected==='Spark Line'){
      graph='area';
    }
    $scope.chartTypeUpdate(graph,'.currentGraph','.areaSpline')
  }
});

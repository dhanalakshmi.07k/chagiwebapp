/*var zmq = require('zmq');*/
var express = require('express'),
  config = require('./config/config'),
  glob = require('glob'),
  mongoose = require('mongoose');

mongoose.connect(config.db);
var db = mongoose.connection;
db.on('error', function () {
  throw new Error('unable to connect to database at ' +config.db);
});

var models = glob.sync(config.root + '/app/models/*.js');
models.forEach(function (model) {
  require(model);
});
var app = express();

var server = require('http').Server(app);
//var socketIoServer = require('socket.io')(server);

server.listen(config.port, function () {
  console.log('Express server listening on port ' + config.port);
});


/*
var zmqsock = zmq.socket('sub');
zmqsock.connect('tcp://127.0.0.1:3000');
zmqsock.subscribe('msg');

zmqsock.on("message",function(topic,message ){
  console.log("new message")
  console.log(message.toString());
  io.emit('msg',JSON.parse(message.toString()))
})
*/
require('./config/socketIo').init(server )
require('./config/express')(app,config);
require('./config/zmqPushPull').zmqPushPull(config);
require('./app/ZmqDataPublisher/wholeZoneVechicleAggregationPublisher').startPublish()

/**
 * Created by rranjan on 11/29/15.
 */
var express = require('express'),
  router = express.Router(),
  mongoose = require('mongoose'),
  vehicleAnalytics = mongoose.model('vehicleAnalytics');
  vehicleAnalyticsModel = mongoose.model('wholeZoneTrafficInfo');
  notificationModel = mongoose.model('notificationModel'),
  SocketIo = require('./socketIo').getSocketIoServer(),
  dataHandlers = require("../app/zmqDataHandler");



  var zmq = require('zmq');
  var zmqPushPull = function (config) {
    startPulling("portVehicleAnalyticsByLane", "vehicleAnalyticsData");
    startPulling("portVehicleWholeZoneAnalytics", "vehicleAnalyticsDataWholeZone");
   /* startPulling("portNotification", "NotificationData");*/

    function startPulling(portName, emitPort) {
      var zmqSocket = zmq.socket('pull');
      var zmqPortPart = 'tcp://' + config.zmq.recHost + ':' //+config.zmq.port
      zmqSocket.bindSync(zmqPortPart + config.zmq[portName], function (err) {
        if (err)console.log(err.stack)
      });
      zmqSocket.on("message", function (message) {
        var jsonPayload = JSON.parse(message.toString());
      /*  console.log("**********break*********")
        console.log(jsonPayload)
        console.log("**********break*********")*/
        processData(jsonPayload, portName);
      })
    }
    function processData(message, portName) {
      var handlerObjectName =portName+"Handler"
      var handler = dataHandlers[handlerObjectName];
      handler(message);
  }
  }


  module.exports = {zmqPushPull: zmqPushPull}







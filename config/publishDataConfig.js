var mainConfig = require('./config');


var config = {
      aggregateData:{
        isEnable:true,
         terminals:[1,2,3],
         zmqPubPort : 3006,
         pubSubFilter:'aggregateData',
         aggregationInterval:15000,
         aggregationSpan:1800

      },
      rawData:{
           isEnable:true,
          terminals:[1,2,3],
           zmqPubPort : 3006,
           pubSubFilter:{
             vehicleAnalyticsRawData:"vehicleAnalyticsRawData",
            wholeZoneAnalyticsRawData:"wholeZoneAnalyticsRawData"
           },
           aggregationInterval:15000,
           aggregationSpan:1800
      }

  }



    var zmq = require('zmq')
    var zmqURI = 'tcp://*' + ':'+config.aggregateData.zmqPubPort
    var sock = zmq.socket('pub');
    sock.bind(zmqURI);

    var getZmqPubSocket = function(){
      return sock;
    }



module.exports={config:config, dataPublishZmqSocket:getZmqPubSocket()};

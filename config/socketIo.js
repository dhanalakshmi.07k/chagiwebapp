/**
 * Created by rranjan on 1/15/16.
 */
"use strict";

var socketIoServer;
var httpServer;
var socketConnectionDetailsCollection = require("../app/multipleSocket");
var init = function(server){
    httpServer=server;
    socketIoServer = require('socket.io')(server);
    socketIoServer.on('connection', function (newSocket) {
      if(newSocket){
        newSocket.join("card_t130");
        newSocket.join("rtGraphspeed2");
        var socketObj = new Object();
        socketObj.id = newSocket.id;
        socketObj.details = {
          terminalDetails:{
            timeSelected:30,
            terminalSelected:1,
            previousRoom:"card_t300"
          },
          realTimeGraphDetails:{
            parameterSelected:"speed",
            zoneSelected:2,
            laneWisDataCurrent:[
              {val:0,time:''},
              {val:0,time:''},
              {val:0,time:''},
              {val:0,time:''},
              {val:0,time:''}
            ]
          }
        };
        socketConnectionDetailsCollection.setSocketArrayDetails(socketObj);
        newSocket.on('disconnect', function(socket) {
          socketConnectionDetailsCollection.removeObjectFromSocketArray(newSocket.id)
        });
      }
  });
}
var getSocketIoServer = function(){
   if(!socketIoServer)init(httpServer)
  return socketIoServer;
}
module.exports={
  init:init,
  getSocketIoServer:getSocketIoServer
}

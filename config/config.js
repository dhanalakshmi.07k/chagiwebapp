var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

var config = {
  development: {
    root: rootPath,
    app: {
      name: 'changi'
    },
    port: 3000,
    db: 'mongodb://localhost/changi-development',/*
    db: 'mongodb://163.172.131.83:28018/changi',*/
    zmq:{
      sendHost:"107.20.148.118",
      recHost:"*",
      //host:'127.0.0.1',
      port:'3001',
      portVehicleAnalyticsByLane:'3002',
      portVehicleWholeZoneAnalytics:'3003',
     /* portNotification:'3004',*/
      vehicleWholeZoneAggregationData:'3006'
    },
    type:'development',
    maxLanesOfZone:[5,4,3]
  },

  integration: {
    root: rootPath,
    app: {
      name: 'changi'
    },
    port: 3000,/*
    db: 'mongodb://changIntigration:changIntigration@ds037175.mongolab.com:37175/changi',*/
    db: 'mongodb://163.172.131.83:28018/changi',
    zmq:{
      //sendHost:"127.0.0.1",
      //recHost:"127.0.0.1",
      sendHost:"212.47.249.75",
      recHost:"*",
      //host:'127.0.0.1',
      port:'3001',
      /*portNotification:'3004',*/
      portVehicleAnalyticsByLane:'3002',
      portVehicleWholeZoneAnalytics:'3003',
      vehicleWholeZoneAggregationData:'3006'
    },
    type:'integration',
    maxLanesOfZone:[5,4,3]
  },

  test: {
    root: rootPath,
    app: {
      name: 'changi'
    },
    port: 3000,
    db: 'mongodb://localhost/changi-test',
    zmq:{
      host:'localhost',
      port:'3001'

    },
    type:'test',
    maxLanesOfZone:[5,4,3]
  },

  production: {
    root: rootPath,
    app: {
      name: 'changi'
    },
    port: 3000,
    db: 'mongodb://localhost/changi-production',
    type:'production',
    maxLanesOfZone:[5,4,3]
  }
};

module.exports = config[env];

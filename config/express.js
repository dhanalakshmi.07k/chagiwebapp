var express = require('express');
var glob = require('glob');
var cors= require('cors');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var compress = require('compression');
var methodOverride = require('method-override');

module.exports = function(app, config,io) {
  module.exports = io;
  app.use(function(req, res, next) {
    req.io = io;
    next();
  });
  var env = process.env.NODE_ENV || 'development';
  app.locals.ENV = env;
  app.locals.ENV_DEVELOPMENT = env == 'development';

  app.set('views', config.root + '/app/views');
  app.set('view engine', 'jade');

  // app.use(favicon(config.root + '/public/img/favicon.ico'));
  app.use(logger('dev'));
  app.use(bodyParser.json());

  //routes to protect the views
  app.use('/templates/settings/*', function(req, res, next) {
    console.log("_0000000path0000000000");
    console.log(req.path);
    console.log(req.originalUrl)
  /*  console.log(req.headers['authorization']);*/
    if (!req.headers['authorization'] ) {
      console.log(req.headers['Bearer']);
   /*   res.sendfile('cannot Access');*/
    /*  window.location.href = 'http://localhost:3000/#/userPage/login';*/
      res.send('Not Authenticated');

    } else {
      next();
    }
  });

  app.use('/templates/*', function(req, res, next) {
    console.log("_0000000path0000000000");
    console.log(req.path);
    console.log(req.originalUrl)
  console.log(req.headers['authorization']);
    if (!req.headers['authorization'] ) {
      console.log(req.headers['Bearer']);
      res.send('Not Authenticated');

    } else {
      next();
    }
  });


 /* app.use('/directives/templates/!*', function(req, res, next) {
    console.log("_0000000path0000000000");
    console.log(req.path);
    console.log(req.originalUrl)
   console.log(req.headers['authorization']);
    if (!req.headers['authorization'] ) {
      console.log(req.headers['Bearer']);
      res.send('cannot Access');

    } else {
      next();
    }
  });
*/




  app.use(bodyParser.urlencoded({
    extended: true
  }));
  app.use(cookieParser());
  app.use(compress());
  app.use(express.static(config.root + '/public'));
  app.use(methodOverride());
  app.use(cors());
  var controllers = glob.sync(config.root + '/app/controllers/*.js');
  controllers.forEach(function (controller) {
    require(controller)(app);
  });

  app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
  });

  if(app.get('env') === 'development'){
    app.use(function (err, req, res, next) {
      res.status(err.status || 500);
      res.render('error', {
        message: err.message,
        error: err,
        title: 'error'
      });
    });
  }

  app.use(function (err, req, res, next) {
    res.status(err.status || 500);
      res.render('error', {
        message: err.message,
        error: {},
        title: 'error'
      });
  });

};

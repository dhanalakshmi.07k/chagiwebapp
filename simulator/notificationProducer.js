var zmq = require('zmq'),
    sock = zmq.socket('pub');



sock.bindSync('tcp://127.0.0.1:3000');
console.log('Producer bound to port 3000');


setInterval(function() {
  console.log('sending work');
  sock.send(['msg', JSON.stringify({data:getMsg()})]);
  }, 5000)


  var getMsg= function(){
    var noOfCar = randomInt(20,100)
    var lane = randomInt(1,4)
    var terminal = randomInt(1,3)
    var iconClass = noOfCar >50 ?"red":"yellow"

  var msg = {
    "message" : "Around "+noOfCar+" cars in Lane "+lane+" near T" +terminal+ " Exit",
    "iconTextPart2" : "Cars",
    "iconClass" : "symbol "+iconClass,
    "iconTextPart1" : noOfCar,
    "messageTime" : new Date()
  }
    return msg
}

function randomInt (low, high) {
  return Math.floor(Math.random() * (high - low) + low);
}

  var express = require('express'),
  router = express.Router(),
  mongoose = require('mongoose'),
  VehicleAnalyticsPubberModel = require('../models/vehicleAnalytics'),
  config = require('../../config/config');
mongoose.connect(config.db);
var random = require('mongoose-simple-random');
var zmq = require('zmq')
var zmqPortPart = 'tcp://127.0.0.1:3002'
var sock = zmq.socket('push');
sock.connect(zmqPortPart )

module.exports = function (app) {
  app.use('/', router);
};

setInterval(function(){
  var updatedObj
  // Find a single random document of vehicleAnalytics
  VehicleAnalyticsPubberModel.findOneRandom(function(err,result){
    var VehicleAnalyticsArray = result.vehicleAnalytics.vehicleDetectionEventsByLane;
    for(var i=0;i<VehicleAnalyticsArray.length;i++){
      VehicleAnalyticsArray[i].timeStamp = new Date();
      if(i==VehicleAnalyticsArray.length-1){
        result.vehicleAnalytics.vehicleDetectionEventsByLane = VehicleAnalyticsArray;
        updatedObj = result;
        console.log("VehicleAnalytics data pushed")
        sock.send( JSON.stringify(updatedObj));
      }
    }

  })

}, 500);


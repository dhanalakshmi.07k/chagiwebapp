/**
 * Created by zendynamix on 08-01-2016.
 */
var express = require('express'),
  router = express.Router();
var mongoose = require('mongoose'),
  //wholeZonePubberModel = require('../models/PubModelWholeZone'),
  wholeZoneTrafficInfoModel = require('../models/wholeZoneTrafficInfo'),
  config = require('../../config/config');
mongoose.connect(config.db);
var random = require('mongoose-simple-random');
var zmq = require('zmq')
var zmqPortPart = 'tcp://127.0.0.1:3003'
var sock = zmq.socket('push');
sock.connect(zmqPortPart )

module.exports = function (app) {
  app.use('/', router);
};

setInterval(function(){
  wholeZoneTrafficInfoModel.findOneRandom(function(err,result){
    /*console.log('sending a multipart message envelope'+result);*/
    sock.send( JSON.stringify(result));
    console.log("wholeZoneTrafficInfoModel data pushed")
  })

}, 500);



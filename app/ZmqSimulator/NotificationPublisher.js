/**
 * Created by zendynamix on 13-01-2016.
 */
var express = require('express'),
  router = express.Router();
var mongoose = require('mongoose'),
  notifyPublisher = require('../models/notificationModel'),
  config = require('../../config/config');
mongoose.connect(config.db);
var random = require('mongoose-simple-random');
var zmq = require('zmq')
var zmqPortPart = 'tcp://127.0.0.1:3004'
var sock = zmq.socket('push');
sock.connect(zmqPortPart);

module.exports = function (app) {
  app.use('/', router);
};

setInterval(function () {
  notifyPublisher.findOneRandom(function (err, resNotification) {

    sock.send(JSON.stringify(resNotification));
    console.log("notification data pushed")
  })

}, 5000);

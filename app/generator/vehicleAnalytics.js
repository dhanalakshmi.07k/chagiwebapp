/**
 * Created by Suhas on 12/18/2015.
 */
var mongoose = require('mongoose'),
  VehicleAnalyticsModel = require('../models/vehicleAnalytics'),
  config = require('../../config/config');
mongoose.connect(config.db);
var count1=0;
var count2=0;
VehicleAnalyticsModel.remove({}, function(err) {
  console.log('VehicleAnalyticsModel collection removed')
  var dateArray = function getDates() {
    var dateArray = new Array();
    var  stopDate= new Date();
    var currentDate = new Date();
    currentDate.setDate(stopDate.getDate()-30);
    while (currentDate <= stopDate) {
      dateArray.push(currentDate)
      currentDate = currentDate.addDays(1);
    }
    vehicleAnalyticsGenerator(dateArray,successMessage)
  }();
});
Date.prototype.addDays = function(days) {
  var dat = new Date(this.valueOf())
  dat.setDate(dat.getDate() + days);
  return dat;
}
function vehicleAnalyticsGenerator(dateArrayGenerated,callback){
  var count=0;
  var lat=[1.350228,1.353864,1.353864];
  var lang=[103.984889,103.986498,103.986498];
  var speed = [40,50,60,70];
  var laneNoArr = [1,2,3,4,5];
  var zoneArray = [1,2,3];
  var minutes=[15,25,35,45,55];
  var dateArrayFOrAnalytics = dateArrayGenerated;
  console.log("Generating Data for Previous "+dateArrayFOrAnalytics.length+" days please wait");
  var loader = ['..','...','....'];
  /*for all date in the date array dateArray.length*/
  for(var d=1;d<dateArrayFOrAnalytics.length;d++){
    var dateOfAlert = new Date(dateArrayFOrAnalytics[d]);
    /*for complete 3 Zones*/
    for(var zone=1;zone<4;zone++){
      /*for complete 24 hours*/
      for(var i=0;i<=23;i++){
        dateOfAlert.setHours(i);
        var minsLength = Math.round(Math.floor(Math.random() * 2) + 4);
        /*for Given minutes*/
        for(var min=1;min<=minsLength;min++){
          var vehicleAnalyticsModelObj  =  new VehicleAnalyticsModel();
          var roadInfo = {
            "roadId" : 1,
            "roadName" : "Airport Road "+i,
            "laneCount" : 5
          }
          var cameraExtrinsics = {
            "cameraId" : zoneArray[Math.floor(Math.random()*zoneArray.length)],
            "cameraName" : "Under Route-"+(min%5)+i+" NorthboundBridge",
            "cameraDirection" : "Towards Northbound Lanes",
            "cameraLatitude" : lat[min%5],
            "cameraLongitude" : lang[min%5],
            "cameraHeight" : 30  // in feet from ground
          }
          var cameraIntrinsics = {
            "cameraFocalLength" : 35, // in mm
            "cameraAngle" : 20, // in degrees wrt horizontal plane
            "imageWidth" : "1920",
            "imageHeight" : "1080",
            "frameRate" : 25 // Frames per second
          }
          var vehicleDetectionEventsByLane = [];
          dateOfAlert.setMinutes(Math.round(Math.floor(Math.random() * 59) + 1));
          var laneNoArr = [1,2,3,4,5];
          var vehicleDetectionLength = Math.round(Math.floor(Math.random() * 5) + 1)
          for(var j = 0;j<vehicleDetectionLength;j++){
            var laneNo = laneNoArr[Math.floor(Math.random()*laneNoArr.length)]
            var occupacy = Math.round(Math.floor(Math.random() * 100) + 10);
            var speed = 100-occupacy;
            var timeVehicleSeenOverDetectorArea = [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9];
            var timeVehicleNotified = dateOfAlert;
            var vehicleDetectionEventsByLaneObj = {
              laneIndex:laneNo,
              timeStamp: i,
              speed: Math.floor(Math.random() * 60) + 30,
              headway: {
                spaceHeadway:(Math.random()*100+0).toFixed(2),
                timeHeadway:(Math.random()*10+0).toFixed(2),
              },
              timeVehicleSeenOverDetectorArea:(Math.random()*10+0).toFixed(2)
            };
            vehicleDetectionEventsByLane.push(vehicleDetectionEventsByLaneObj);
            if(j == vehicleDetectionLength-1){
              laneNoArr = [1,2,3,4,5];
              callSaveFunction(vehicleAnalyticsModelObj,dateOfAlert);
            }
            else{
              for(var arrObj = laneNoArr.length - 1; arrObj >= 0; arrObj--) {
                if(laneNoArr[arrObj] === laneNo) {
                  laneNoArr.splice(arrObj, 1);
                }
              }
            }
          }
          function callSaveFunction(vehicleAnalyticsObject,dateTime){
            vehicleAnalyticsObject.vehicleAnalytics.cameraExtrinsics=cameraExtrinsics;
            vehicleAnalyticsObject.vehicleAnalytics.cameraIntrinsics=cameraIntrinsics;
            vehicleAnalyticsObject.vehicleAnalytics.roadInfo=roadInfo;
            vehicleAnalyticsObject.vehicleAnalytics.vehicleDetectionEventsByLane=vehicleDetectionEventsByLane;
            vehicleAnalyticsObject.dateTime=new Date(dateTime);
            vehicleAnalyticsModelObj.save(function(err,result){
              if(err)
                console.log(err)
              count++;
              console.log("saving Data")
            })
          }
        }
      }
    }

  }
  callback();
}

function successMessage (){
  console.log("")
}

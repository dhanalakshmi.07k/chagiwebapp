/**
 * Created by zendynamix on 21-01-2016.
 */
var mongoose = require('mongoose'),
  VehicleAnalyticsModel = require('../models/pubblisherModelOfVehicleAnalytics'),
  config = require('../../config/config');

VehicleAnalyticsModel.remove({}, function(err) {
  console.log('VehicleAnalytics Sample Model collection removed')
});
mongoose.connect(config.db);

// Define JSON File
var fs = require("fs");
console.log("\n *STARTING* \n");
// Get content from file
var contents = fs.readFileSync("../data/data.json");
// Define to JSON type
var jsonContent = JSON.parse(contents);
// Get Value from JSON
console.log(jsonContent.length)
for(var k=0;k<jsonContent.length;k++){
  console.log(jsonContent[k]);
  var vehicleAnalyticsObj = VehicleAnalyticsModel();
  vehicleAnalyticsObj.vehicleAnalytics = jsonContent[k].vehicleAnalytics;
  vehicleAnalyticsObj.dateTime = new Date();
  console.log("!@#!@#!@#"+vehicleAnalyticsObj);
  vehicleAnalyticsObj.save(function (err, result) {
    if (err) {
      console.log(err);
    }
    console.log("saving Vehicle Detection Data");
  })
}




console.log("\n *EXIT* \n");

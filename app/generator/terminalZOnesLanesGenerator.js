/**
 * Created by zendynamix on 14-04-2016.
 */

var mongoose = require('mongoose'),
  config = require('../../config/config');
var terminalModel = require('../models/terminalGenerator');
Schema = mongoose.Schema;
mongoose.connect(config.db);
function createTerminalData(){
  var terminals=[1,2,3];
  for(var i=1;i<=terminals.length;i++){
    var terminalModelObj = terminalModel();
    terminalModelObj.terminal.terminalId =i;
    var zoneDetailsArray=[];
    var cameraId=[];
    var laneDetails=[];
    /*i value is terminal*/
    if(i==1){
      var obj1={
        cameraId:[1],
        zoneId:1,
        laneIndex:[2,3,4,5]
      }
      var obj2={
        cameraId:[2],
        zoneId:2,
        laneIndex:[1,2,3,4]
      }
      var obj3={
        cameraId:[3],
        zoneId:3,
        laneIndex:[1,2,3]
      }
       zoneDetailsArray.push(obj1);
       zoneDetailsArray.push(obj2);
       zoneDetailsArray.push(obj3);
    }
    else if(i==2){
      var obj1={
        cameraId:[1],
        zoneId:1,
        laneIndex:[4,5]
      }
      var obj2={
        cameraId:[2],
        zoneId:2,
        laneIndex:[3,4]
      }

      zoneDetailsArray.push(obj1);
      zoneDetailsArray.push(obj2);
    }
    else if(i==3){
      var obj={
        cameraId:[1],
        zoneId:1,
        laneIndex:[1,2]
      }
      zoneDetailsArray.push(obj);
    }
    /*else if(i==4){
      var obj={
        cameraId:[3,2],
        zoneId:4,
        laneIndex:[2,1]
      }
      zoneDetailsArray.push(obj);
    }*/
    terminalModelObj.terminal.zoneDetails=zoneDetailsArray;
    if(terminalModelObj.terminal.zoneDetails.length!=0){
      terminalModelObj.save(function(err,result){
        if(err){
          console.log(err)
        }
        console.log("saving Data")
      })
    }
  }
}
var removePreviousDataAndGenerate = function(){
  terminalModel.remove({}, function(err) {
    console.log("removed terminal document");
    createTerminalData();
  });
}
removePreviousDataAndGenerate();
module.exports={
  removePreviousDataAndGenerate:removePreviousDataAndGenerate
}


/**
 * Created by zendynamix on 27-04-2016.
 */

var mongoose = require('mongoose'),
  config = require('../../config/config');
var zoneSegmentInLaneDetails = require('../models/zoneSegmentInLane');
Schema = mongoose.Schema;
mongoose.connect(config.db);
zoneSegmentInLaneDetails.remove({}, function(err) {
  console.log("removed zoneSegmentInLaneDetails document");
  createZoneSegmentInLaneConfiguration();
});

function createZoneSegmentInLaneConfiguration(){
  var zones=[1,2,3];
  var zone1=[
    {lat: 1.3471226337599076, lng: 103.98413121700287},
    {lat: 1.3473532399698958, lng: 103.98421704769135},
    {lat: 1.3475945720267368, lng: 103.98432970046997},
    {lat: 1.3479109851316848, lng: 103.98445844650269},
    {lat: 1.3482488499271839, lng: 103.98460865020752},
    {lat: 1.348602803472155, lng: 103.98477494716644},
    {lat: 1.3489138535146545, lng: 103.98490905761719},
    {lat: 1.349133733693114, lng: 103.98500561714171},
    {lat: 1.3493804285038262, lng: 103.98508071899414},
    {lat: 1.349621760359669, lng: 103.9851665496826}
  ];
  var zone2=[
    {lat: 1.3505280953377055, lng: 103.98556351661682},
    {lat: 1.3509410407481706, lng: 103.98572981357573},
    {lat: 1.3513432602363402, lng: 103.9859014749527},
    {lat: 1.3517454796579087, lng: 103.98609459400176},
    {lat: 1.3520887068449627, lng: 103.9862447977066},
    {lat: 1.3524372969071434, lng: 103.98639500141142},
    {lat: 1.35279661276497, lng: 103.98653984069824},
    {lat: 1.3532041948676643, lng: 103.9867115020752},
    {lat: 1.3535259701634998, lng: 103.98686707019806},
    {lat: 1.353896011700933, lng: 103.98701190948486}
  ];
  var zone3=[
    {lat: 1.3542982306992972, lng: 103.98718893527985},
    {lat: 1.3546414575251164, lng: 103.98733913898468},
    {lat: 1.3549954101383253, lng: 103.98747861385345},
    {lat: 1.3553869031199566, lng: 103.98765027523041},
    {lat: 1.3557033152095443, lng: 103.98780584335326},
    {lat: 1.3561162597388559, lng: 103.98799359798431},
    {lat: 1.3565452929414084, lng: 103.98812234401703},
    {lat: 1.3570225922898875, lng: 103.98834764957428},
    {lat: 1.3574623511567672, lng: 103.98854076862335},
    {lat: 1.3578860212089987, lng: 103.98870170116425}
  ];

  for(var i=0;i<zones.length;i++){
    var zoneSegmentObj =  new zoneSegmentInLaneDetails();
    zoneSegmentObj.id=i;
    if(i==0){
      zoneSegmentObj.zoneSegmentArray=zone1;
    }
    else if(i==1){
      zoneSegmentObj.zoneSegmentArray=zone2;
    }
    else if(i==2){
      zoneSegmentObj.zoneSegmentArray=zone3;
    }
    zoneSegmentObj.zoneSegmentLaneTime=new Date();
    zoneSegmentObj.save(function(err,result){
      if(err){
        console.log(err)
      }
      console.log("configuration data saved")
    })
  }
}



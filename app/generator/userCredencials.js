/**
 * Created by zendynamix on 07-03-2016.
 */
var mongoose = require('mongoose'),
    config = require('../../config/config');
var PassportLocalUser = require('../models/Passport_LocalUsers');
Schema = mongoose.Schema;
mongoose.connect(config.db);
PassportLocalUser.findOneAndRemove({"username": 'admin'}, function(err){
  if(err) {
    console.log(err)
  }else{
    console.log("removed previous admin document   ")
    PassportLocalUser.findOneAndRemove({"username": 'user'}, function(err){
      if(err) {
        console.log(err)
      }else{
        console.log("removed previous admin document   ")
        generateUserCredentials();
      }

    });
  }

});


function generateUserCredentials(){
    var userObj = new PassportLocalUser();
    userObj.username="admin";
    userObj.password="$2a$10$7/zyPNb1XxbAKG2INj4/R.TYrn0a.k9jrLvrifiV/V6RDxhpTaht6";
    userObj.email="admin@gmail.com";
    userObj.firstName="admin";
    userObj.lastName="admin";
    userObj.isAdmin=true;
    userObj.save(function(err,result){
      generatePublicUserCredentials();
        if(err)
            console.log(err)
        console.log("Credencials generated")
    })
};

function generatePublicUserCredentials(){
  var userObj = new PassportLocalUser();
  userObj.username="user";
  userObj.password="$2a$10$JzLQmIThoU4swr36KUmcAO5QK5.J529qW5saO36jzgShKJV/EfBsi";
  userObj.email="user@gmail.com";
  userObj.firstName="user";
  userObj.lastName="user";

  userObj.save(function(err,result){

    if(err)
      console.log(err)
    console.log("Credencials generated")
  })
}



/**
 * Created by Suhas on 1/21/2016.
 */
var mongoose = require('mongoose'),
  config = require('../../config/config');
var smsPhoneNoNotificationModel = require('../models/smsPhoneNumberForNotifier');
mongoose.connect(config.db);
var phoneNo = [919886213442]
function createSmsPhoneNoNotificationData(){
  for(var i=0;i<phoneNo.length;i++){
    var smsPhoneNoNotificationObj = smsPhoneNoNotificationModel();
    smsPhoneNoNotificationObj.phoneNo = phoneNo[i];
    smsPhoneNoNotificationObj.phoneId = i+1;
    smsPhoneNoNotificationObj.save(function(err,result){
      if(err){
        console.log(err)
      }
      console.log("saving Data")
    })
  }
}
var removePreviousDataAndGenerate = function(){
  smsPhoneNoNotificationModel.remove({}, function(err) {
    console.log("removed PhoneNo for sms document");
    createSmsPhoneNoNotificationData();
  });
}
removePreviousDataAndGenerate()
module.exports={
  removePreviousDataAndGenerate:removePreviousDataAndGenerate
}


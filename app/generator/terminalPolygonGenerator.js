/**
 * Created by zendynamix on 23-01-2016.
 */

var mongoose = require('mongoose'),
  config = require('../../config/config');
var polygonSchema = require('../models/terminalPolygonSchema');
Schema = mongoose.Schema;
mongoose.connect(config.db);

function createPolygon(){
  var cordinateLattitude=[8800,10800];
  var cordinateLangitude=[7800,9800];
  var thershold=[11,20,6,4]
  for(var i=0;i<cordinateLattitude.length;i++){
    var polygonObject = polygonSchema();
    polygonObject.id=i;
    var polygonObj={
      corordinat1Lat: cordinateLattitude[i],
      corordinat1Lan:cordinateLangitude[i],
      corordinat2Lat: cordinateLattitude[i+1],
      corordinat2Lan:cordinateLangitude[i+1]
    }
    i++;
    polygonObject.polygonobj=polygonObj;
    polygonObject.threshold=thershold[Math.floor(Math.random()*thershold.length)];
    polygonObject.polygonTime=new Date();
    polygonObject.save(function(err,result){
      if(err){
        console.log(err)
      }

      console.log("saving Data")
    })
  }

}


var removePreviousDataAndGenerate = function(){

  polygonSchema.remove({}, function(err) {
    console.log("removed notification document");
    createPolygon();
  });
}
removePreviousDataAndGenerate();
module.exports={
  removePreviousDataAndGenerate:removePreviousDataAndGenerate
}

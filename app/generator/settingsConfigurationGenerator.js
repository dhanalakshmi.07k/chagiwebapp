/**
 * Created by Suhas on 4/29/2016.
 */
var mongoose = require('mongoose'),
  config = require('../../config/config');
var settingConfigurationModel = require('../models/settingsConfigurationModel');
Schema = mongoose.Schema;
mongoose.connect(config.db);
/*createSettingConfiguration();*/
function createSettingConfiguration(){
  var settingConfigurationModelObj = settingConfigurationModel();
  settingConfigurationModelObj.id=1;
    var settingTabConfiguration={
      showSimulatorTab:false,
      showDataDownloadTab:true,
      showUserDetailsTab:true
  }
  var notificationSettings={
    notificationFilterPeriod:5
  }
  settingConfigurationModelObj.settingTabConfiguration = settingTabConfiguration;
  settingConfigurationModelObj.notificationSettings = notificationSettings;
  settingConfigurationModelObj.save(function(err){
    if(err){
      console.log(err.stack)
    }else{
      console.log("data saved")
    }
  })
}
var removePreviousDataAndGenerate = function(){
  settingConfigurationModel.remove({}, function(err) {
    console.log("removed terminal document");
    createSettingConfiguration();
  });
}
removePreviousDataAndGenerate();
module.exports={
  removePreviousDataAndGenerate:removePreviousDataAndGenerate
}

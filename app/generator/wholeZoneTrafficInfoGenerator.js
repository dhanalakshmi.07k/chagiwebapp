/**
 * Created by sHathwar on 12/22/2015.
 */
var mongoose = require('mongoose'),
  wholeZoneTrafficInfoModel = require('../models/wholeZoneTrafficInfo'),
  config = require('../../config/config');
function randomInt(min,max)
{
  return Math.round(Math.floor(Math.random()*(max-(min+1))+(min+1)));
}
mongoose.connect(config.db);
var count = 0;
wholeZoneTrafficInfoModel.remove({}, function(err) {
  console.log('wholeZoneTrafficInfo collection removed')
  var dateArray = function getDates() {
    var dateArray = new Array();
    var dateArray = new Array();
    var  stopDate= new Date();
    var currentDate = new Date();
    currentDate.setDate(stopDate.getDate()-30);
    while (currentDate <= stopDate) {
      dateArray.push(currentDate)
      currentDate = currentDate.addDays(1);
    }
    wholeZoneTrafficInfoGenerator(dateArray)
  }();
});
Date.prototype.addDays = function(days) {
  var dat = new Date(this.valueOf())
  dat.setDate(dat.getDate() + days);
  return dat;
}


function wholeZoneTrafficInfoGenerator(dateArray){
  var lat=[1.350228,1.353864,1.353864];
  var lang=[103.984889,103.986498,103.986498];
  var speed = [40,50,60,70];
  var minutes=[5,10,20,25,30,35,40,45,50,55];
  console.log("Generating Data for "+dateArray.length+" many days please wait.....")
      /*for all date in the date array*/
      for(var d=1;d<dateArray.length;d++){
          /*for complete 3 Zones*/
          for(var zone =1;zone<4;zone++){
              /*for complete 24 hours*/
              for(var i=0;i<=23;i++){
                  var minutessLength = [1,2,3,4,5,6,7,8];
                  /*for Given minutes*/
                  for(var min=1;min<minutes.length;min++){
                      var wholeZoneTrafficInfoModelObj  =  new wholeZoneTrafficInfoModel();
                      var roadInfo = {
                        "roadId" : 1,
                        "roadName" : "Airport Road "+i,
                        "laneCount" : 5
                      }
                      var zoneNo = Math.round(Math.floor(Math.random() * 3) + 1)
                      var cameraExtrinsics = {
                          "cameraId" : zone,
                          "cameraName" : "Under Route-"+zone+" NorthboundBridge",
                          "cameraDirection" : "Towards Northbound Lanes",
                          "cameraLatitude" : lat[min%5],
                          "cameraLongitude" : lang[min%5],
                          "cameraHeight" : 30  // in feet from ground
                      }
                      var cameraIntrinsics = {
                        "cameraFocalLength" : 35, // in mm
                        "cameraAngle" : 20, // in degrees wrt horizontal plane
                        "imageWidth" : "1920",
                        "imageHeight" : "1080",
                        "frameRate" : 25 // Frames per second
                      }
                      var wholeZoneTrafficInformation = [];
                      var dateOfAlert = dateArray[d];
                      dateOfAlert.setHours(i);
                      dateOfAlert.setMinutes(Math.round(Math.floor(Math.random() * 59) + 1));
                      var laneNoArr = [1,2,3,4,5];
                      var vehicleDetectionLength = Math.round(Math.floor(Math.random() * 5) + 1)
                      /*var saveData = false;*/
                      for(var j =1;j<6;j++){
                          var laneNo = j;
                          var occupancy = randomInt(10,100);
                          var speed = randomInt(5,120);
                          var wholeZoneTrafficInformationObj = {
                            laneIndex: j,
                            zoneOccupancy: occupancy,
                            zoneSpeed: speed,
                            levelofService: randomInt(0,4)
                          };
                          wholeZoneTrafficInformation.push(wholeZoneTrafficInformationObj);
                          if(j == 5){
                            laneNoArr = [1,2,3,4,5];
                            /*saveData = true*/
                            saveWholeZoneDataAnalytics(wholeZoneTrafficInfoModelObj,dateOfAlert)
                          }
                          else{
                          for(var arrObj = laneNoArr.length - 1; arrObj >= 0; arrObj--) {
                            if(laneNoArr[arrObj] === laneNo) {
                              laneNoArr.splice(arrObj, 1);
                            }
                          }
                        }
                      }
                      function saveWholeZoneDataAnalytics(objToBeSaved,dateTime){
                          objToBeSaved.vehicleAnalytics.cameraExtrinsics=cameraExtrinsics;
                          objToBeSaved.vehicleAnalytics.cameraIntrinsics=cameraIntrinsics;
                          objToBeSaved.vehicleAnalytics.roadInfo=roadInfo;
                          objToBeSaved.vehicleAnalytics.wholeZoneTrafficInformation=wholeZoneTrafficInformation;
                          objToBeSaved.dateTime=new Date(dateTime);
                          /*if(saveData){*/
                          objToBeSaved.save(function(err,result){
                            if(err)
                              console.log(err)
                            count++;
                            console.log(count+" No of Data Generated For whole Zone Traffic Information");
                          })
                      }
                  }
              }
          }
    }

}


/**
 * Created by zendynamix on 27-04-2016.
 */

var mongoose = require('mongoose'),
  config = require('../../config/config');
var cameraDetails = require('../models/cameraConfiguration');
Schema = mongoose.Schema;
mongoose.connect(config.db);
var removePreviousDataAndGenerate=function(){
  cameraDetails.remove({}, function(err) {
    console.log("removed zoneSegmentInLaneDetails document");
    createCameraMarkerConfiguration();
  });
}
removePreviousDataAndGenerate();
function createCameraMarkerConfiguration(){

  var cameraPlotsArray= [
    {

      "lat": 1.347064,
      "lng": 103.984104
    },
    {
      "lat": 1.349542,
      "lng": 103.985117
    },
    {
      "lat": 1.349496,
      "lng": 103.985223
    }
    , {
      "lat":  1.354569,
      "lng": 103.987266
    }
  ]
    var cameraDetailsObj =  new cameraDetails();
    cameraDetailsObj.id=1;
    cameraDetailsObj.cameraMarkerArray=cameraPlotsArray;
    cameraDetailsObj.cameraMarkerTime=new Date();
    cameraDetailsObj.save(function(err,result){
      if(err){
        console.log(err)
      }
      console.log("configuration data saved")
    })

}
module.exports={
  removePreviousDataAndGenerate:removePreviousDataAndGenerate
}

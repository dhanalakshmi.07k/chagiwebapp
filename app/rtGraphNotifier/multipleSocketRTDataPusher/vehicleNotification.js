/**
 * Created by Suhas on 2/9/2016.
 */
var SocketIo = require('../../../config/socketIo').getSocketIoServer(),
  socketConnectionDetailsCollection = require("../../multipleSocket");
var config = require('../../../config/config');
  var socketRoomList=[];
  SocketIo.on('connection', function (newSocket){
      if (newSocket) {
          newSocket.on("setAnalyticsConfiguration", function (configDetails) {
              var rtGraphDetails =  configDetails.realTimeGraphDetails;
              var newRoom = "rtGraph"+rtGraphDetails.parameterSelected+""+rtGraphDetails.zoneSelected;
              var parameterSelected = rtGraphDetails.parameterSelected;
              var roomList = newSocket.rooms;
              if(parameterSelected!='occupancy'){
                for(var i=1;i<roomList.length;i++){
                  var previousRoom =roomList[i];
                  var roomNameFirstLetter = previousRoom.substring(0,5);
                  var isOccupancy = false;
                  if(previousRoom.length>8){
                    if(newRoom.substring(7,8)==="o"){
                      isOccupancy=true;
                    }
                  }
                  if(roomNameFirstLetter==='rtGra'){
                    if(previousRoom===newRoom){
                    }
                    else{
                      newSocket.leave(previousRoom)
                      newSocket.join(newRoom)
                      console.log("(data ping notifier)client "+newSocket.id+" left room "+previousRoom+" " +"and Joined "+newRoom+" room")
                    }
                    var connId= newSocket.id;
                    socketRoomList=[];
                    socketConnectionDetailsCollection.updateSocketArrayObj(connId,configDetails);
                    createPublisherRooms();
                  }else{}
                }
              }else{
                socketRoomList=[];
                createPublisherRooms();
              }

          })
          newSocket.on("disconnect", function (configDetails) {
        socketRoomList=[];
        createPublisherRooms();
      })
    }
  });
  var notifyVehiclesDetected = function(data){
  if(socketRoomList && socketRoomList.length>0){
      var zoneId = data.vehicleAnalytics.cameraExtrinsics.cameraId;
      for(var i=0;i<socketRoomList.length;i++){
          var clientsRtGraphDetails = socketRoomList[i];
          var clientZoneSelected =clientsRtGraphDetails.zoneSelected;
          var clientParameterSelected =clientsRtGraphDetails.parameterSelected;
          var clientLaneWisDataCurrent =clientsRtGraphDetails.laneWisDataCurrent;
          var roomName = clientsRtGraphDetails.roomName;
          var vehicleDetectionArray = data.vehicleAnalytics.vehicleDetectionEventsByLane;
          var maxLanesPerZone = config.maxLanesOfZone[zoneId-1];
          clientLaneWisDataCurrent = clientLaneWisDataCurrent.slice(0,maxLanesPerZone)
          if(clientZoneSelected == zoneId && clientParameterSelected!='occupancy'){
              for(var j=0;j<vehicleDetectionArray.length;j++){
                  var vehicleDetectionObj = vehicleDetectionArray[j];
                  var laneIndex = vehicleDetectionObj.laneIndex;
                  if(laneIndex<=maxLanesPerZone){
                      if(clientParameterSelected=='speed'){
                        clientLaneWisDataCurrent[laneIndex-1].val = vehicleDetectionObj.speed;
                      }else{
                        clientLaneWisDataCurrent[laneIndex-1].val = vehicleDetectionObj.headway[clientParameterSelected];
                      }
                  }
                if(j==vehicleDetectionArray.length-1){
                  SocketIo.to(roomName).emit('vehicleDetectionEventNotifier',clientLaneWisDataCurrent);
                  console.log("Emitting data to room "+roomName)
                }

              }
          }
      }
  }else{
    createPublisherRooms()
  }
}
  function createPublisherRooms(){
    var clientDetails = socketConnectionDetailsCollection.getSocketArrayDetails();
    if(clientDetails && clientDetails.length>0){
      for(var i=0;i<clientDetails.length;i++){
        var clientRtGraphDetails = clientDetails[i].details.realTimeGraphDetails;
        var parameterSelected = clientRtGraphDetails.parameterSelected;
        var zoneSelected = clientRtGraphDetails.zoneSelected;
        var laneWisDataCurrent  = clientRtGraphDetails.laneWisDataCurrent;
        var roomName = "rtGraph"+parameterSelected+""+zoneSelected;
        var roomPublisherObj = {
          roomName:roomName,
          parameterSelected:parameterSelected,
          zoneSelected:zoneSelected,
          laneWisDataCurrent:laneWisDataCurrent,
          noOfClients:1
        }
        if(parameterSelected!="occupancy" && socketRoomList.length==0){
          socketRoomList.push(roomPublisherObj)
        }else if(parameterSelected!="occupancy" && socketRoomList.length>0 && !isRoomAlreadyExist(socketRoomList,roomPublisherObj.roomName)){
          socketRoomList.push(roomPublisherObj)
        }
      }
    }
  }
  function isRoomAlreadyExist(roomListArray,objRoomName){
    for(var i=0;i<roomListArray.length;i++){
      var roomName = roomListArray[i].roomName;
      if(objRoomName === roomName){
        roomListArray[i].noOfClients = roomListArray[i].noOfClients+1;
        socketRoomList = roomListArray;
        return true;
        break;
      }
      if(i == roomListArray.length-1){
        if(objRoomName === roomName){
          roomListArray[i].noOfClients = roomListArray[i].noOfClients+1;
          socketRoomList = roomListArray;
          return true;
        }else{
          return false;
        }
      }
    }
  }
  module.exports = {
    notifyVehiclesDetected:notifyVehiclesDetected
  }

/**
 * Created by Suhas on 2/9/2016.
 */
var VehicleAnalyticsModel = require("../../models/vehicleAnalytics"),
  SocketIo = require('../../../config/socketIo').getSocketIoServer(),
  socketConnectionDetailsCollection = require("../../multipleSocket");
  var socketRoomList=[];
  var config = require('../../../config/config');
  SocketIo.on('connection', function (newSocket){
      if (newSocket) {
          newSocket.on("setAnalyticsConfiguration", function (configDetails) {
              var rtGraphDetails =  configDetails.realTimeGraphDetails;
              var newRoom = "rtGraph"+rtGraphDetails.parameterSelected+""+rtGraphDetails.zoneSelected;
              var parameterSelected = rtGraphDetails.parameterSelected;
              var roomList = newSocket.rooms;
            if(parameterSelected==='occupancy'){
              for(var i=1;i<roomList.length;i++){
                var previousRoom =roomList[i];
                var roomNameFirstLetter = previousRoom.substring(0,5);
                var isOccupancy = false;
                if(previousRoom.length>8){
                  /* console.log(previousRoom.substring(7,8))*/
                  if(newRoom.substring(7,8)==="o"){
                    isOccupancy=true;
                  }
                }
                if(roomNameFirstLetter==='rtGra'){
                  if(previousRoom===newRoom){
                  }
                  else{
                    newSocket.leave(previousRoom)
                    newSocket.join(newRoom)
                    console.log("(data ping notifier)client "+newSocket.id+" left room "+previousRoom+" " +"and Joined "+newRoom+" room")
                  }
                  var connId= newSocket.id;
                  socketRoomList=[];
                  socketConnectionDetailsCollection.updateSocketArrayObj(connId,configDetails);
                  createPublisherRooms();
                }else{}
              }
            }else{
              socketRoomList=[];
              createPublisherRooms();
            }

          })
          newSocket.on("disconnect", function (configDetails){
          socketRoomList=[];
          createPublisherRooms();
        })
      }
});
  function createPublisherRooms(){
      var clientDetails = socketConnectionDetailsCollection.getSocketArrayDetails();
      console.log('Clients Length --->  '+clientDetails.length)
      if(clientDetails && clientDetails.length>0){
          for(var i=0;i<clientDetails.length;i++){
              var clientDetailsObj = clientDetails[i].details.realTimeGraphDetails;
              var parameterSelected = clientDetailsObj.parameterSelected;
              var zoneSelected = clientDetailsObj.zoneSelected;
              var laneWisDataCurrent  = clientDetailsObj.laneWisDataCurrent;
              var roomName = "rtGraph"+parameterSelected+""+zoneSelected;
              var roomPublisherObj = {
                  roomName:roomName,
                  parameterSelected:parameterSelected,
                  zoneSelected:zoneSelected,
                  laneWisDataCurrent:laneWisDataCurrent,
                  noOfClients:1
              }
              if(parameterSelected==="occupancy" && socketRoomList.length==0){
                socketRoomList.push(roomPublisherObj)
                console.log('SocketRoomList Length --->  '+socketRoomList.length)
              }else if(parameterSelected==="occupancy" && socketRoomList.length>0 && !isRoomAlreadyExist(socketRoomList,roomPublisherObj.roomName)){
                socketRoomList.push(roomPublisherObj)
                console.log('SocketRoomList Length --->  '+socketRoomList.length)
              }
          }
      }
  }
  var notifyWholeZoneDetected = function(data){
      if(socketRoomList.length>0){
          var zoneId = data.vehicleAnalytics.cameraExtrinsics.cameraId;
          for(var i=0;i<socketRoomList.length;i++){
              var socketRoomObj =socketRoomList[i];
              var clientZoneSelected =socketRoomObj.zoneSelected;
              var clientParameterSelected =socketRoomObj.parameterSelected;
              var clientLaneWisDataCurrent =socketRoomObj.laneWisDataCurrent;/*
               var clientId = clientDetails[i].id;*/
              var roomName = socketRoomObj.roomName;
              var maxLanesPerZone = config.maxLanesOfZone[zoneId-1];
              clientLaneWisDataCurrent = clientLaneWisDataCurrent.slice(0,maxLanesPerZone)
              var wholeZoneVehicleDetectionArray = data.vehicleAnalytics.wholeZoneTrafficInformation;
              if(clientZoneSelected == zoneId && clientParameterSelected=='occupancy'){
                  for(var j=0;j<wholeZoneVehicleDetectionArray.length;j++){
                      var wholeZoneVehicleDetectionObj = wholeZoneVehicleDetectionArray[j];
                      var laneIndex = wholeZoneVehicleDetectionObj.laneIndex;
                      if(laneIndex<=maxLanesPerZone){
                        clientLaneWisDataCurrent[laneIndex-1].val = wholeZoneVehicleDetectionObj.zoneOccupancy;
                      }
                      if(j==wholeZoneVehicleDetectionArray.length-1){
                          SocketIo.to(roomName).emit('wholeZoneVehicleDetectionEventNotifier',clientLaneWisDataCurrent);
                          console.log("Emitting data to room "+roomName)
                      }
                  }
              }
          }
      }else{
        createPublisherRooms()
      }
  }
  function isRoomAlreadyExist(roomListArray,objRoomName){
  for(var i=0;i<roomListArray.length;i++){
    var roomName = roomListArray[i].roomName;
    /*console.log(roomName)*/
    console.log(roomListArray)
    if(objRoomName === roomName){
      roomListArray[i].noOfClients = roomListArray[i].noOfClients+1;
      socketRoomList = roomListArray;
      return true;
      break;
    }
    if(i == roomListArray.length-1){
      if(objRoomName === roomName){
        roomListArray[i].noOfClients = roomListArray[i].noOfClients+1;
        socketRoomList = roomListArray;
        return true;
      }else{
        return false;
      }
    }
  }
}
  module.exports = {
  notifyWholeZoneDetected:notifyWholeZoneDetected
}

/**
 * Created by Suhas on 2/6/2016.
 */
/*var VehicleAnalyticsModel = require("../../models/vehicleAnalytics"),
  wholeZoneTrafficInfoModel = require('../../models/wholeZoneTrafficInfo'),
  SocketIo = require('../../../config/socketIo').getSocketIoServer(),
  socketConnectionDetailsCollection = require("../../socketConnectionData");

var notifyVehiclesDetected = function(data){
  var clientDetails = socketConnectionDetailsCollection.getSocketArrayDetails();
  if(clientDetails.length>0){
    var zoneId = data.vehicleAnalytics.cameraExtrinsics.cameraId;
    for(var i=0;i<clientDetails.length;i++){
      var clientZoneSelected =clientDetails[i].details.realTimeGraphDetails.zoneSelected;
      var clientParameterSelected =clientDetails[i].details.realTimeGraphDetails.parameterSelected;
      var clientLaneWisDataCurrent =clientDetails[i].details.realTimeGraphDetails.laneWisDataCurrent;
      var testData = clientDetails[i].details.realTimeGraphDetails;
      var clientId = clientDetails[i].id;
      var vehicleDetectionArray = data.vehicleAnalytics.vehicleDetectionEventsByLane;
      if(clientZoneSelected == zoneId){
        for(var j=0;j<vehicleDetectionArray.length;j++){
          var vehicleDetectionObj = vehicleDetectionArray[j];
          var laneIndex = vehicleDetectionObj.laneIndex;
          if(clientDetails[i].details.realTimeGraphDetails.laneWisDataCurrent || clientParameterSelected=='occupancy'){
            if(clientParameterSelected=='speed'){
              clientLaneWisDataCurrent[laneIndex-1].val = vehicleDetectionObj.speed;
            }else{
              clientLaneWisDataCurrent[laneIndex-1].val = vehicleDetectionObj.headway[clientParameterSelected];
            }
            SocketIo.to(clientId).emit('vehicleDetectionEventNotifier',clientLaneWisDataCurrent);
          }
        }
      }
      }
    }
  }*/
var vehicleNotifier = require('./vehicleNotification');
var wholeZoneNotifier = require('./wholeZoneNotifier');
module.exports = {
  vehicleNotifier: vehicleNotifier,
  wholeZoneNotifier:wholeZoneNotifier
}

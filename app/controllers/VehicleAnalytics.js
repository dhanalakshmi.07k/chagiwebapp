/**
 * Created by zendynamix on 09-01-2016.
 */

  var express = require('express'),
  router = express.Router(),
  mongoose = require('mongoose'),
  VehicleAnalyticsModel = mongoose.model('vehicleAnalytics'),
  TerminalZonesAndLaneDetails = mongoose.model('terminalModel'),
  random = require('mongoose-simple-random'),
  request = require("request"),
  moment = require("moment"),
  SocketIo = require('../../config/socketIo').getSocketIoServer(),
  vehicleDetectionNotifier =require('../rtGraphNotifier/vehicleAnalytics'),
  notifyClientsRTDetails = require("../rtGraphNotifier/multipleSocketRTDataPusher");
var socketConnectionDetailsCollection = require("../multipleSocket");
var triggerAggregationForAnalyticsPage = require("../dataPusher");
var historicalDataAggregationModule = require("../historicalData");
var config = require('../../config/config');


Array.prototype.divide = function (arr) {
  var sum = [];
  if (arr != null && this.length == arr.length) {
    for (var i = 0; i < arr.length; i++) {
      if(this[i]==0){
        sum.push(0);
      }else{
        var no = this[i]/arr[i]
        var no1 = parseInt(no.toFixed(2))
        sum.push(no1);
      }

    }
  }
  return sum;
}
Array.prototype.AvgArray = function (length) {
  var avgArray = [];
  if (this.length != null) {
    for (var i = 0; i < this.length; i++) {
      this[i] = this[i] / length;
      avgArray.push(this[i]);
    }
  }
  return avgArray;
}
Array.prototype.addArrayElements = function () {
  var sum = 0;
  if (this.length != null) {
    for (var i = 0; i < this.length; i++) {
      sum = this[i]+sum;
    }
  }
  return sum;
}
Array.prototype.AvgOfMultipleArray = function (arrayContainer) {
  var avgArray = [];
  var noOfArrays = arrayContainer.length;
  if (this.length != null) {
    for (var i = 0; i < this.length; i++) {
      for(var j=0;j<noOfArrays;j++){
        this[i]=this[i]+arrayContainer[j][i];
        if(j==noOfArrays-1){
          this[i] = this[i]/(noOfArrays+1);
          avgArray.push(this[i])
        }
        if(i==this.length-1 && j==noOfArrays-1){
          return avgArray
        }
      }
      /*this[i] = this[i] / noOfArrays;
       avgArray.push(this[i]);*/
    }
  }
  return avgArray;
}
Array.prototype.contains = function(obj) {
  var i = this.length;
  while (i--) {
    if (this[i]==obj) {
      return true;
    }
  }
  return false;
}

module.exports = function (app) {
  app.use('/', router);
};

//router.use('/api', expressJwt({secret: secret}));
router.use(function(err, req, res, next){
  if (err.constructor.name === 'UnauthorizedError') {
    res.status(401).send('Unauthorized Token Is not present');
  }
});
router.get('/', function (req, res, next) {
  Article.find(function (err, articles) {
    if (err) return next(err);
    res.render('index', {
      title: 'Generator-Express MVC',
      articles: articles
    });
  });
});
function randomInt (low, high) {
  return Math.floor(Math.random() * (high - low) + low);
}


// Find a single random document of vehicleAnalytics
router.get('/api/randomAnalytics', function (req, res) {
  var io = req.io;

  VehicleAnalyticsModel.findOneRandom(function(err,result){
    if(result){
      vehicleDetectionNotifierMethod(result);
      var VehicleAnalyticsArray = result.vehicleAnalytics.vehicleDetectionEventsByLane;
      for(var i=0;i<VehicleAnalyticsArray.length;i++){
        VehicleAnalyticsArray[i].timeStamp = new Date();
        if(i==VehicleAnalyticsArray.length-1){
          result.vehicleAnalytics.vehicleDetectionEventsByLane = VehicleAnalyticsArray;
          var updatedObj = result;
          saveData(updatedObj);
          res.send("okay");
        }
      }
    }else{
      res.send("No Data In Database")
    }
  })
})


/* function to save datat to db*/
function saveData(message) {
  var vehicleAnalyticsObj = VehicleAnalyticsModel();
  vehicleAnalyticsObj.vehicleAnalytics = message.vehicleAnalytics;
  vehicleAnalyticsObj.dateTime = new Date();
  vehicleAnalyticsObj.save(function(err,result){
    if(err){
      console.log(err);
    }
  })
}



/*For Real-time Analytics(DIALS) Of Road Analytics to get flow/volume speed timeHeadway and speedHeadway According to Zone selected*/
router.post('/api/roadAnalyticsParametersForLanes', function (req, res) {
  var start = new Date();
  var end = new Date();
  var seconds = req.body.seconds;
  var laneType = req.body.laneSelected;
  end.setSeconds(start.getSeconds()-seconds)
  VehicleAnalyticsModel.find({dateTime: {$gt:end,$lt:start}}, function (err, result) {
    if (err) {
      res.send(err);
    }
    else{
      var vehicleAnalyticsData = {
        speed:0,
        timeHeadway:0,
        spaceHeadway:0,
        volume:0
      };
      if (result.length>0) {
        var analyticsForDateRange = result;
        for (var i=0;i<analyticsForDateRange.length;i++) {
          var vehicleDetectedArray = analyticsForDateRange[i].vehicleAnalytics.vehicleDetectionEventsByLane;
          for (var j = 0; j <vehicleDetectedArray.length; j++) {
            var laneIndex = laneType-1;
            if(vehicleDetectedArray[j].laneIndex === laneType){
              if(vehicleAnalyticsData["speed"]){
                vehicleAnalyticsData["speed"] = vehicleAnalyticsData["speed"]+vehicleDetectedArray[j].speed;
              }
              else{
                vehicleAnalyticsData["speed"] = vehicleDetectedArray[j].speed;
              }
              if(vehicleAnalyticsData["timeHeadway"]){
                vehicleAnalyticsData["timeHeadway"] = vehicleAnalyticsData["timeHeadway"]+
                  vehicleDetectedArray[j].headway["timeHeadway"];
              }
              else{
                vehicleAnalyticsData["timeHeadway"] = vehicleDetectedArray[j].headway["timeHeadway"];
              }
              if(vehicleAnalyticsData["spaceHeadway"]){
                vehicleAnalyticsData["spaceHeadway"] = vehicleAnalyticsData["spaceHeadway"]+
                  vehicleDetectedArray[j].headway["timeHeadway"];;
              }
              else{
                vehicleAnalyticsData["spaceHeadway"] = vehicleDetectedArray[j].headway["timeHeadway"];;
              }
              if(vehicleAnalyticsData["volume"]){
                vehicleAnalyticsData["volume"] = vehicleAnalyticsData["volume"] + 1;
              }
              else{
                vehicleAnalyticsData["volume"]= 1;
              }
            }
            if(j == vehicleDetectedArray.length-1 && i == analyticsForDateRange.length-1){
              var data = {
                speed:0,
                timeHeadway:0,
                spaceHeadway:0,
                volume:0,
              }
              data.speed = vehicleAnalyticsData["speed"]/vehicleAnalyticsData["volume"];
              data.timeHeadway = vehicleAnalyticsData["timeHeadway"]/vehicleAnalyticsData["volume"]
              data.spaceHeadway = vehicleAnalyticsData["spaceHeadway"]/vehicleAnalyticsData["volume"]
              data.volume = vehicleAnalyticsData["volume"];
              if(data.speed &&data.timeHeadway && data.spaceHeadway){
                roadAnalyticsDataForDials(data)
                res.send("Sent Selected Lane Details Successfully")
              }
            }
          }
        }
      }
      else {
        var nullData = {
          speed:0,
          timeHeadway:0,
          spaceHeadway:0,
          volume:0
        };
        roadAnalyticsDataForDials(nullData);
        res.send("NO Data");
      }
    }
  })
})


/*For Real-time(DIALS) Analytics Of Road Analytics to get flow/volume speed timeHeadway and speedHeadway According to Lanes selected*/
router.post('/api/roadAnalyticsParametersForZones', function (req, res) {
  var start = new Date();
  var end = new Date();
  var seconds = req.body.seconds;
  var zoneType = req.body.zoneSelected;
  end.setSeconds(start.getSeconds()-seconds)
  VehicleAnalyticsModel.find({dateTime: {$gt:end,$lt:start}}, function (err, result) {
    if (err) {
      res.send(err);
    }
    else{
      var vehicleAnalyticsData = {
        speed:0,
        timeHeadway:0,
        spaceHeadway:0,
        volume:0
      };
      if (result.length>0) {
        var analyticsForDateRange = result;
        for (var i=0;i<analyticsForDateRange.length;i++) {
          var vehicleDetectedArray = analyticsForDateRange[i].vehicleAnalytics.vehicleDetectionEventsByLane;
          var zoneIndex= analyticsForDateRange[i].vehicleAnalytics.cameraExtrinsics.cameraId;
          for (var j = 0; j <vehicleDetectedArray.length; j++) {
            if(zoneIndex === zoneType){
              if(vehicleAnalyticsData["speed"]){
                vehicleAnalyticsData["speed"] = vehicleAnalyticsData["speed"]+vehicleDetectedArray[j].speed;
              }
              else{
                vehicleAnalyticsData["speed"] = vehicleDetectedArray[j].speed;
              }
              if(vehicleAnalyticsData["timeHeadway"]){
                vehicleAnalyticsData["timeHeadway"] = vehicleAnalyticsData["timeHeadway"]+
                  vehicleDetectedArray[j].headway["timeHeadway"];
              }
              else{
                vehicleAnalyticsData["timeHeadway"] = vehicleDetectedArray[j].headway["timeHeadway"];
              }
              if(vehicleAnalyticsData["spaceHeadway"]){
                vehicleAnalyticsData["spaceHeadway"] = vehicleAnalyticsData["spaceHeadway"]+
                  vehicleDetectedArray[j].headway["timeHeadway"];;
              }
              else{
                vehicleAnalyticsData["spaceHeadway"] = vehicleDetectedArray[j].headway["timeHeadway"];;
              }
              if(vehicleAnalyticsData["volume"]){
                vehicleAnalyticsData["volume"] = vehicleAnalyticsData["volume"] + 1;
              }
              else{
                vehicleAnalyticsData["volume"]= 1;
              }
            }
            if(j == vehicleDetectedArray.length-1 && i == analyticsForDateRange.length-1){
              var data = {
                speed:0,
                timeHeadway:0,
                spaceHeadway:0,
                volume:0,
              }
              data.speed = vehicleAnalyticsData["speed"]/vehicleAnalyticsData["volume"];
              data.timeHeadway = vehicleAnalyticsData["timeHeadway"]/vehicleAnalyticsData["volume"]
              data.spaceHeadway = vehicleAnalyticsData["spaceHeadway"]/vehicleAnalyticsData["volume"]
              data.volume = vehicleAnalyticsData["volume"];
              if(data.speed &&data.timeHeadway && data.spaceHeadway){
                roadAnalyticsDataForDials(data)
                res.send("Sent Selected Zone Details Successfully")
              }
            }
          }
        }
      }
      else {
        var nullData = {
          speed:0,
          timeHeadway:0,
          spaceHeadway:0,
          volume:0
        };
        roadAnalyticsDataForDials(nullData);
        res.send("NO Data");
      }
    }
  })
})


/*For Real-time (DIALS)Analytics Of Road Analytics to get flow/volume speed timeHeadway and speedHeadway According to Individual points selected*/
router.post('/api/roadAnalyticsParametersForIndividualPoints', function (req, res) {
  var start = new Date();
  var end = new Date();
  var seconds = req.body.seconds;
  var zoneType = req.body.zoneSelected;
  var laneType = req.body.laneSelected;
  end.setSeconds(start.getSeconds()-seconds)
  var date = {
    start:start,
    end:end
  }
  VehicleAnalyticsModel.find({dateTime: {$gt:end,$lt:start}}, function (err, result) {
    if (err) {
      res.send(err);
    }
    else{
      var vehicleAnalyticsData = {
        speed:0,
        timeHeadway:0,
        spaceHeadway:0,
        volume:0
      };
      if (result.length>0) {
        var analyticsForDateRange = result;
        for (var i=0;i<analyticsForDateRange.length;i++) {
          var vehicleDetectedArray = analyticsForDateRange[i].vehicleAnalytics.vehicleDetectionEventsByLane;
          var zoneIndex= analyticsForDateRange[i].vehicleAnalytics.cameraExtrinsics.cameraId;
          for (var j = 0; j <vehicleDetectedArray.length; j++) {
            var laneIndex = vehicleDetectedArray[j].laneIndex;
            if(zoneIndex === zoneType){
              if(laneIndex == laneType){
                if(vehicleAnalyticsData["speed"]){
                  vehicleAnalyticsData["speed"] = vehicleAnalyticsData["speed"]+vehicleDetectedArray[j].speed;
                }
                else{
                  vehicleAnalyticsData["speed"] = vehicleDetectedArray[j].speed;
                }
                if(vehicleAnalyticsData["timeHeadway"]){
                  vehicleAnalyticsData["timeHeadway"] = vehicleAnalyticsData["timeHeadway"]+
                    vehicleDetectedArray[j].headway["timeHeadway"];
                }
                else{
                  vehicleAnalyticsData["timeHeadway"] = vehicleDetectedArray[j].headway["timeHeadway"];
                }
                if(vehicleAnalyticsData["spaceHeadway"]){
                  vehicleAnalyticsData["spaceHeadway"] = vehicleAnalyticsData["spaceHeadway"]+
                    vehicleDetectedArray[j].headway["spaceHeadway"];;
                }
                else{
                  vehicleAnalyticsData["spaceHeadway"] = vehicleDetectedArray[j].headway["spaceHeadway"];;
                }
                if(vehicleAnalyticsData["volume"]){
                  vehicleAnalyticsData["volume"] = vehicleAnalyticsData["volume"] + 1;
                }
                else{
                  vehicleAnalyticsData["volume"]= 1;
                }
              }
            }
            if(j == vehicleDetectedArray.length-1 && i == analyticsForDateRange.length-1){
              var data = {
                speed:0,
                timeHeadway:0,
                spaceHeadway:0,
                volume:0,
              }
              data.speed = vehicleAnalyticsData["speed"]/vehicleAnalyticsData["volume"];
              data.timeHeadway = vehicleAnalyticsData["timeHeadway"]/vehicleAnalyticsData["volume"]
              data.spaceHeadway = vehicleAnalyticsData["spaceHeadway"]/vehicleAnalyticsData["volume"]
              data.volume = vehicleAnalyticsData["volume"];
              if(data.speed &&data.timeHeadway && data.spaceHeadway){
                roadAnalyticsDataForDials(data)
                res.send(date+"Sent Individual Points Details Successfully")
              }
            }
          }
        }
      }
      else {
        var nullData = {
          speed:0,
          timeHeadway:0,
          spaceHeadway:0,
          volume:0
        };
        roadAnalyticsDataForDials(nullData);
        res.send("NO Data");
      }
    }
  })
})


/*For Real-time(DIALS) Analytics Of Road Analytics to get flow/volume speed timeHeadway and speedHeadway According to All lanes and zones*/
router.get('/api/roadAnalyticsParametersForAllLanesAndZones/:seconds', function (req, res) {
  var start = new Date();
  var end = new Date();
  var seconds = req.params.seconds;
  end.setSeconds(start.getSeconds()-seconds)
  /*end.setSeconds(startTimeSeconds-endSeconds);*/
  VehicleAnalyticsModel.find({dateTime: {$gt:end,$lt:start}}, function (err, result) {
    if (err) {
      res.send(err);
    }
    else{
      var vehicleAnalyticsData = {
        speed:0,
        timeHeadway:0,
        spaceHeadway:0,
        volume:0
      };
      if (result.length>0) {
        var analyticsForDateRange = result;
        for (var i=0;i<analyticsForDateRange.length;i++) {
          var vehicleDetectedArray = analyticsForDateRange[i].vehicleAnalytics.vehicleDetectionEventsByLane;
          var zoneIndex= analyticsForDateRange[i].vehicleAnalytics.cameraExtrinsics.cameraId;
          for (var j = 0; j <vehicleDetectedArray.length; j++) {
            var laneIndex = vehicleDetectedArray[j].laneIndex;
            if(vehicleAnalyticsData["speed"]){
              vehicleAnalyticsData["speed"] = vehicleAnalyticsData["speed"]+vehicleDetectedArray[j].speed;
            }
            else{
              vehicleAnalyticsData["speed"] = vehicleDetectedArray[j].speed;
            }
            if(vehicleAnalyticsData["timeHeadway"]){
              vehicleAnalyticsData["timeHeadway"] = vehicleAnalyticsData["timeHeadway"]+
                vehicleDetectedArray[j].headway["timeHeadway"];
            }
            else{
              vehicleAnalyticsData["timeHeadway"] = vehicleDetectedArray[j].headway["timeHeadway"];
            }
            if(vehicleAnalyticsData["spaceHeadway"]){
              vehicleAnalyticsData["spaceHeadway"] = vehicleAnalyticsData["spaceHeadway"]+
                vehicleDetectedArray[j].headway["spaceHeadway"];;
            }
            else{
              vehicleAnalyticsData["spaceHeadway"] = vehicleDetectedArray[j].headway["spaceHeadway"];;
            }
            if(vehicleAnalyticsData["volume"]){
              vehicleAnalyticsData["volume"] = vehicleAnalyticsData["volume"] + 1;
            }
            else{
              vehicleAnalyticsData["volume"]= 1;
            }
            if(j == vehicleDetectedArray.length-1 && i == analyticsForDateRange.length-1){
              var data = {
                speed:0,
                timeHeadway:0,
                spaceHeadway:0,
                volume:0,
              }
              data.speed = vehicleAnalyticsData["speed"]/vehicleAnalyticsData["volume"];
              data.timeHeadway = vehicleAnalyticsData["timeHeadway"]/vehicleAnalyticsData["volume"]
              data.spaceHeadway = vehicleAnalyticsData["spaceHeadway"]/vehicleAnalyticsData["volume"]
              data.volume = vehicleAnalyticsData["volume"];
              if(data.speed &&data.timeHeadway && data.spaceHeadway){
                roadAnalyticsDataForDials(data)
                res.send("Sent All Zones Details Successfully")
              }
            }
          }
        }
      }
      else {
        var nullData = {
          speed:0,
          timeHeadway:0,
          spaceHeadway:0,
          volume:0
        };
        roadAnalyticsDataForDials(nullData);
        res.send("NO Data");
      }
    }
  })
})


function roadAnalyticsDataForDials(data){
  var payLoad = {
    vehicleAnalyticsData:data
  }
  SocketIo.emit('solidGuageAnalyticsData',{
    data:payLoad
  })
}

/*For Real-timeRoad Road Analytics by providing type like speed timeHeadWAy speedHeadway*/
router.post('/api/realTime/roadAnalyticsData/type/zoneId', function (req, res) {
  var endSeconds = req.body.seconds;
  var start = new Date();
  var startTimeSeconds = start.getSeconds();
  var end = new Date();
  end.setSeconds(startTimeSeconds-endSeconds);
  /*var endSeconds = new Date();
   var start = new Date();
   start.setTime(0);
   start.setDate(start.getDate())
   var end = new Date();
   end.setDate(start.getDate());*/
  end.setSeconds(end.getSeconds()-req.body.seconds);
  var type=req.body.type;
  var zoneSelected=req.body.zoneSelected;
  VehicleAnalyticsModel.find({dateTime:{$gt : end, $lt : start}},function(err,result){
    if(err){
      res.send(err)
    }
    if(result.length>0){
      if(err){
        res.send(err);
      }else{
        var vehicleAnalyticsData=[0,0,0,0,0];
        var countOfVehicleAnalyticsData=[0,0,0,0,0];
        var analyticsForDateRange = result;
        for (var i = 0; i < analyticsForDateRange.length; i++) {
          var vehicleDetectedArray = analyticsForDateRange[i].vehicleAnalytics.vehicleDetectionEventsByLane;
          var zone = analyticsForDateRange[i].vehicleAnalytics.cameraExtrinsics.cameraId;
          for (var j = 0; j <vehicleDetectedArray.length; j++) {
            var laneIndex = vehicleDetectedArray[j].laneIndex-1;
            if(zoneSelected===zone){
              if(type==='speed'){
                if(vehicleAnalyticsData[laneIndex]){
                  vehicleAnalyticsData[laneIndex] = vehicleAnalyticsData[laneIndex]+vehicleDetectedArray[j][type];
                }else{
                  vehicleAnalyticsData[laneIndex] = vehicleDetectedArray[j][type];
                }
              }
              else{
                if(vehicleAnalyticsData[laneIndex]){
                  vehicleAnalyticsData[laneIndex] = vehicleAnalyticsData[laneIndex]+vehicleDetectedArray[j].headway[type];
                }else{
                  vehicleAnalyticsData[laneIndex] = vehicleDetectedArray[j].headway[type];
                }
              }
              if(countOfVehicleAnalyticsData[laneIndex]){
                countOfVehicleAnalyticsData[laneIndex] = countOfVehicleAnalyticsData[laneIndex]+1;
              }
              else {
                countOfVehicleAnalyticsData[laneIndex] = 1;
              }
            }
            if (i == analyticsForDateRange.length-1 && j == vehicleDetectedArray.length-1) {
              var data = [];
              if(type==="volume"){
                roadAnalyticsDataForRealTimeLaneGraph(countOfVehicleAnalyticsData);
                res.send("Data Sent SuccessFully For RealTime Graph");
              }
              else{
                for(var laneArrayIndex=0;laneArrayIndex<5;laneArrayIndex++){
                  data[laneArrayIndex]=vehicleAnalyticsData[laneArrayIndex]/countOfVehicleAnalyticsData[laneArrayIndex];
                  if(laneArrayIndex===4){
                    roadAnalyticsDataForRealTimeLaneGraph(data);
                    res.send("Data Sent SuccessFully For RealTime Graph");
                  }
                }
              }
            }
          }
        }
      }
    }else{
      var nullData=[0,0,0,0,0]
      roadAnalyticsDataForRealTimeLaneGraph(nullData);
      res.send("no data")
    }
  })
})


/*to get  Historical Road Analytics by Date ,Lanes selected and type Of data(speed and Occupancy)*/
router.post('/api/historicalData/roadAnalyticsByZoneAndType', function (req, res) {
  var start = new Date(req.body.startDate);
  var end = new Date(req.body.endDate);
  /*var start = new Date();
   var end = new Date();
   end.setDate(end.getDate()-3);*/
  start.setHours(0);
  var type = req.body.type;
  var zoneSelected = req.body.zoneSelected;
  var today = new Date();
  if (start.getMonth() == today.getMonth() && start.getFullYear() == today.getFullYear() && start.getDate() == today.getDate()) {
    end = today;
  }
  else {
    end.setHours(23);
  }
  var date = {
    one:start.getDate(),
    2:end.getDate()
  }
  VehicleAnalyticsModel.find({dateTime: {$gt: start, $lt: end}}, function (err, result) {
    if (err) {
      res.send(err);
    }
    else {
      var vehicleAnalyticsData =
        [
          [],[],[],[],[]
        ];
      var count = [[],[],[],[],[]];
      if (result.length > 0) {
        var analyticsForDateRange = result;
        for (var i = 0; i < analyticsForDateRange.length; i++) {
          var zone = analyticsForDateRange[i].vehicleAnalytics.cameraExtrinsics.cameraId;
          var vehicleDetectedTrafficArray = analyticsForDateRange[i].vehicleAnalytics.vehicleDetectionEventsByLane;
          var hour = new Date(analyticsForDateRange[i].dateTime).getHours();
          for (var j = 0; j < vehicleDetectedTrafficArray.length; j++) {
            var laneIndex = vehicleDetectedTrafficArray[j].laneIndex-1;
            var zoneIndex = "zone"+zone;
            if(zoneSelected==zone){
              if(type=="speed"){
                if(vehicleAnalyticsData[laneIndex][hour]){
                  vehicleAnalyticsData[laneIndex][hour] = vehicleAnalyticsData[laneIndex][hour] + vehicleDetectedTrafficArray[j][type];
                }else{
                  vehicleAnalyticsData[laneIndex][hour] = vehicleDetectedTrafficArray[j][type];
                }
              }
              else{
                if(vehicleAnalyticsData[laneIndex][hour]){
                  vehicleAnalyticsData[laneIndex][hour] = vehicleAnalyticsData[laneIndex][hour] + vehicleDetectedTrafficArray[j].headway[type];
                }else{
                  vehicleAnalyticsData[laneIndex][hour] = vehicleDetectedTrafficArray[j].headway[type];
                }
              }
              if(count[laneIndex][hour]){
                count[laneIndex][hour] = count[laneIndex][hour] + 1;
              }else{
                count[laneIndex][hour] = 1;
              }
            }
            if (j == vehicleDetectedTrafficArray.length - 1 && i == analyticsForDateRange.length - 1) {
              if(type!="flow"){
                var data =[[],[],[],[],[]];
                for(var laneArray=0;laneArray<5;laneArray++){
                  data[laneArray]=vehicleAnalyticsData[laneArray].divide(count[laneArray]);
                  if(laneArray==4){
                    res.send(data);
                  }
                }
              }else{
                res.send(count)
              }
            }
          }
        }
      }
      else {
        res.send("NO Data");
      }
    }
  })
})

function getDates(date) {
  var dateArray = [];
  var  stopDate= new Date();
  stopDate.setDate(stopDate.getDate())
  var currentDate = new Date();
  currentDate.setDate(stopDate.getDate())
  currentDate.setMinutes(stopDate.getMinutes()-15)
  currentDate.setSeconds(0);
  while (currentDate.getMinutes()<=stopDate.getMinutes()) {

    currentDate.setSeconds(currentDate.getSeconds()+10);
    dateArray.push(currentDate);
  }
  return dateArray;
};

function roadAnalyticsDataForRealTimeLaneGraph(data){
  var payLoad = {
    vehicleAnalyticsData:data,
    timeStamp:new Date()
  }
  SocketIo.emit('analyticsData',{
    data:payLoad
  })
}

function getDates(date) {
  var dateArray = [];
  var  stopDate= new Date();
  stopDate.setDate(stopDate.getDate())
  var currentDate = new Date();
  currentDate.setDate(stopDate.getDate())
  currentDate.setMinutes(stopDate.getMinutes()-15)
  currentDate.setSeconds(0);
  while (currentDate.getMinutes()<=stopDate.getMinutes()) {

    currentDate.setSeconds(currentDate.getSeconds()+10);
    dateArray.push(currentDate);
  }
  return dateArray;
}
var seconds = 30;
var laneSelected=[2,3];
var zoneSelected = [1];
var vehicleAnalyticsMetrics={
  terminalRelated:{
    timer:2000,
    intervalId:0,
    averageDataOfSeconds:30,
    lanesSelected:[2,3]
  }
}
router.get('/api/getVehicleAnalyticsForDials', function (req, res) {
  getAverageDataOfVehicleAnalyticsForTerminal();
  res.send("INterval started")
})
router.get('/api/startAnalytics', function (req, res) {
  //startTimerForVehicleAnalyticsMetricsForTerminal();
  res.send("INterval started")
})
router.get('/api/stopAnalytics', function (req, res) {
  //clearInterval(vehicleAnalyticsMetrics.terminalRelated.intervalId);
  res.send("INterval stopped--------------------------")
})
router.get('/api/changeAverageDataOfSeconds/:seconds', function (req, res) {
  seconds=req.params.seconds;
  res.send(seconds)
});
router.get('/selectedTerminals/:terminal', function (req, res) {
  var terminal =req.params.terminal;
  TerminalZonesAndLaneDetails.findOne({"terminal.terminalId":terminal},function(err,result){
    if(err){
      res.send(err)
    }else{
      /*laneSelected = result.lanesSelected;
      zoneSelected = result.zonesSelected;*/
      var laneSelected = result.terminal.zoneDetails[0].laneIndex;
      var zoneSelected = result.terminal.zoneDetails[0].zoneId;
      res.send(laneSelected)
    }
  });
})
/*startTimerForVehicleAnalyticsMetricsForTerminal();*/
function startTimerForVehicleAnalyticsMetricsForTerminal(){
  vehicleAnalyticsMetrics.terminalRelated.intervalId =setInterval(function() {
    getAverageDataOfVehicleAnalyticsForTerminal();
  },vehicleAnalyticsMetrics.terminalRelated.timer);
}
function getAverageDataOfVehicleAnalyticsForTerminal(){
  var start = new Date();
  var end = new Date();
  start.setSeconds(start.getSeconds()-seconds);
  var lanesSelected = laneSelected;/*
  var zoneIndex = 1;*/
  VehicleAnalyticsModel.aggregate(
    [
      {
        $match: {
          dateTime:{$gt:start,$lt:end}
          ,"vehicleAnalytics.cameraExtrinsics.cameraId":{$in:zoneSelected}
      }
      },
      {$unwind: "$vehicleAnalytics.vehicleDetectionEventsByLane"},
      {
        $match: {
          "vehicleAnalytics.vehicleDetectionEventsByLane.laneIndex":{$in:lanesSelected}
        }
      },
      {$group:
      {
        _id:null,
        speed: {$avg: "$vehicleAnalytics.vehicleDetectionEventsByLane.speed"},
        timeHeadway: {$avg: "$vehicleAnalytics.vehicleDetectionEventsByLane.headway.timeHeadway"},
        spaceHeadway: {$avg: "$vehicleAnalytics.vehicleDetectionEventsByLane.headway.spaceHeadway"},
        volume:{$sum:1}
      }
      },
      {$sort: {_id:1}},
      {$project: { _id: 0,speed:"$speed",timeHeadway:"$timeHeadway",spaceHeadway:"$spaceHeadway",volume:"$volume"}}
    ],
    function (err, result) {
      if (err) {
        console.log(err);
        return;
      }
      var data;
      if(result.length>0){
         data = result[0]
      }else{
        data={
              speed:0,
              timeHeadway:0,
              spaceHeadway:0,
              volume:0
         }

      }
      SocketIo.emit('dialDataForVehicle',data)

    });
}


/*RT_LANE_GRAPH*/
router.get('/api/setTheZoneTypeSelectedForRTGraph/:zoneId', function (req, res) {
  var zoneSelected = req.params.zoneId;
  vehicleDetectionNotifier.setZoneType(zoneSelected);
  res.send("Zone Selected "+zoneSelected)
});
router.get('/api/setTheTypeOfParametersNeeded/:parameter', function (req, res) {
  var typeSelected = req.params.parameter;
  vehicleDetectionNotifier.setParameterType(typeSelected);
  res.send("Parameter Type Selected "+typeSelected)
});
function vehicleDetectionNotifierMethod(result){
  notifyClientsRTDetails.vehicleNotifier.notifyVehiclesDetected(result);
};

router.get('/userData/vehicleAnalytics/:startDate/:endDate',function(req, res){/**/
  res.set('Content-Type', 'application/octet-stream');
  res.set('Content-Disposition', 'disp-ext-type; filename=VehicleAnalyticsDataData.json;creation-date-parm:'+new Date());
  var startTime = new Date(req.params.startDate);
  var endTime = new Date(req.params.endDate);

  res.write('[')
  var stream=VehicleAnalyticsModel.find({"dateTime":
  {$gt:startTime,$lt:endTime}},{"_id":0,"vehicleAnalytics.vehicleDetectionEventsByLane":1,"vehicleAnalytics.roadInfo":1}).stream();
  stream.on('data', function (doc) {
    res.write(JSON.stringify(doc)+',')
  })
  stream.on('error', function (err) {
    console.log(err.stack)
  })
  stream.on('close', function (doc) {
    console.log("done")
    res.write('{}]')
    res.end();
  })
})


router.get('/getSocketData',function(req, res){
  res.send(socketConnectionDetailsCollection.getSocketArrayDetails())

})


function startIntervalForRefreshingVehicleData() {
 var interval = setInterval(function () {
   triggerAggregationForAnalyticsPage.terminalLaneDataPusher.refreshAggregationCalculations();
 },5000);
 }
startIntervalForRefreshingVehicleData()

/*router.post('/historicalData/vehicleAnalytics',function(req, res){
  var zoneId = parseInt(req.body.zoneId);
  var parameterSelected = req.body.parameterSelected;
  var seriesSelected = parseInt(req.body.seriesSelected)
  var historicalData=[]
  var startDate = new Date(req.body.startDate);
  var endDate =   new Date(req.body.startDate);
  var lanesSelected= [];
  var maxLaneForSelectedZone = config.maxLanesOfZone[zoneId-1]
  for(var i=1;i<=maxLaneForSelectedZone;i++){
    lanesSelected.push(i)
  }
  startDate.setMinutes(startDate.getMinutes()-seriesSelected);
  historicalDataAggregationModule.vehicleAggregate.getAggregatedHistoricalData(startDate,endDate,parameterSelected,zoneId,lanesSelected,
    function(result){
      if(result && result.length>0){
        for(i = 0;i<result.length;i++){
          var parameterAggregation = result[i].occupancy;
          historicalData[i]=new Array(seriesSelected);
          historicalData[i].fill(null)
          for(var data=0;data<parameterAggregation.length;data++){
            var parameterObj = parameterAggregation[data];
            historicalData[i][parameterObj.minutes%seriesSelected]=Math.round(parameterObj.occupancy);
            if(historicalData.length==result.length && data==parameterAggregation.length-1){
              res.send(historicalData)
            }
          }
        }
      }else{
        res.send(historicalData)
      }
  })
})*/


/*api to get historical data*/
router.post('/historicalData/vehicleAnalytics/secondWiseData',function(req, res){
  var zoneId = parseInt(req.body.zoneId);
  var parameterSelected = req.body.parameterSelected;
  var seriesSelected = parseInt(req.body.seriesSelected)
  var startDate = new Date();
  startDate.setMinutes(startDate.getMinutes()-seriesSelected);
  var endDate =   new Date();
  var lanesSelected= [];
  var maxLaneForSelectedZone = config.maxLanesOfZone[zoneId-1]
  for(var i=1;i<=maxLaneForSelectedZone;i++){
    lanesSelected.push(i)
  }
  historicalDataAggregationModule.vehicleAggregate.getAggregatedHistoricalDataOverSeconds
  (endDate,endDate,seriesSelected,parameterSelected,zoneId,lanesSelected,function(result){
    res.send(result)
  })
})

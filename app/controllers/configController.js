/**
 * Created by Suhas on 5/19/2016.
 */
var express = require('express'),
  router = express.Router(),
  config=require('../../config/config');

var bCrypt = require('bcrypt-nodejs');
var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var expressJwt = require('express-jwt'); //https://npmjs.org/package/express-jwt
var secret = 'this is the secrete password';
module.exports = function (app) {
  app.use('/', router);
};
router.use(function(err, req, res, next){
  if (err.constructor.name === 'UnauthorizedError') {
    res.status(401).send('Unauthorized Token Is not present');
  }
});
router.get('/api/maxLane/:zoneId', function (req, res, next) {
  var zoneId =parseInt(req.params.zoneId);
  var maxLane = {
    maxLane:config.maxLanesOfZone[zoneId-1]
  }
  res.send(maxLane);
});

/**
 * Created by Suhas on 1/19/2016.
 */
var express = require('express'),
  router = express.Router(),
  SocketIo = require('../../config/socketIo').getSocketIoServer();





/*********************************************RT-GRAPH****************************************************************************/
/*
var selectedType = {
  parametersType:"speed",
  zoneSelected:1
}
var laneWisDataCurrent=[
    {val:0,time:''},
    {val:0,time:''},
    {val:0,time:''},
    {val:0,time:''},
    {val:0,time:''}
  ];
function clearLaneWiseDAtaCurrent(){
  laneWisDataCurrent=
    [
      {val:0,time:''},
      {val:0,time:''},
      {val:0,time:''},
      {val:0,time:''},
      {val:0,time:''}
    ];
}
router.get('/setTheZoneTypeSelectedForRTGraph/:zoneId', function (req, res) {
  clearLaneWiseDAtaCurrent();
  selectedType.zoneSelected=req.params.zoneId;
  res.send(selectedType)
});
router.get('/setTheTypeOfParametersNeeded/:parameter', function (req, res) {
  clearLaneWiseDAtaCurrent();
  selectedType.parametersType=req.params.parameter;
  res.send(selectedType)
});
var notifyVehicleDetectedInRealTime = function(notifyVehicleDetectedData,callback){
  if(selectedType.parametersType!='occupancy'){
    if(selectedType.zoneSelected == notifyVehicleDetectedData.vehicleAnalytics.cameraExtrinsics.cameraId){
      var vehicleDetectedArray = notifyVehicleDetectedData.vehicleAnalytics.vehicleDetectionEventsByLane;
      var type = selectedType.parametersType;
      for(var i=0;i<vehicleDetectedArray.length;i++){
        var laneIndex = vehicleDetectedArray[i].laneIndex;
        if(selectedType.parametersType!="speed"){
          laneWisDataCurrent[laneIndex-1].val = vehicleDetectedArray[i].headway[type];
        }
        else{
          laneWisDataCurrent[laneIndex-1].val = vehicleDetectedArray[i][type];
        }
        laneWisDataCurrent[laneIndex-1].time = new Date();
        if(i==vehicleDetectedArray.length-1){
          callback(null,laneWisDataCurrent)
        }
      }
    }
  }
}
*/


module.exports = function (app) {/*
  module.exports = notifyVehicleDetectedInRealTime;*/
  app.use('/', router);
};


/**
 * Created by zendynamix on 11-01-2016.
 */

/*var zmq = require('zmq');*/
var express = require('express'),
  router = express.Router(),
  mongoose = require('mongoose'),
  plotMap = mongoose.model('plotMap'),
  plotMap2 = mongoose.model('mapDetailsNew'),
  zoneSegmentInLaneDetails = mongoose.model('zoneSegmentInLaneDetails'),
  cameraMarkerDetails = mongoose.model('cameraMarkerDetails')

var random = require('mongoose-simple-random');
var request = require("request");

var bCrypt = require('bcrypt-nodejs');
var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var expressJwt = require('express-jwt'); //https://npmjs.org/package/express-jwt
var secret = 'this is the secrete password';
module.exports = function (app) {
  app.use('/', router);
};
var SocketIo = require('../../config/socketIo').getSocketIoServer();


router.use(function(err, req, res, next){
  if (err.constructor.name === 'UnauthorizedError') {
    res.status(401).send('Unauthorized Token Is not present');
  }
});
router.get('/osrmService/:lat1/:lon1/:lat2/:lon2', function (req, res, next) {
  request("http://api-osrm-routed-production.tilestream.net/viaroute?instructions=true&alt=true&loc=" + req.params.lat1 + "," + req.params.lon1 + "&loc=" + req.params.lat2 + "," + req.params.lon2, function (error, response, body) {
    res.send(body);
  });
});
router.get('/api/yoursRoute', function (req, res, next) {
  Notify.findOne({notifyId:msgId[Math.floor(Math.random()*msgId.length)]},function(err,notification){
    SocketIo.emit('msg',{
      //data:notification
      data:getMsg()
    })

  })
  res.send('ok');
});
router.get('/api/newPlotMsg', function (req, res, next) {

    plotMap2.find(function(err,resplotData){
      if(err){
        res.send(err);
      }else{
        SocketIo.emit('plotData',{
          data:resplotData
        })
        res.send(resplotData);
      }

    })

});
router.get('/ciscoService', function (req, res, next) {
  request("https://58.185.36.20/api/contextaware/v1/location/tags.json", function (error, response, body) {
    res.send(body);
  });
});


router.get('/api/zoneSegmentInLaneDetails', function (req, res, next) {
  zoneSegmentInLaneDetails.find(function(err,reponse){
    if(err){
      res.send(err);
    }
    res.send(reponse);
  })
});

router.get('/api/cameraMarkersDetails', function (req, res, next) {
  cameraMarkerDetails.find(function(err,reponseObj){
    console.log(reponseObj)
    if(err){
      res.send(err);
    }
    res.send(reponseObj);
  })
});








/**
 * Created by Suhas on 1/22/2016.
 */
var express = require('express'),
  router = express.Router(),
  mongoose = require('mongoose'),
  VehicleAnalyticsModel = mongoose.model('vehicleAnalytics'),
  TerminalZonesAndLaneDetails = mongoose.model('terminalModel'),
  random = require('mongoose-simple-random'),
  request = require("request"),
  moment = require("moment"),
  SocketIo = require('../../config/socketIo').getSocketIoServer(),
  vehicleDetectionNotifier =require('../rtGraphNotifier/vehicleAnalytics');

module.exports = function (app) {
  app.use('/', router);
};


var seconds = 30;
var laneSelected=[2,3];
var zoneSelected = [1,2,3];
router.get('/vehicleAnalytics/selectedTerminals/:terminal', function (req, res) {
  var terminal =req.params.terminal;
  TerminalZonesAndLaneDetails.findOne({"terminalId":terminal},function(err,result){
    if(err){
      res.send(err)
    }else{
      laneSelected = result.lanesSelected;
      zoneSelected = result.zonesSelected;
      res.send("selected Terminals No "+terminal);
    }
  });
})
router.get('/vehicleAnalytics/selectedAverageSeconds/:seconds', function (req, res) {
  seconds=req.params.seconds;
  res.send("selected Time in seconds "+seconds);
})

// Find a single random document of vehicleAnalytics
router.get('/vehicleAnalytics/averagedParameters', function (req, res) {
  console.log("call made")
  var start = new Date();
  var end = new Date();
  start.setSeconds(start.getSeconds()-seconds);
  var lanesSelected = laneSelected;
  VehicleAnalyticsModel.aggregate(
    [
      {
        $match: {
          dateTime:{$gt:start,$lt:end}
          ,"vehicleAnalytics.cameraExtrinsics.cameraId":{$in:zoneSelected}
        }
      },
      {$unwind: "$vehicleAnalytics.vehicleDetectionEventsByLane"},
      {
        $match: {
          "vehicleAnalytics.vehicleDetectionEventsByLane.laneIndex":{$in:lanesSelected}
        }
      },
      {$group:
      {
        _id:null,
        speed: {$avg: "$vehicleAnalytics.vehicleDetectionEventsByLane.speed"},
        timeHeadway: {$avg: "$vehicleAnalytics.vehicleDetectionEventsByLane.headway.timeHeadway"},
        spaceHeadway: {$avg: "$vehicleAnalytics.vehicleDetectionEventsByLane.headway.spaceHeadway"},
        volume:{$sum:1}
      }
      },
      {$sort: {_id:1}},
      {$project: { _id: 0,speed:"$speed",timeHeadway:"$timeHeadway",spaceHeadway:"$spaceHeadway",volume:"$volume"}}
    ],
    function (err, result) {
      if (err) {
        console.log(err);
        return;
      }
      var data;
      if(result.length>0){
        data = result[0];
        res.send(data)
      }else{
        data={
          speed:0,
          timeHeadway:0,
          spaceHeadway:0,
          volume:0
        };
        res.send(data)
      }
    });
})
router.get('/vehicleAnalytics/zoneLaneDetails/:terminalNo', function (req, res) {
  var terminal =req.params.terminal;
  TerminalZonesAndLaneDetails.findOne({"terminalId":terminal},function(err,result){
    if(err){
      res.send(err)
    }else{
      laneSelected = result.lanesSelected;
      zoneSelected = result.zonesSelected;
      res.send("Terminal Value Changed")
    }
  });
})
router.get('/vehicleAnalytics/averagedParameters/:seconds', function (req, res) {
  seconds=req.params.seconds;
});

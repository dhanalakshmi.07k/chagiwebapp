/**
 * Created by Suhas on 2/5/2016.
 */
  var express = require('express');
  var Q = require("q");
  router = express.Router(),
  SocketIo = require('../../config/socketIo').getSocketIoServer(),
  VehicleAnalyticsModel = require("../models/vehicleAnalytics"),
  TerminalZonesAndLaneDetails = require("../models/terminalGenerator"),
  wholeZoneTrafficInfoModel = require('../models/wholeZoneTrafficInfo');

  var vehicleWholeZoneAnalyticsAggregate = require('../aggregators');


  module.exports = function (app) {
  app.use('/', router);
};
var socketConnectionDetailsCollection = require("../multipleSocket");
SocketIo.on('connection', function (newSocket) {
 if (newSocket) {
   newSocket.on("setAnalyticsConfiguration", function (configDetails) {
     socketConnectionDetailsCollection.updateSocketArrayObj(newSocket.id,configDetails);
     getTerminalLaneDetails(configDetails, newSocket.id);
     getWholeZoneDetails(configDetails, newSocket.id);
   })
 }
 });


function getTerminalLaneDetails(configDetails, connId) {
  TerminalZonesAndLaneDetails.findOne({"terminal.terminalId": configDetails.terminalDetails.terminalSelected},
    function (err, result) {
    if (err) {
      res.send(err)
    } else {
      var promises = [];
      var valueCount = 0;
      timeSelected = configDetails.terminalDetails.timeSelected;
      for (var k = 0; k < result.terminal.zoneDetails.length; k++) {
        var laneSelected = result.terminal.zoneDetails[k].laneIndex;
        var zoneSelected = result.terminal.zoneDetails[k].zoneId;
        var promise = vehicleWholeZoneAnalyticsAggregate.vehicleAnalytics.sendVehicleAnalyticsDetailsForCards(laneSelected, zoneSelected, timeSelected).then(function (data) {
          return Q(data);
        });
        promises.push(promise);
      }
      Q.all(promises).then(function (resData) {
        var avgSpeed = 0, avgTimeHeadway = 0, avgSpaceHeadway = 0, avgvolume = 0;
        for (var j = 0; j < resData.length; j++) {
          if (resData[j].speed != 0) {
            valueCount++;
          }
          avgSpeed += resData[j].speed;
          avgTimeHeadway += resData[j].timeHeadway;
          avgSpaceHeadway += resData[j].spaceHeadway;
          avgvolume += resData[j].volume;

        }
        var data = {}
        if (valueCount === 0) {
          data.speed = avgSpeed;
          data.timeHeadway = avgTimeHeadway;
          data.spaceHeadway = avgSpaceHeadway;
          data.volume = avgvolume;
        }
        else {
          data.speed = avgSpeed / valueCount;
          data.timeHeadway = avgTimeHeadway / valueCount;
          data.spaceHeadway = avgSpaceHeadway / valueCount;
          data.volume = avgvolume / valueCount;
        }

        SocketIo.to(connId).emit('dialDataForVehicle', data);
        SocketIo.to(connId).emit('getTerminalValue', data);
      });

    }
  });
}


function getWholeZoneDetails(configDetails, connId) {
  TerminalZonesAndLaneDetails.findOne({"terminal.terminalId":
  configDetails.terminalDetails.terminalSelected}, function (err, result) {
    if (err) {
      res.send(err)
    } else {
      var promisesWholeZone = [];
      var valueCountWholeZoneAnalytics = 0;
      timeSelected = configDetails.terminalDetails.timeSelected;
      for (var l = 0; l < result.terminal.zoneDetails.length; l++) {
        var laneSelected = result.terminal.zoneDetails[l].laneIndex;
        var zoneSelected = result.terminal.zoneDetails[l].zoneId;
        var promiseWholeZoneAnalytics =vehicleWholeZoneAnalyticsAggregate.wholeZoneAnalytics.getAverageDataOfWholeZoneAnalyticsForTerminal(laneSelected, zoneSelected, timeSelected).then(function (data) {
          return Q(data);
        });
        promisesWholeZone.push(promiseWholeZoneAnalytics);
      }
      Q.all(promisesWholeZone).then(function (resData) {
        var avgOccupancy = 0;
        var maxLevelOfService = [];
        for (var j = 0; j < resData.length; j++) {

          if (resData[j].occupancy != 0) {
            valueCountWholeZoneAnalytics++;
          }
          avgOccupancy += resData[j].occupancy;
          maxLevelOfService.push(resData[j].levelOfService)
        }

        var data = {};
        var maxLOS=0;
        if(maxLevelOfService.length!=0){
          maxLOS=maxLevelOfService.max();
        }
        if (valueCountWholeZoneAnalytics === 0) {
          data.occupancy = avgOccupancy;
          data.levelOfService = maxLOS;
          data.levelServiceArray=maxLevelOfService;
        }
        else {
          data.occupancy = avgOccupancy / valueCountWholeZoneAnalytics;
          data.levelOfService = maxLOS;
          data.levelServiceArray=maxLevelOfService;
        }
        SocketIo.to(connId).emit('dialDataForWholeZone', data)
        SocketIo.to(connId).emit('getTerminalValue', data)
      });

    }
  });
}
Array.prototype.max = function() {
  return Math.max.apply(null, this);
};




function refreshVehicleCarddata() {
  var vehicleDetailsOfAClientArray = socketConnectionDetailsCollection.getSocketArrayDetails();

  if (vehicleDetailsOfAClientArray.length > 0) {
    for (var j = 0; j < vehicleDetailsOfAClientArray.length; j++) {
      var vehicleDetailsOfAClient = vehicleDetailsOfAClientArray[j];
      var terminalDetails = vehicleDetailsOfAClientArray[j].details.terminalDetails;
      /*vehicleAnalyticsCardDataPublisher(vehicleDetailsOfAClient.details, vehicleDetailsOfAClient.id)*/
      /*wholeZoneAnalyticsCardDataPublisher(vehicleDetailsOfAClient.details, vehicleDetailsOfAClient.id)*/
    }
  }
}
function startIntervalForRefreshingVehicleData() {
  var interval = setInterval(function () {
    /*refreshVehicleCarddata();*/
  }, 5000);
}
startIntervalForRefreshingVehicleData();

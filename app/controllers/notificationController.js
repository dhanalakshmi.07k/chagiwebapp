/**
 * Created by Suhas on 1/21/2016.
 */
var express = require('express'),
  router = express.Router(),
  mongoose = require('mongoose')
smsSender = require('../smsNotifier'),
  smsPhoneNoForNotification = mongoose.model('smsPhoneNoNotificationModel');
module.exports = function (app) {
  app.use('/', router);
};
// To set the phone no to which sms is sent
router.get('/api/phoneNoForSms/:phoneNo', function (req, res) {
  smsPhoneNoForNotification.findOne({"phoneId": 1}, function (err, result) {
    if (err) {
      res.send(err);
    } else if (result) {
      result.phoneNo = req.params.phoneNo
      result.save(function (err1, response) {
        if (err1) {

        } else {
          res.send(response)
        }
      })
    }
    /*res.send("msg sent");*/
  })


})
// To get the phone no to which sms is sent
router.get('/api/phoneNoForSms', function (req, res) {
  smsPhoneNoForNotification.findOne({"phoneId": 1}, function (err, result) {
    if (err) {
      res.send(err);
    } else if (result) {
      var phoneDetails = {
        phoneNo: result.phoneNo
      }
      res.send(phoneDetails)
    }
    /*res.send("msg sent");*/
  })


})
// To send sms to the phone present in database
router.post('/api/sendNotificationSms', function (req, res) {
  var msg = req.body.msg;
  smsPhoneNoForNotification.findOne({"phoneId": 1}, function (err, result) {
    if (err) {
      res.send(err);
    } else if (result) {
      smsSender.sendNotificationSMS(result.phoneNo, msg);
    }
    res.send("msg sent");
  })


})
router.get('/api/toggleSmsNotificationNotifier/:flag', function (req, res) {
  smsSender.setAutoSMSNotifierFlag(req.params.flag);
  res.send("the value of AutoSMS has been set to :" + req.params.flag);
})
router.get('/api/getToggleSmsNotificationNotifier/flag', function (req, res) {
  var autoSendSMSFlag = smsSender.getAutoSMSNotifierFlag;
  res.send(autoSendSMSFlag)
})

/**
 * Created by zendynamix on 09-01-2016.
 */
var express = require('express'),
  router = express.Router(),
  mongoose = require('mongoose'),
  WholeZoneTrafficInfoModel = mongoose.model('wholeZoneTrafficInfo'),
  TerminalZonesAndLaneDetails = mongoose.model('terminalModel'),
  VehicleAnalyticsModel = mongoose.model('vehicleAnalytics');

module.exports = function (app) {
  app.use('/', router);
};

/*new*/
var seconds = 30;
var laneSelected=[2,3];
var zoneSelected = [1];
var wholeZoneAnalyticsMetrics={
  terminalRelated:{
    timer:5000,
    intervalId:0,
    averageDataOfSeconds:30,
    lanesSelected:[2,3]
  },
  levelOfServiceForMap:{
    timer:5000,
    intervalId:0,
  }
}
var intervalId;
router.get('/getWholeZoneAnalyticsForDials', function (req, res) {
  getAverageDataOfWholeZoneAnalyticsForTerminal();
  res.send("INterval started")
})
router.get('/startWholeZoneAnalytics', function (req, res) {
  intervalId =setInterval(function() {
    getAverageDataOfWholeZoneAnalyticsForTerminal();
  },wholeZoneAnalyticsMetrics.terminalRelated.timer);
  res.send("WholeZone Data Interval Started")
})
router.get('/stopWholeZoneAnalytics', function (req, res) {
  clearInterval(intervalId);
  res.send("INterval stopped--------------------------")
})
router.get('/changeAverageDataOfSecondsForWholeZone/:seconds', function (req, res) {
  seconds=req.params.seconds;
  res.send(seconds)
});
router.get('/selectedTerminalsForWholeZone/:terminal', function (req, res) {
  var terminal =req.params.terminal;
  TerminalZonesAndLaneDetails.findOne({"terminalId":terminal},function(err,result){
    if(err){
      res.send(err)
    }else{
      laneSelected = result.lanesSelected;
      zoneSelected = result.zonesSelected;
      res.send(result)
    }
  });
  /*if(terminal == 1){
   laneSelected = [2,3]
   }else if(terminal == 2){
   laneSelected = [4,5]
   }else if(terminal == 3){
   laneSelected = [1,2]
   }*/

});
function getAverageDataOfWholeZoneAnalyticsForTerminal(){
  var start = new Date();
  var end = new Date();
  start.setSeconds(start.getSeconds()-seconds);
  var lanesSelected = laneSelected;
  var zonesSelected = zoneSelected;
  WholeZoneTrafficInfoModel.aggregate(
    [
      /*matching through dateTime,laneIndex,zoneIndex*/
      {
        $match: {
          dateTime:{$gt:start,$lt:end}
          ,"vehicleAnalytics.wholeZoneTrafficInformation.laneIndex":{$in:lanesSelected}
          ,"vehicleAnalytics.cameraExtrinsics.cameraId":{$in:zonesSelected}
        }
      },
      {$unwind: "$vehicleAnalytics.wholeZoneTrafficInformation"},
      {
        $group: {
          _id:null,
          levelOfService: {$avg: "$vehicleAnalytics.wholeZoneTrafficInformation.levelofService"},
          occupancy: {$avg: "$vehicleAnalytics.wholeZoneTrafficInformation.zoneOccupancy"}
        }
      },
      {$sort: {_id:1}},
      {$project: { _id: 0,levelOfService:"$levelOfService",occupancy:"$occupancy"}}
    ],
    function (err, result) {
      if (err) {
        console.log(err);
        return;
      }
      var data;
      if(result.length>0){
        data = result[0]
      }else{
        data={
          occupancy:0,
          levelOfService:0
        }
      }
      SocketIo.emit('dialDataForWholeZone',data)
    });
}


var vehicleAnalyticsMetrics={
  terminalRelated:{
    timer:5000,
    intervalId:0,
    averageDataOfSeconds:30,
    lanesSelected:[2,3]
  }
}
router.get('/api/getVehicleAnalyticsForDials', function (req, res) {
  getAverageDataOfVehicleAnalyticsForTerminal();
  res.send("INterval started")
})
router.get('/api/startAnalytics', function (req, res) {
  startTimerForVehicleAnalyticsMetricsForTerminal();
  res.send("INterval started")
})
router.get('/api/stopAnalytics', function (req, res) {
  clearInterval(vehicleAnalyticsMetrics.terminalRelated.intervalId);
  res.send("INterval stopped--------------------------")
})
router.get('/api/changeAverageDataOfSeconds/:seconds', function (req, res) {
  seconds=req.params.seconds;
  res.send(seconds)
});
router.get('/api/selectedTerminals/:terminal', function (req, res) {
  var terminal =req.params.terminal;
  TerminalZonesAndLaneDetails.findOne({"terminal.terminalId":terminal},function(err,result){
    if(err){
      res.send(err)
    }else{
      var laneSelected = result.terminal.zoneDetails[0].laneIndex;
      var zoneSelected = result.terminal.zoneDetails[0].zoneId;
     /* laneSelected = result.lanesSelected;
      zoneSelected = result.zonesSelected;*/
      res.send(laneSelected)
    }
  });
})
function startTimerForVehicleAnalyticsMetricsForTerminal(){
  vehicleAnalyticsMetrics.terminalRelated.intervalId =setInterval(function() {
    getAverageDataOfVehicleAnalyticsForTerminal();
  },vehicleAnalyticsMetrics.terminalRelated.timer);
}
function getAverageDataOfVehicleAnalyticsForTerminal(){
  var start = new Date();
  var end = new Date();
  start.setSeconds(start.getSeconds()-seconds);
  var lanesSelected = laneSelected;/*
   var zoneIndex = 1;*/
  VehicleAnalyticsModel.aggregate(
    [
      {
        $match: {
          dateTime:{$gt:start,$lt:end},
          "vehicleAnalytics.vehicleDetectionEventsByLane.laneIndex":{$in:lanesSelected}
          ,"vehicleAnalytics.cameraExtrinsics.cameraId":{$in:zoneSelected}
        }
      },
      {$unwind: "$vehicleAnalytics.vehicleDetectionEventsByLane"},
      {$group:
      {
        _id:null,
        speed: {$avg: "$vehicleAnalytics.vehicleDetectionEventsByLane.speed"},
        timeHeadway: {$avg: "$vehicleAnalytics.vehicleDetectionEventsByLane.headway.timeHeadway"},
        spaceHeadway: {$avg: "$vehicleAnalytics.vehicleDetectionEventsByLane.headway.spaceHeadway"},
        volume:{$sum:1}
      }
      },
      {$sort: {_id:1}},
      {$project: { _id: 0,speed:"$speed",timeHeadway:"$timeHeadway",spaceHeadway:"$spaceHeadway",volume:"$volume"}}
    ],
    function (err, result) {
      if (err) {
        console.log(err);
        return;
      }
      var data;
      if(result.length>0){
        data = result[0]
      }else{
        data={
          speed:0,
          timeHeadway:0,
          spaceHeadway:0,
          volume:0
        }

      }
      SocketIo.emit('dialDataForVehicle',data)

    });
}



/**
 * Created by Suhas on 1/22/2016.
 */
var express = require('express'),
  router = express.Router(),
  mongoose = require('mongoose'),
  WholeZoneTrafficInfoModel = mongoose.model('wholeZoneTrafficInfo'),
  TerminalZonesAndLaneDetails = mongoose.model('terminalModel')
wholeZoneDetailsDetectedNotifier = require('../rtGraphNotifier/wholeZoneAnalytics/index');

module.exports = function (app) {
  app.use('/', router);
};

var seconds = 30;
var laneSelected=[2,3];
var zoneSelected = [1,2,3];
router.get('/wholeZoneAnalytics/selectedAverageSeconds/:seconds', function (req, res) {
  seconds =  req.params.seconds;
  res.send("selected Time in seconds "+seconds);
})
router.get('/wholeZoneAnalytics/selectedTerminals/:terminal', function (req, res) {
  var terminal =req.params.terminal;
  TerminalZonesAndLaneDetails.findOne({"terminalId":terminal},function(err,result){
    if(err){
      res.send(err)
    }else{
      laneSelected = result.lanesSelected;
      zoneSelected = result.zonesSelected;
      res.send("selected Terminals No "+terminal);
    }
  });
})
// Find a single random document of vehicleAnalytics
router.get('/wholeZone/averagedParameters', function (req, res) {
  var start = new Date();
  var end = new Date();
  start.setSeconds(start.getSeconds()-seconds);
  var lanesSelected = laneSelected;
  var zonesSelected = zoneSelected;
  WholeZoneTrafficInfoModel.aggregate(
    [
      /*matching through dateTime,laneIndex,zoneIndex*/
      {
        $match: {
          dateTime:{$gt:start,$lt:end}
          ,"vehicleAnalytics.cameraExtrinsics.cameraId":{$in:zonesSelected}
        }
      },
      {$unwind: "$vehicleAnalytics.wholeZoneTrafficInformation"},
      {
        $match: {
          "vehicleAnalytics.wholeZoneTrafficInformation.laneIndex":{$in:lanesSelected}
        }
      },
      {
        $group: {
          _id:null,
          levelOfService: {$max: "$vehicleAnalytics.wholeZoneTrafficInformation.levelofService"},
          occupancy: {$avg: "$vehicleAnalytics.wholeZoneTrafficInformation.zoneOccupancy"}
        }
      },
      {$sort: {_id:1}},
      {$project: { _id: 0,levelOfService:"$levelOfService",occupancy:"$occupancy"}}
    ],
    function (err, result) {
      if (err) {
        console.log(err);
        return;
      }
      var data;
      if(result.length>0){
        data = result[0]
      }else{
        data={
          occupancy:0,
          levelOfService:0
        }
      }
      res.send(data)
    });
})
router.get('/wholeZone/zoneLaneDetails/:terminalNo', function (req, res) {
  var terminal =req.params.terminal;
  TerminalZonesAndLaneDetails.findOne({"terminalId":terminal},function(err,result){
    if(err){
      res.send(err)
    }else{
      laneSelected = result.lanesSelected;
      zoneSelected = result.zonesSelected;
      res.send("Terminal Value Changed")
    }
  });
})
router.get('/wholeZone/averagedParameters/:seconds', function (req, res) {
  seconds=req.params.seconds;
});

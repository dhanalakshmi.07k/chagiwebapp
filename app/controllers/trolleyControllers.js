/**
 * Created by Suhas on 1/16/2016.
 */
var express = require('express'),
  https = require('https'),
  router = express.Router(),
  fs = require("fs")
trolleyDataAdapter = require("../starhubAdapter");
request = require("request");
mongoose = require('mongoose'),
  TrolleyLocation = mongoose.model('TrolleyLocation');
trolleyPolygonDetails=mongoose.model('polygonDetails');
var SocketIo = require('../../config/socketIo').getSocketIoServer();
var trolleysMacAddress = [];
initTrolleyData();
module.exports = function (app) {
  app.use('/', router);
};
var trolleyDetails = {
  "apiID": "csp_ichangi",
  "apiPwd": "a&&X^X;3<j2>Hbd",
  "action": "whereAmI",
  "macAddr": "00:23:A7:8D:1F:74"
}
var trolleyLocationDetails = {
  refreshTime: 0.5 * 60 * 1000,

}
var intervalId;
var trolleyNotificationIntervalId;
var thershold;
var minLat=10000,maxLat=11000,minLan=7500,maxLan=9000;
var previousMarker=0;
var previousThreshold=0;
smsSender =require('../smsNotifier'),
  smsPhoneNoForNotification = mongoose.model('smsPhoneNoNotificationModel');

router.get('/api/trolleyLocation/setRefreshTime:refreshTime', function (req, res) {
  clearInterval(intervalId);
  trolleyLocationDetails.refreshTime = req.params.refreshTime;
  startTimerForVehicleAnalyticsMetricsForTerminal();
  res.send("changed refresh time to " + trolleyLocationDetails.refreshTime)
})
router.get('/api/trolleyLocation/refreshTime', function (req, res) {
  res.send(trolleyLocationDetails)
})
startTimerForVehicleAnalyticsMetricsForTerminal();
function startTimerForVehicleAnalyticsMetricsForTerminal() {
  intervalId = setInterval(function () {
    getUpdatedTrolleyLocation();
  }, trolleyLocationDetails.refreshTime);
}
function getUpdatedTrolleyLocation() {
  TrolleyLocation.find({}, function (err, trollylocation) {
    if (err) {
      res.send(err)
    }
    SocketIo.emit("updatedTrolleyLocation", trollylocation);
  })
}
router.get('/api/trolleyLocation', function (req, res) {
  TrolleyLocation.find({}, function (err, trollylocation) {
    if (err) {
      res.send(err)
    }
    SocketIo.emit("updatedTrolleyLocation", trollylocation);
    res.send("trolley updated Location sent");
  })
})
function initTrolleyData() {
  var filePath = fs.realpathSync(__dirname + "/../data/")
  //console.log(`process directory: ${processDir}`);
  fs.readFile(filePath + "/trolleyMacAddress.json", function (err, data) {
    if (err) {
      console.log(err.stack)
      return;
    }

    var trolleyData = JSON.parse(data);
    trolleysMacAddress = trolleyData.trolleyMacAddressList
    //updateTrolleyData();
    startGettingTrolleyInfo()

  })
}
function startGettingTrolleyInfo() {
  setInterval(function () {
    updateTrolleyData();
  }, trolleyLocationDetails.refreshTime)
}
function updateTrolleyData() {
  //console.log(`trolleysMacAddress.length ${trolleysMacAddress.length}`)
  console.log("Updating the trolley Location .........")
  trolleysMacAddress.forEach(function (macAddress) {
    trolleyDataAdapter.getTrolleyLocation(macAddress, function (err, data) {
      if (!data || data.length < 2) {
        return;
      }
      TrolleyLocation.findOne({macAddress: macAddress}, function (err, trolleyLocation) {
        if (err) {
          console.error(err.stack)
        }
        if (trolleyLocation) {
          trolleyLocation.x = data[0];
          trolleyLocation.y = data[1];
          trolleyLocation.refreshDateTime = Date.now();
        } else {
          trolleyLocation = new TrolleyLocation();
          trolleyLocation.x = data[1];
          trolleyLocation.y = data[1];
          trolleyLocation.macAddress = macAddress
        }
        trolleyLocation.save();

      })

    })
  })
}
router.get('/api/trolley/polygon', function (req, res, next) {
  trolleyPolygonDetails.find(function(err,response){
    if(err){
      res.send(err);
    }else{
      res.send(response);
    }

  })
});
function startTrolleySimulator(){
  trolleyNotificationIntervalId = setInterval(function () {
    getPolygonPlots();
  }, 5000);
}
function getTrolleys(polygondetails){

  TrolleyLocation.find({} , function (err, trollylocation) {
    if (err) {
      res.send(err)
    }
    if(trollylocation && polygondetails){
      /*var minLat=10000,maxLat=11000,minLan=7500,maxLan=9000;*/
      thershold=polygondetails.threshold;
      var markerCounter=0;
      var minLat  = Math.min(polygondetails.polygonobj.corordinat1Lat, polygondetails.polygonobj.corordinat2Lat);
      var maxLat  = Math.max(polygondetails.polygonobj.corordinat1Lat, polygondetails.polygonobj.corordinat2Lat);
      var minLan  = Math.min(polygondetails.polygonobj.corordinat1Lan, polygondetails.polygonobj.corordinat2Lan);
      var maxLan  = Math.max(polygondetails.polygonobj.corordinat1Lan, polygondetails.polygonobj.corordinat2Lan);
      for(var i=0;i<trollylocation.length;i++){
        var markerLat = trollylocation[i].x, markerlan = 16000-trollylocation[i].y;
        if ((markerLat >= minLat && markerLat <= maxLat) &&
          (markerlan >= minLan && markerlan <= maxLan)) {
          markerCounter++;
        }
        if(i==trollylocation.length-1 && markerCounter<thershold){
            previousThreshold=thershold;
            previousMarker =markerCounter;
            var payload={
              notificationType:"trolley",
              message:"trolley thershold reached ",
              thershold:polygondetails.threshold,
              trolleyCount:markerCounter,
              regionId:1,
              messageTime:new Date()
            }
            SocketIo.emit("trolleyNotification", payload);
        }
      }

    }

  })
}
function getPolygonPlots(){
  trolleyPolygonDetails.findOne({"id":0},function(err,result){
    if(err){
      res.send(err);
    }else if(result){
      getTrolleys(result);
    }
  })

}
startTrolleySimulator();


/*getter*/
router.get('/api/TrolleyRegion', function (req, res) {
  trolleyPolygonDetails.findOne({"id":0},function(err,result){
    if(err){
      res.send(err);
    }else if(result){
      var data={
        topLeft:{
          x:result.polygonobj.corordinat1Lat,
          y:result.polygonobj.corordinat1Lan
        },
        bottomRight:{
          x:result.polygonobj.corordinat2Lat,
          y:result.polygonobj.corordinat2Lan
        }
      }
      res.send(data)
    }
  })

})
router.get('/api/TrolleyThreshold', function (req, res) {

  trolleyPolygonDetails.findOne({"id":0},function(err,result){
    if(err){
      res.send(err);
    }else if(result){
      var data = {
        threshold:result.threshold
      }
      res.send(data)
    }
  })

})
/*setter*/
router.post('/api/setTrolleyRegion', function (req, res) {
  var data  =req.body;
  minLat=data.top.x;
  maxLan=data.top.y;
  maxLat=data.bottom.x;
  minLan=data.bottom.y;
  trolleyPolygonDetails.findOne({"id":0},function(err,result){
    if(err){
      res.send(err);
    }else if(result){
      result.polygonobj.corordinat1Lat=data.top.x;
      result.polygonobj.corordinat1Lan=data.top.y;
      result.polygonobj.corordinat2Lat=data.bottom.x;
      result.polygonobj.corordinat2Lan=data.bottom.y;
      result.save(function(err1,result11){
        if(err1){
          res.send(err)
        }
        res.send(result11)
      })
    }
  })

})
router.get('/api/setTrolleyThreshold/:threshold', function (req, res) {
  thershold = req.params.threshold;
  trolleyPolygonDetails.findOne({"id":0},function(err,result){
    if(err){
      res.send(err);
    }else if(result){
      result.threshold = req.params.threshold;
      result.save(function(err,trolleyObj){
        if(err){
          res.send(err)
        }
        res.send(trolleyObj)
      })
    }
  })
})


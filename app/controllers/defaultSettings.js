/**
 * Created by Suhas on 4/29/2016.
 */
var express = require('express'),
    https = require('https'),
    router = express.Router(),
    config = require('../../config/config')
    settingConfigurationModel = require("../models/settingsConfigurationModel")
    notificationValidationModule=require('../vehicleNotificationValidation');

var bCrypt = require('bcrypt-nodejs');
var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var expressJwt = require('express-jwt'); //https://npmjs.org/package/express-jwt
var secret = 'this is the secrete password';

module.exports = function (app) {
  app.use('/', router);
};

router.get('/api/getEnvType', function (req, res) {
  console.log(config.type)
  res.send(config.type)
})

router.get('/api/getSettingsDetails', function (req, res) {
  settingConfigurationModel.findOne({'id':1},function(err,result){
        if(result){
            res.send(result)
        }
  });
})
function getSettingDetails(){
  settingConfigurationModel.findOne({'id':1},function(err,result){
    if(result){
      console.log()
      var notificationValidationPeriod = result.notificationSettings.notificationFilterPeriod;
      getValidationPeriodForNotification(notificationValidationPeriod)
    }
  });
}
function getValidationPeriodForNotification(validationTimeInSec){
  notificationValidationModule.setNotificationValidationPeriod(validationTimeInSec)
}
getSettingDetails();

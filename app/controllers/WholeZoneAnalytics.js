/**
 * Created by zendynamix on 09-01-2016.
 */
var express = require('express'),
  router = express.Router(),
  mongoose = require('mongoose'),
  WholeZoneTrafficInfoModel = mongoose.model('wholeZoneTrafficInfo'),
  TerminalZonesAndLaneDetails = mongoose.model('terminalModel')
  wholeZoneDetailsDetectedNotifier = require('../rtGraphNotifier/wholeZoneAnalytics/index'),
  notifyClientsRTDetails = require("../rtGraphNotifier/multipleSocketRTDataPusher");
var random = require('mongoose-simple-random');
var request = require("request");
var SocketIo = require('../../config/socketIo').getSocketIoServer();
var historicalDataAggregationModule = require("../historicalData");
var config = require('../../config/config');

Array.prototype.divide = function (arr) {
  var sum = [];
  if (arr != null && this.length == arr.length) {
    for (var i = 0; i < arr.length; i++) {
      if(this[i]==0){
        sum.push(0);
      }else{
        var no = this[i]/arr[i]
        var no1 = parseInt(no.toFixed(2))
        sum.push(no1);
      }

    }
  }
  return sum;
}
Array.prototype.AvgArray = function (length) {
  var avgArray = [];
  if (this.length != null) {
    for (var i = 0; i < this.length; i++) {
      this[i] = this[i] / length;
      avgArray.push(this[i]);
    }
  }
  return avgArray;
}
Array.prototype.addArrayElements = function () {
  var sum = 0;
  if (this.length != null) {
    for (var i = 0; i < this.length; i++) {
      sum = this[i]+sum;
    }
  }
  return sum;
}
Array.prototype.AvgOfMultipleArray = function (arrayContainer) {
  var avgArray = [];
  var noOfArrays = arrayContainer.length;
  if (this.length != null) {
    for (var i = 0; i < this.length; i++) {
      for(var j=0;j<noOfArrays;j++){
        this[i]=this[i]+arrayContainer[j][i];
        if(j==noOfArrays-1){
          this[i] = this[i]/(noOfArrays+1);
          avgArray.push(this[i])
        }
        if(i==this.length-1 && j==noOfArrays-1){
          return avgArray
        }
      }
      /*this[i] = this[i] / noOfArrays;
       avgArray.push(this[i]);*/
    }
  }
  return avgArray;
}
Array.prototype.contains = function(obj) {
  var i = this.length;
  while (i--) {
    if (this[i]==obj) {
      return true;
    }
  }
  return false;
}
module.exports = function (app) {
  app.use('/', router);
};
//router.use('/api', expressJwt({secret: secret}));
router.use(function(err, req, res, next){
  if (err.constructor.name === 'UnauthorizedError') {
    res.status(401).send('Unauthorized Token Is not present');
  }
});
router.get('/', function (req, res, next) {
  Article.find(function (err, articles) {
    if (err) return next(err);
    res.render('index', {
      title: 'Generator-Express MVC',
      articles: articles
    });
  });
});

/* function to save datat to db*/
function saveData(message) {
  var vehicleAnalyticsObj = VehicleAnalyticsModel();
  vehicleAnalyticsObj.vehicleAnalytics = message.vehicleAnalytics;
  vehicleAnalyticsObj.dateTime = new Date();
  vehicleAnalyticsObj.save(function(err,result){
    if(err){
      console.log(err);
    }
    /*res.send(result)*/
    /* console.log(result);*/
  })
}
/*To get random WholeZoneAnalytics data*/
router.get('/api/randomWholeZoneAnalytics', function (req, res) {
  WholeZoneTrafficInfoModel.findOneRandom(function(err,result){
    if(result){
      result.dateTime=new Date();
      notifyWholeZoneDetailsDetected(result)
      saveWholeAnalyticsData(result)
      res.send("okay");
    }else{
      res.send("No Data In Database")
    }

  })
})

/*To get RealTime  WholeZoneAnalytics data that are stored previously by given seconds to current date*/
/*router.get('/api/wholeZoneAnalyticsData/:seconds', function (req, res) {
  var endSeconds = req.params.seconds;
  var start = new Date();
  var startTimeSeconds = start.getSeconds();
  var end = new Date();
  end.setSeconds(startTimeSeconds-endSeconds);
  WholeZoneTrafficInfoModel.find({dateTime:{$gt:end,$lt:start}},function(err,result){
    var io = req.io;
    if(result.length>0){
      if(err){
        res.send(err);
      }
      else{
        /!*var wholeAnalyticsData = {
         /!*data:[zone1:[lane1,lane2,lane3,lane4,lane5]]*!/
         speed:{
         "zone1":{
         "lane1":0,
         "lane2":0,
         "lane3":0,
         "lane4":0,
         "lane5":0
         },
         "zone2":{
         "lane1":0,
         "lane2":0,
         "lane3":0,
         "lane4":0,
         "lane5":0
         },
         "zone3":{
         "lane1":0,
         "lane2":0,
         "lane3":0,
         "lane4":0,
         "lane5":0
         }
         },
         occupancy:{
         "zone1":{
         "lane1":0,
         "lane2":0,
         "lane3":0,
         "lane4":0,
         "lane5":0
         },
         "zone2":{
         "lane1":0,
         "lane2":0,
         "lane3":0,
         "lane4":0,
         "lane5":0
         },
         "zone3":{
         "lane1":0,
         "lane2":0,
         "lane3":0,
         "lane4":0,
         "lane5":0
         }
         },
         volume:{
         "zone1":{
         "lane1":0,
         "lane2":0,
         "lane3":0,
         "lane4":0,
         "lane5":0
         },
         "zone2":{
         "lane1":0,
         "lane2":0,
         "lane3":0,
         "lane4":0,
         "lane5":0
         },
         "zone3":{
         "lane1":0,
         "lane2":0,
         "lane3":0,
         "lane4":0,
         "lane5":0
         }
         },
         flow:{
         "zone1":{
         "lane1":0,
         "lane2":0,
         "lane3":0,
         "lane4":0,
         "lane5":0
         },
         "zone2":{
         "lane1":0,
         "lane2":0,
         "lane3":0,
         "lane4":0,
         "lane5":0
         },
         "zone3":{
         "lane1":0,
         "lane2":0,
         "lane3":0,
         "lane4":0,
         "lane5":0
         }
         },
         }*!/
        /!*var count1 ={
         zone1:{
         lane1:0,
         lane2:0,
         lane3:0,
         lane4:0,
         lane5:0
         },
         zone2:{
         lane1:0,
         lane2:0,
         lane3:0,
         lane4:0,
         lane5:0
         },
         zone3:{
         lane1:0,
         lane2:0,
         lane3:0,
         lane4:0,
         lane5:0
         }
         }*!/
        var wholeAnalyticsData = {
          /!*data:[zone1:[lane1,lane2,lane3,lane4,lane5]]*!/
          speed:[[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0]],
          occupancy:[[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0]],
          volume:[[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0]],
          flow:[[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0]],
        }
        var count1 ={
          zone1:[0,0,0,0,0],
          zone2:[0,0,0,0,0],
          zone3:[0,0,0,0,0]
        }
        /!* var wholeZoneAnalyticsData = {
         speed:0,
         occupancy:0,
         volume:0,
         flow:0
         }
         var count = 0;*!/
        var wholeZoneAnalysisArray = result;
        for(var i=0;i<wholeZoneAnalysisArray.length;i++){
          var zone = wholeZoneAnalysisArray[i].vehicleAnalytics.cameraExtrinsics.cameraId;
          var zoneFilter = zone-1;
          var wholeAnalyticsZoneSpeedDetails = wholeAnalyticsData.speed[zone-1];
          var wholeAnalyticsZoneOccupancyDetails = wholeAnalyticsData.occupancy[zone-1];
          var wholeAnalyticsZoneVolumeDetails = wholeAnalyticsData.volume[zone-1];
          var wholeAnalyticsZoneFlowDetails = wholeAnalyticsData.flow[zone-1];
          var wholeAnalyticsZoneCount = count1["zone"+zone];
          var trafficInformationArray = wholeZoneAnalysisArray[i].vehicleAnalytics.wholeZoneTrafficInformation;
          for(var j=0;j<trafficInformationArray.length;j++){
            var lane = trafficInformationArray[j].laneIndex;
            /!*var arrayIndex = "lane"+lane;*!/
            var arrayIndex = lane-1;
            if(wholeAnalyticsZoneSpeedDetails[arrayIndex]){
              wholeAnalyticsZoneSpeedDetails[arrayIndex] = wholeAnalyticsZoneSpeedDetails[arrayIndex] + trafficInformationArray[j].zoneSpeed;
              wholeAnalyticsZoneOccupancyDetails[arrayIndex] = wholeAnalyticsZoneOccupancyDetails[arrayIndex] + trafficInformationArray[j].zoneOccupancy;
              wholeAnalyticsZoneCount[arrayIndex] = wholeAnalyticsZoneCount[arrayIndex]+1;
            }else {
              wholeAnalyticsZoneSpeedDetails[arrayIndex] = trafficInformationArray[j].zoneSpeed;
              wholeAnalyticsZoneOccupancyDetails[arrayIndex] = trafficInformationArray[j].zoneOccupancy;
              wholeAnalyticsZoneCount[arrayIndex]=1;
            }
            if(i==wholeZoneAnalysisArray.length-1 && j==trafficInformationArray.length-1){
              var ZoneAverageData ={};
              var laneAverageData = {};
              var laneAveragedata;
              var zoneWiseData={};
              var individualZoneAndLaneData={
                speed:[[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0]],
                occupancy:[[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0]],
              }
              for(var zoneIndex=1;zoneIndex<4;zoneIndex++){
                var zone = "zone"+zoneIndex;
                var zoneCount = count1[zone];
                ZoneAverageData[zone]= {};
                ZoneAverageData[zone]["speed"]=wholeAnalyticsData["speed"][zoneIndex-1].divide(zoneCount);
                ZoneAverageData[zone]["occupancy"]=wholeAnalyticsData["occupancy"][zoneIndex-1].divide(zoneCount);
                individualZoneAndLaneData["speed"][zoneIndex-1] = ZoneAverageData[zone]["speed"];
                individualZoneAndLaneData["occupancy"][zoneIndex-1] = ZoneAverageData[zone]["occupancy"];
                if(zoneIndex==3){
                  var arrayOfSpeedForZone1And2 = [ZoneAverageData["zone2"]["speed"],ZoneAverageData["zone3"]["speed"]]
                  var wholeZoneAverageSpeedArray = ZoneAverageData["zone1"]["speed"].AvgOfMultipleArray(arrayOfSpeedForZone1And2);
                  var arrayOfOccupancyForZone1And2 = [ZoneAverageData["zone2"]["occupancy"],ZoneAverageData["zone3"]["occupancy"]]
                  var wholeZoneAverageOccupancyArray = ZoneAverageData["zone1"]["occupancy"].AvgOfMultipleArray(arrayOfOccupancyForZone1And2);
                  var wholeZoneAverageSpeed=wholeZoneAverageSpeedArray.addArrayElements()/5;
                  var wholeZoneAverageOccupancy=wholeZoneAverageOccupancyArray.addArrayElements()/5;
                  var zoneWiseDataIndex;
                  for(var l=1;l<Object.keys(ZoneAverageData).length+1;l++){
                    zoneWiseDataIndex = "zone"+l;
                    zoneWiseData[zoneWiseDataIndex]={};
                    zoneWiseData[zoneWiseDataIndex]["speed"]=ZoneAverageData[zoneWiseDataIndex]["speed"].addArrayElements()/5;
                    zoneWiseData[zoneWiseDataIndex]["occupancy"]=ZoneAverageData[zoneWiseDataIndex]["occupancy"].addArrayElements()/5;
                    if(l==Object.keys(ZoneAverageData).length){
                      var analyticsObj = {
                        zoneWiseData:zoneWiseData,
                        laneWiseData:{speed:wholeZoneAverageSpeedArray,occupancy:wholeZoneAverageOccupancyArray},
                        wholeZoneData:{speed:wholeZoneAverageSpeed,occupancy:wholeZoneAverageOccupancy},
                        individualZoneAndLaneData:individualZoneAndLaneData
                      }
                      var payLoad = {
                        data:analyticsObj,
                        timeStamp:new Date()
                      }
                      io.emit('solidGuageAnalyticsData',{
                        data:payLoad
                      })
                      res.send(analyticsObj);
                    }
                  }

                  if(wholeZoneAverageSpeedArray && wholeZoneAverageOccupancyArray){

                  }
                }
              }
            }
          }
        }
      }
    }
    else{
      res.send("no data")
    }
  })
})*/

/*Method to save WholeZoneAnalytics data*/
function saveWholeAnalyticsData(data) {
  var WholeZoneTrafficInfoModelObj = WholeZoneTrafficInfoModel();
  WholeZoneTrafficInfoModelObj.vehicleAnalytics = data.vehicleAnalytics;
  WholeZoneTrafficInfoModelObj.dateTime = new Date();
  WholeZoneTrafficInfoModelObj.save(function(err,result){
    if(err){
      console.log(err);
    }
    console.log("data Saved")
    /* console.log(result);*/
  })
}

/*To get WholeZoneAnalytics data in terms of lanes and zones*/
router.post('/api/wholeZoneTrafficInfoByDateForLanes', function (req, res) {
  var start = new Date(req.body.start);
  var end = new Date(req.body.end);
  var today = new Date()
  start.setHours(0);
  if(start.getMonth()== today.getMonth() && start.getFullYear()== today.getFullYear() && start.getDate()== today.getDate()){
    end = today;
  }else{
    end.setHours(23);
  }
  WholeZoneTrafficInfoModel.find({dateTime:{$gt:start,$lt:end}},function(err,result){
    if(err){
      res.send(err);
    }
    else{
      var wholeAnalyticsData = {
        occupancy:[
          [[],[],[],[],[]],
          [[],[],[],[],[]],
          [[],[],[],[],[]]
        ]
      }
      var count1=[
        [[],[],[],[],[]],
        [[],[],[],[],[]],
        [[],[],[],[],[]],
      ]
      if (result.length>0) {
        var wholeZoneAnalyticsArray = result;
        for (var i = 0;i<wholeZoneAnalyticsArray.length;i++) {
          var zone = wholeZoneAnalyticsArray[i].vehicleAnalytics.cameraExtrinsics.cameraId;
          var wholeZoneDetectedArray = wholeZoneAnalyticsArray[i].vehicleAnalytics.wholeZoneTrafficInformation;
          var hour = new Date(wholeZoneAnalyticsArray[i].dateTime).getHours();
          for (var j = 0;j<wholeZoneDetectedArray.length;j++) {
            var laneIndex = wholeZoneDetectedArray[j].laneIndex;
            if(wholeAnalyticsData.speed[zone-1][laneIndex-1][hour]){
              wholeAnalyticsData.speed[zone-1][laneIndex-1][hour] = wholeAnalyticsData.speed[zone-1][laneIndex-1][hour]+wholeZoneDetectedArray[j].zoneSpeed;
            }else{
              wholeAnalyticsData.speed[zone-1][laneIndex-1][hour] = wholeZoneDetectedArray[j].zoneSpeed;
            }
            if(wholeAnalyticsData.occupancy[zone-1][laneIndex-1][hour]){
              wholeAnalyticsData.occupancy[zone-1][laneIndex-1][hour] = wholeAnalyticsData.occupancy[zone-1][laneIndex-1][hour]+wholeZoneDetectedArray[j].zoneOccupancy;
            }else{
              wholeAnalyticsData.occupancy[zone-1][laneIndex-1][hour] = wholeZoneDetectedArray[j].zoneOccupancy;
            }
            if(count1[zone-1][laneIndex-1][hour]){
              count1[zone-1][laneIndex-1][hour] = count1[zone-1][laneIndex-1][hour]+1;
            }else{
              count1[zone-1][laneIndex-1][hour]= 1;
            }
            if(i==wholeZoneAnalyticsArray.length-1 && j==wholeZoneDetectedArray.length-1){
              var resultData = {
                speed:[],
                occupancy:[],
                flow:[]
              };
              for(var zoneValue=0;zoneValue<3;zoneValue++){
                resultData["speed"][zoneValue]=[];
                resultData["occupancy"][zoneValue]=[];
                resultData["flow"][zoneValue]=[];
                for(var laneValue=0;laneValue<5;laneValue++){
                  resultData["speed"][zoneValue][laneValue]=wholeAnalyticsData.speed[zoneValue][laneValue].divide(count1[zoneValue][laneValue]);
                  resultData["occupancy"][zoneValue][laneValue]=wholeAnalyticsData.occupancy[zoneValue][laneValue].divide(count1[zoneValue][laneValue]);
                  resultData["flow"][zoneValue][laneValue]=count1[zoneValue][laneValue];
                  if(laneValue==4 && zoneValue==2){
                    res.send(resultData)/**/
                  }

                }
              }

            }
          }
        }
      } else {
        res.send("no data")
      }
    }
  })
})

/*to get  Historical Whole Zone Analytics by Date ,Lanes selected and type Of data(speed and Occupancy)*/
router.post('/api/wholeZoneAnalyticsByDateTypeLanesSelected', function (req, res) {
  var start = new Date(req.body.startDate);
  var end = new Date(req.body.endDate);
  start.setHours(0);
  var type = req.body.type;
  var laneSelected = req.body.lanesSelected;
  var today = new Date();
  if (start.getMonth() == today.getMonth() && start.getFullYear() == today.getFullYear() && start.getDate() == today.getDate()) {
    end = today;
  }
  else {
    end.setHours(23);
  }
  WholeZoneTrafficInfoModel.find({dateTime: {$gt: start, $lt: end}}, function (err, result) {
    if (err) {
      res.send(err);
    }
    else {
      var wholeZoneAnalytics = {
        zone1:[],
        zone2:[],
        zone3:[]
      }
      var count1 ={
        zone1:[],
        zone2:[],
        zone3:[]
      }
      if (result.length > 0) {
        var analyticsForDateRange = result;
        for (var i = 0; i < analyticsForDateRange.length; i++) {
          var zone = analyticsForDateRange[i].vehicleAnalytics.cameraExtrinsics.cameraId;
          var wholeZoneTrafficArray = analyticsForDateRange[i].vehicleAnalytics.wholeZoneTrafficInformation;
          var hour = new Date(analyticsForDateRange[i].dateTime).getHours();
          for (var j = 0; j < wholeZoneTrafficArray.length; j++) {
            var laneIndex = wholeZoneTrafficArray[j].laneIndex;
            var zoneIndex = "zone"+zone;
            if (laneSelected.contains(laneIndex)) {
              if (wholeZoneAnalytics[zoneIndex][hour]) {
                wholeZoneAnalytics[zoneIndex][hour] = wholeZoneAnalytics[zoneIndex][hour] + wholeZoneTrafficArray[j][type];
              }
              else {
                wholeZoneAnalytics[zoneIndex][hour] = wholeZoneTrafficArray[j][type];
              }
              if (count1[zoneIndex][hour]) {
                count1[zoneIndex][hour] = count1[zoneIndex][hour] + 1;
              }
              else {
                count1[zoneIndex][hour] = 1;
              }
            }
            if (j == wholeZoneTrafficArray.length - 1 && i == analyticsForDateRange.length - 1) {
              var data =[];
              for(var zoneIndexForLanes=1;zoneIndexForLanes<4;zoneIndexForLanes++){
                var zoneString ="zone"+zoneIndexForLanes;
                data[zoneIndexForLanes-1]=wholeZoneAnalytics[zoneString].divide(count1[zoneString]);
                if(zoneIndexForLanes==3){
                  res.send(data);
                }
              }
            }
          }
        }
      }
      else {
        res.send("NO Data");
      }
    }
  })
})


/*For Real-timeRoad Whole Zone Analytics by providing Occupancy for selected Zones*/
router.post('/api/realTime/wholeZoneAnalyticsData/type/zoneId', function (req, res) {
  var endSeconds = req.body.seconds;
  var start = new Date();
  var startTimeSeconds = start.getSeconds();
  var end = new Date();
  end.setSeconds(startTimeSeconds-endSeconds);
  var type;
  if(req.body.type=="occupancy"){
     type="zoneOccupancy";
  }else if(req.body.type=="Level Of Service"){
    type="levelOfService";
  }else if(req.body.type=="Speed"){
    type="zoneSpeed";
  }
  var zoneSelected=req.body.zoneSelected/*req.body.zoneSelected*/;
  WholeZoneTrafficInfoModel.find({dateTime:{$gt : end, $lt : start}},function(err,result){
    if(err){
      res.send(err)
    }else if(result.length>0){
        var wholeZoneAnalyticsData=[0,0,0,0,0];
        var countOfWholeZoneObjectDetected=[0,0,0,0,0];
        var wholeZoneAnalyticsForGivenDateRange = result;
        for (var i = 0; i < wholeZoneAnalyticsForGivenDateRange.length; i++) {
          var wholeZoneArrayObject = wholeZoneAnalyticsForGivenDateRange[i].vehicleAnalytics.wholeZoneTrafficInformation;
          var zone = wholeZoneAnalyticsForGivenDateRange[i].vehicleAnalytics.cameraExtrinsics.cameraId;
          for (var j = 0; j <wholeZoneArrayObject.length; j++) {
            var laneIndex = wholeZoneArrayObject[j].laneIndex-1;
            if(zoneSelected===zone){
                if(wholeZoneAnalyticsData[laneIndex]){
                  wholeZoneAnalyticsData[laneIndex] = wholeZoneAnalyticsData[laneIndex]+wholeZoneArrayObject[j][type];
                }else{
                  wholeZoneAnalyticsData[laneIndex] = wholeZoneArrayObject[j][type];
                }
              if(countOfWholeZoneObjectDetected[laneIndex]){
                countOfWholeZoneObjectDetected[laneIndex] = countOfWholeZoneObjectDetected[laneIndex]+1;
              }
              else {
                countOfWholeZoneObjectDetected[laneIndex] = 1;
              }
            }
            if (i == wholeZoneAnalyticsForGivenDateRange.length-1 && j == wholeZoneArrayObject.length-1) {
              var data = [];
                for(var laneArrayIndex=0;laneArrayIndex<5;laneArrayIndex++){
                  data[laneArrayIndex]=wholeZoneAnalyticsData[laneArrayIndex]/countOfWholeZoneObjectDetected[laneArrayIndex];
                  if(laneArrayIndex===4){
                    occupancyDataForRealTimeLaneGraph(data);
                    res.send(data);
                  }
                }

            }
          }
        }
    }else{
      var nullData=[0,0,0,0,0]
      occupancyDataForRealTimeLaneGraph(nullData);
      res.send("no data")
    }
  })
})

/*For Data of Whole Zone Analytics for dials by providing selected Lanes(particular lane)*/
router.post('/api/realTime/wholeZoneAnalyticsData/laneIndex/type', function (req, res) {
  var endSeconds = req.body.seconds;
  var start = new Date();
  var startTimeSeconds = start.getSeconds();
  var end = new Date();
  end.setSeconds(startTimeSeconds-endSeconds);
  var type ="zoneOccupancy";
  /*if(req.body.type=="occupancy"){
    type="zoneOccupancy";
  }else if(req.body.type=="Level Of Service"){
    type="levelOfService";
  }else if(req.body.type=="Speed"){
    type="zoneSpeed";
  }*/
  var laneSelected=req.body.laneSelected/*req.body.zoneSelected*/;
  WholeZoneTrafficInfoModel.find({dateTime:{$gt : end, $lt : start}},function(err,result){
    if(err){
      res.send(err)
    }else if(result.length>0){
      var wholeZoneAnalyticsData=0;
      var countOfWholeZoneObjectDetected=0;
      var wholeZoneAnalyticsForGivenDateRange = result;
      for (var i = 0; i < wholeZoneAnalyticsForGivenDateRange.length; i++) {
        var wholeZoneArrayObject = wholeZoneAnalyticsForGivenDateRange[i].vehicleAnalytics.wholeZoneTrafficInformation;
        var zone = wholeZoneAnalyticsForGivenDateRange[i].vehicleAnalytics.cameraExtrinsics.cameraId;
        for (var j = 0; j <wholeZoneArrayObject.length; j++) {
          var laneIndex = wholeZoneArrayObject[j].laneIndex;
          if(laneSelected==laneIndex){
            wholeZoneAnalyticsData= wholeZoneAnalyticsData+wholeZoneArrayObject[j][type];
            countOfWholeZoneObjectDetected = countOfWholeZoneObjectDetected+1;
          }
          if (i == wholeZoneAnalyticsForGivenDateRange.length-1 && j == wholeZoneArrayObject.length-1) {
            var data = {};
            for(var laneArrayIndex=0;laneArrayIndex<5;laneArrayIndex++){
              data[type]=Math.round(wholeZoneAnalyticsData/countOfWholeZoneObjectDetected);
              if(laneArrayIndex===4){
                wholeZoneAnalyticsDataForDials(data);
                res.send(data);
              }
            }

          }
        }
      }
    }else{
      var nullData={
        zoneOccupancy:0
      };
      wholeZoneAnalyticsDataForDials(nullData);
      res.send("no data")
    }
  })
})

/*To get Data of Whole Zone Analytics for dials For all lanes and all zones(All zone and Lane)*/
router.get('/api/realTime/wholeZoneAnalyticsDataForAllLanesAndZones/:seconds', function (req, res) {
  var endSeconds = req.params.seconds;
  var start = new Date();
  var startTimeSeconds = start.getSeconds();
  var end = new Date();
  end.setSeconds(startTimeSeconds-endSeconds);
  var type ="zoneOccupancy";
  /*if(req.body.type=="occupancy"){
   type="zoneOccupancy";
   }else if(req.body.type=="Level Of Service"){
   type="levelOfService";
   }else if(req.body.type=="Speed"){
   type="zoneSpeed";
   }*/
  WholeZoneTrafficInfoModel.find({dateTime:{$gt : end, $lt : start}},function(err,result){
    if(err){
      res.send(err)
    }else if(result.length>0){
      var wholeZoneAnalyticsData=0;
      var countOfWholeZoneObjectDetected=0;
      var wholeZoneAnalyticsForGivenDateRange = result;
      for (var i = 0; i < wholeZoneAnalyticsForGivenDateRange.length; i++) {
        var wholeZoneArrayObject = wholeZoneAnalyticsForGivenDateRange[i].vehicleAnalytics.wholeZoneTrafficInformation;
        var zone = wholeZoneAnalyticsForGivenDateRange[i].vehicleAnalytics.cameraExtrinsics.cameraId;
        for (var j = 0; j <wholeZoneArrayObject.length; j++) {
          var laneIndex = wholeZoneArrayObject[j].laneIndex;
          wholeZoneAnalyticsData= wholeZoneAnalyticsData+wholeZoneArrayObject[j][type];
          countOfWholeZoneObjectDetected = countOfWholeZoneObjectDetected+1;
          if (i == wholeZoneAnalyticsForGivenDateRange.length-1 && j == wholeZoneArrayObject.length-1) {
            var data = {};
            for(var laneArrayIndex=0;laneArrayIndex<5;laneArrayIndex++){
              data[type]=Math.round(wholeZoneAnalyticsData/countOfWholeZoneObjectDetected);
              if(laneArrayIndex===4){
                wholeZoneAnalyticsDataForDials(data);
                res.send(data);
              }
            }

          }
        }
      }
    }else{
      var nullData={
        zoneOccupancy:0
      };
      wholeZoneAnalyticsDataForDials(nullData);
      res.send("no data")
    }
  })
})

/*To get Data of Whole Zone Analytics for dials For a lanes Of particular zone(Selected point)*/
router.post('/api/realTime/wholeZoneAnalyticsData/zoneIndex/laneIndex', function (req, res) {
  var endSeconds = req.body.seconds;
  var start = new Date();
  var startTimeSeconds = start.getSeconds();
  var end = new Date();
  end.setSeconds(startTimeSeconds-endSeconds);
  var type ="zoneOccupancy";
  var laneSelected=req.body.laneSelected;
  var zoneSelected=req.body.zoneSelected;
  WholeZoneTrafficInfoModel.find({dateTime:{$gt : end, $lt : start}},function(err,result){
    if(err){
      res.send(err)
    }else if(result.length>0){
      var wholeZoneAnalyticsData=0;
      var countOfWholeZoneObjectDetected=0;
      var wholeZoneAnalyticsForGivenDateRange = result;
      for (var i = 0; i < wholeZoneAnalyticsForGivenDateRange.length; i++) {
        var wholeZoneArrayObject = wholeZoneAnalyticsForGivenDateRange[i].vehicleAnalytics.wholeZoneTrafficInformation;
        var zone = wholeZoneAnalyticsForGivenDateRange[i].vehicleAnalytics.cameraExtrinsics.cameraId;
        for (var j = 0; j <wholeZoneArrayObject.length; j++) {
          var laneIndex = wholeZoneArrayObject[j].laneIndex;
          if(laneIndex==laneSelected && zone==zoneSelected){
            wholeZoneAnalyticsData= wholeZoneAnalyticsData+wholeZoneArrayObject[j][type];
            countOfWholeZoneObjectDetected = countOfWholeZoneObjectDetected+1;
          }
          if (i == wholeZoneAnalyticsForGivenDateRange.length-1 && j == wholeZoneArrayObject.length-1) {
            var data = {};
            for(var laneArrayIndex=0;laneArrayIndex<5;laneArrayIndex++){
              data[type]=Math.round(wholeZoneAnalyticsData/countOfWholeZoneObjectDetected);
              if(laneArrayIndex===4){
                wholeZoneAnalyticsDataForDials(data);
                res.send(data);
              }
            }

          }
        }
      }
    }else{
      var nullData={
        zoneOccupancy:0
      };
      wholeZoneAnalyticsDataForDials(nullData);
      res.send("no data")
    }
  })
})


/*To get Level of service Data of Whole Zone Analytics for a given last seconds*/
router.get('/api/realTime/wholeZoneAnalyticsLevelOfService/:seconds', function (req, res) {
  var endSeconds = req.params.seconds;
  var start = new Date();
  var startTimeSeconds = start.getSeconds();
  var end = new Date();
  end.setSeconds(startTimeSeconds-endSeconds);
  var type ="levelofService";
  WholeZoneTrafficInfoModel.find({dateTime:{$gt : end, $lt : start}},function(err,result){
    if(err){
      res.send(err)
    }else if(result.length>0){
      var wholeZoneAnalyticsData=[[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0]];
      var countOfWholeZoneObjectDetected=[[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0]];
      var wholeZoneAnalyticsForGivenDateRange = result;
      for (var i = 0; i < wholeZoneAnalyticsForGivenDateRange.length; i++) {
        var wholeZoneArrayObject = wholeZoneAnalyticsForGivenDateRange[i].vehicleAnalytics.wholeZoneTrafficInformation;
        var zone = wholeZoneAnalyticsForGivenDateRange[i].vehicleAnalytics.cameraExtrinsics.cameraId-1;
        for (var j = 0; j <wholeZoneArrayObject.length; j++) {
          var laneIndex = wholeZoneArrayObject[j].laneIndex-1;
          wholeZoneAnalyticsData[zone][laneIndex]= wholeZoneAnalyticsData[zone][laneIndex]+wholeZoneArrayObject[j][type];
          countOfWholeZoneObjectDetected[zone][laneIndex] = countOfWholeZoneObjectDetected[zone][laneIndex]+1;
          if (i == wholeZoneAnalyticsForGivenDateRange.length-1 && j == wholeZoneArrayObject.length-1) {
            for(var laneArrayIndex=0;laneArrayIndex<5;laneArrayIndex++){
              var data=[];
              if(laneArrayIndex===4){
                for(var z=0;z<3;z++){
                    data[z]=wholeZoneAnalyticsData[z].divide(countOfWholeZoneObjectDetected[z]);
                    if(z==2){
                      wholeZoneAnalyticsLOSDataForMetrics(data);
                      res.send("LOS data sent for metrics  avg for "+endSeconds);
                    }
                }
              }
            }

          }
        }
      }
    }else{
      var nullData=[[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0]]
      wholeZoneAnalyticsLOSDataForMetrics(nullData);
      res.send("no data")
    }
  })
})

function occupancyDataForRealTimeLaneGraph(data){
  var payLoad = {
    wholeZoneAnalyticsData:data,
    timeStamp:new Date()
  }
  SocketIo.emit('analyticsData',{
    data:payLoad
  })
}
function wholeZoneAnalyticsDataForDials(data){
  var payLoad = {
    wholeZoneAnalyticsData:data
  }
  /*SocketIo.getSocketIoServer().emit('solidGuageAnalyticsData',{
    data:payLoad
  })*/
}
function wholeZoneAnalyticsLOSDataForMetrics(data){
  var payLoad = {
    wholeZoneAnalyticsData:data
  }

    SocketIo.emit('levelOfServiceForMetrics',{
    data:payLoad
    })


}
/*new*/
var seconds = 5;
var laneSelected=[2,3];
var zoneSelected = [1];
var wholeZoneAnalyticsMetrics={
  terminalRelated:{
    timer:5000,
    intervalId:0,
    averageDataOfSeconds:30,
    lanesSelected:[2,3]
  },
  levelOfServiceForMap:{
    timer:5000,
    intervalId:0,
  }
}
var intervalId;
var losIntervalId;
router.get('/api/getWholeZoneAnalyticsForDials', function (req, res) {
  getAverageDataOfWholeZoneAnalyticsForTerminal();
  res.send("INterval started")
})
router.get('/api/startWholeZoneAnalytics', function (req, res) {
  //startTimerForWholeZoneAnalyticsMetricsForTerminal();
  res.send("WholeZone Data Interval Started")
})
router.get('/api/stopWholeZoneAnalytics', function (req, res) {
  //clearInterval(intervalId);
  res.send("INterval stopped--------------------------")
})
router.get('/api/changeAverageDataOfSecondsForWholeZone/:seconds', function (req, res) {
  seconds=req.params.seconds;
  res.send(seconds)
});
router.get('/api/selectedTerminalsForWholeZone/:terminal', function (req, res) {
  var terminal =req.params.terminal;
  TerminalZonesAndLaneDetails.findOne({"terminal.terminalId":terminal},function(err,result){
    if(err){
      res.send(err)
    }else{
      /*laneSelected = result.lanesSelected;
      zoneSelected = result.zonesSelected;*/
       laneSelected = result.terminal.zoneDetails[0].laneIndex;
       zoneSelected = result.terminal.zoneDetails[0].zoneId;
      res.send(result)
    }
  });
  /*if(terminal == 1){
    laneSelected = [2,3]
  }else if(terminal == 2){
    laneSelected = [4,5]
  }else if(terminal == 3){
    laneSelected = [1,2]
  }*/

})
/*startTimerForWholeZoneAnalyticsMetricsForTerminal();*/
function startTimerForWholeZoneAnalyticsMetricsForTerminal(){
  intervalId =setInterval(function() {
    getAverageDataOfWholeZoneAnalyticsForTerminal();
  },wholeZoneAnalyticsMetrics.terminalRelated.timer);
}
function getAverageDataOfWholeZoneAnalyticsForTerminal(){
  var start = new Date();
  var end = new Date();
  start.setSeconds(start.getSeconds()-seconds);
  var lanesSelected = laneSelected;
   var zonesSelected = zoneSelected;
  WholeZoneTrafficInfoModel.aggregate(
    [
      /*matching through dateTime,laneIndex,zoneIndex*/
      {
        $match: {
                  dateTime:{$gt:start,$lt:end}
                  ,"vehicleAnalytics.cameraExtrinsics.cameraId":{$in:zonesSelected}
        }
      },
      {$unwind: "$vehicleAnalytics.wholeZoneTrafficInformation"},
      {
        $match: {
          "vehicleAnalytics.wholeZoneTrafficInformation.laneIndex":{$in:lanesSelected}
        }
      },
      {
        $group: {
                _id:null,
                levelOfService: {$max: "$vehicleAnalytics.wholeZoneTrafficInformation.levelofService"},
                occupancy: {$avg: "$vehicleAnalytics.wholeZoneTrafficInformation.zoneOccupancy"}
        }
      },
      {$sort: {_id:1}},
      {$project: { _id: 0,levelOfService:"$levelOfService",occupancy:"$occupancy"}}
    ],
    function (err, result) {
      if (err) {
        console.log(err);
        return;
      }
      var data;
      if(result.length>0){
        data = result[0]
      }else{
        data={
          occupancy:0,
          levelOfService:0
        }
      }
      SocketIo.emit('dialDataForWholeZone',data)
    });
}
/*Related to get data for map depending on selected terminal and last seconds*/
/*router.get('/api/startGettingLOSFromWholeZoneAnalytics', function (req, res) {
  //startTimerForWholeZoneLOSAnalyticsMetricsForTerminal();
  res.send("los Interval started")
})
router.get('/api/stopGettingLOSFromWholeZoneAnalytics', function (req, res) {
  clearInterval(losIntervalId);
  res.send("INterval stopped--------------------------")
})
function startTimerForWholeZoneLOSAnalyticsMetricsForTerminal(){
  losIntervalId =setInterval(function() {
    toGetLOSFromWholeZoneAnalyticsData();
  },wholeZoneAnalyticsMetrics.levelOfServiceForMap.timer);
}
function toGetLOSFromWholeZoneAnalyticsData(){
    var start = new Date();
    var end = new Date();
    start.setSeconds(start.getSeconds() - seconds);
    var lanesSelected = laneSelected;
    var zonesSelected = zoneSelected;
    WholeZoneTrafficInfoModel.aggregate(
      [
        {
          $match: {
            dateTime: {$gt: start, $lt: end},
            "vehicleAnalytics.wholeZoneTrafficInformation.laneIndex":{$in:laneSelected}
            ,"vehicleAnalytics.cameraExtrinsics.cameraId":{$in:zoneSelected}
          }
        },
        {$unwind: "$vehicleAnalytics.wholeZoneTrafficInformation"},
        {
          $group: {
            _id:null,
            levelOfService: {$avg: "$vehicleAnalytics.wholeZoneTrafficInformation.levelofService"}/!*,
            occupancy: {$avg: "$vehicleAnalytics.wholeZoneTrafficInformation.zoneOccupancy"}*!/
          }
        },
        {$sort: {_id: 1}},
        {$project: {_id:0,levelOfService: "$levelOfService"/!*, occupancy: "$occupancy"*!/}}
      ],
      function (err, result) {
        if (err) {
          console.log(err);
          return;
        }else{
          SocketIo.emit('losMapData',result)
        }
        console.log(result);
      });
};*/
/************************************************RT-GRAPH(OCCUPANCY)***************************************************************/
router.get('/api/setTheZoneTypeSelectedForRTGraph/:zoneId', function (req, res) {
  var zoneId =req.params.zoneId;
  wholeZoneDetailsDetectedNotifier.setZoneType(zoneId)
  res.send(zoneId)
});
router.get('/api/setTheZoneTypeOfParametersNeeded/:parameter', function (req, res) {
  var type =req.params.parameter;
  wholeZoneDetailsDetectedNotifier.setParameterType(type)
  res.send(type)
});
function notifyWholeZoneDetailsDetected(message){
  notifyClientsRTDetails.wholeZoneNotifier.notifyWholeZoneDetected(message);
}
router.get('/userData/wholeZoneAnalytics/:startDate/:endDate',function(req, res){
  res.set('Content-Type', 'application/octet-stream');
  res.set('Content-Disposition', 'disp-ext-type; filename=wholeZoneAnalyticsDataData.json;creation-date-parm:'+new Date());
  var startTime = new Date(req.params.startDate);
  var endTime = new Date(req.params.endDate);
  res.write('[')
  var stream=WholeZoneTrafficInfoModel.find({"dateTime":
  {$gt:startTime,$lt:endTime}}
  ,{"_id":0,"vehicleAnalytics.wholeZoneTrafficInformation":1,"vehicleAnalytics.roadInfo":1}).stream();
  stream.on('data', function (doc) {
    res.write(JSON.stringify(doc)+',')
  })
  stream.on('error', function (err) {
    console.log(err.stack)
  })
  stream.on('close', function (doc) {
    console.log("done")
    res.write(']')
    res.end();
  })
})

function startIntervalForRefreshingWholeZoneData() {
  var interval = setInterval(function () {
    getWholeZoneLOSDetailsForMapDetails();
  }, 5000);
}
startIntervalForRefreshingWholeZoneData();

function getWholeZoneLOSDetailsForMapDetails(){
  var start = new Date();
  var end = new Date();
  start.setSeconds(start.getSeconds() - 30);
  WholeZoneTrafficInfoModel.aggregate(
    [
      {
        $match: {
          dateTime: {
            $gte:start,
            $lte: end
          }
        }
      },
      {$unwind: "$vehicleAnalytics.wholeZoneTrafficInformation"},
      {
        $group: {
          _id:"$vehicleAnalytics.cameraExtrinsics.cameraId",
          levelOfService: {$last: "$vehicleAnalytics.wholeZoneTrafficInformation.levelofService"}
        }
      },
      {$sort: {dateTime: -1}},
      {$project: {_id: 1, levelOfService: "$levelOfService"}}
    ],
    function (err, result) {
      if (err) {
        console.log(err);
        return;
      }
      var data;
      if(result.length>0){
        data = result
      }else{
        data=[]
      }
      SocketIo.emit('losDataForWholeZone',data)
    });
}

/*router.post('/historicalData/wholeZoneAnalytics',function(req, res){
  var zoneId = parseInt(req.body.zoneId);
  var parameterSelected = req.body.parameterSelected;
  var seriesSelected = parseInt(req.body.seriesSelected)
  var historicalData=[]
  var startDate = new Date();
  var endDate =   new Date();
  var lanesSelected= [];
  var maxLaneForSelectedZone = config.maxLanesOfZone[zoneId-1]
  for(var i=1;i<=maxLaneForSelectedZone;i++){
    lanesSelected.push(i)
  }
  startDate.setMinutes(startDate.getMinutes()-seriesSelected);
  historicalDataAggregationModule.wholeZoneAggregate.
  getAggregatedHistoricalData(startDate,endDate,parameterSelected,zoneId,lanesSelected,
    function(result){
      if(result &&result.length>0){
        for(i = 0;i<result.length;i++){
          var parameterAggregation = result[i].occupancy;
          historicalData[i]=new Array(seriesSelected);
          historicalData[i].fill(null)
          for(var data=0;data<parameterAggregation.length;data++){
            var parameterObj = parameterAggregation[data];
            historicalData[i][parameterObj.minutes%seriesSelected]=Math.round(parameterObj.occupancy);
            if(historicalData.length==result.length && data==parameterAggregation.length-1){
              res.send(historicalData)
            }
          }
        }
      }else{
        res.send(historicalData)
      }

    })
})*/


/*api to get historical data*/
router.post('/historicalData/wholeZoneAnalytics/secondWiseData',function(req, res){
  var zoneId = parseInt(req.body.zoneId);
  var parameterSelected = req.body.parameterSelected;
  var seriesSelected = parseInt(req.body.seriesSelected)
  var startDate = new Date();
  startDate.setMinutes(startDate.getMinutes()-seriesSelected);
  var endDate =   new Date();
  var lanesSelected= [];
  var maxLaneForSelectedZone = config.maxLanesOfZone[zoneId-1]
  for(var i=1;i<=maxLaneForSelectedZone;i++){
    lanesSelected.push(i)
    if(i==maxLaneForSelectedZone){
      historicalDataAggregationModule.wholeZoneAggregate.getAggregatedHistoricalDataOverSeconds
      (endDate,endDate,seriesSelected,parameterSelected,zoneId,lanesSelected,function(result){
        res.send(result)
      })
    }
  }
})

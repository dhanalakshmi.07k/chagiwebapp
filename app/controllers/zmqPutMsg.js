/**
 * Created by rranjan on 11/29/15.
 */

var express = require('express');
var mongoose = require('mongoose');
var zmq = require('zmq');
var config = require('../../config/config');

var HashTable = require('hashtable');
var portHashtable = new HashTable();

//var SocketIo = require('../../config/express');

var router = express.Router();
var zmqPubSocket = zmq.socket('pub');
module.exports = function (app) {
  app.use('/', router);
};


router.post('/putMsgIn0MQAnalyticsByLane', function (req, res, next) {
  pushData("portVehicleAnalyticsByLane",JSON.stringify(req.body))
  res.send("Vehicle Analytics data received")

});
router.post('/putMsgIn0MQAnalyticsByWholeZoneAnalytics', function (req, res, next) {
  pushData("portVehicleWholeZoneAnalytics",JSON.stringify(req.body))
  res.send()
});

router.post('/putMsgIn0MQNotification', function (req, res, next) {
  pushData("portNotification",JSON.stringify(req.body))
  res.send()
});

var pushData = function(portName ,dataToPush){
  var zmqSocket = getPort(portName)
  zmqSocket.send(dataToPush);

}

function getPort(portName){

  var port = portHashtable.get(portName);
  if(!port){
    var zmqSocket = zmq.socket('push');
    var portIp = config.zmq.sendHost;
    var portEndPoint = config.zmq[portName];
    var zmqPortPart = 'tcp://'+portIp+':'+portEndPoint;
    zmqSocket.connect(zmqPortPart)
    port = zmqSocket;
    portHashtable.put(portName,port);
  }
  /*console.log("returning port :");
  console.log(port);*/
  return port;

}

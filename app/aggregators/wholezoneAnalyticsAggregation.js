/**
 * Created by zendynamix on 01-06-2016.
 */



var mongoose = require('mongoose'),
    wholeZoneTrafficInfoModel = require('../models/wholeZoneTrafficInfo'),
     Q = require("q");



function getAverageDataOfWholeZoneAnalyticsForTerminal(laneSelected, zoneSelected, timeSelected) {
  var deferedWholeZone = Q.defer();

  var start = new Date();
  var end = new Date();
  start.setSeconds(start.getSeconds() - timeSelected);
  var lanesSelected = laneSelected;
  var zonesSelected = zoneSelected;
  wholeZoneTrafficInfoModel.aggregate(
    [
      /*matching through dateTime,laneIndex,zoneIndex*/
      {
        $match: {
          dateTime: {$gt: start, $lt: end},
          "vehicleAnalytics.cameraExtrinsics.cameraId": zoneSelected
        }
      },
      {$unwind: "$vehicleAnalytics.wholeZoneTrafficInformation"},
      {
        $match: {
          "vehicleAnalytics.wholeZoneTrafficInformation.laneIndex": {$in: laneSelected}
        }
      },
      {
        $sort: {
          "dateTime":-1
        }
      },
      {
        $group: {
          _id: null,
          levelOfService: {$last: "$vehicleAnalytics.wholeZoneTrafficInformation.levelofService"},
          occupancy: {$avg: "$vehicleAnalytics.wholeZoneTrafficInformation.zoneOccupancy"}
        }
      },
      {$sort: {_id: 1}},
      {$project: {_id: 0, levelOfService: "$levelOfService", occupancy: "$occupancy"}}
    ],

    function (err, result) {

      var data;
      if (err) {
        deferedWholeZone.reject(err);
      }
      if (result.length > 0) {
        data = result[0];
        deferedWholeZone.resolve(data);
      } else {
        data = {
          occupancy: 0,
          levelOfService: 0
        }
        deferedWholeZone.resolve(data);
      }
    });
  return deferedWholeZone.promise;


}



module.exports={
  getAverageDataOfWholeZoneAnalyticsForTerminal:getAverageDataOfWholeZoneAnalyticsForTerminal
}

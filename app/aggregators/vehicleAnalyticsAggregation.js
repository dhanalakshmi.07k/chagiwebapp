/**
 * Created by zendynamix on 01-06-2016.
 */


var mongoose = require('mongoose'),
    VehicleAnalyticsModel = require("../models/vehicleAnalytics"),
    Q = require("q");


var sendVehicleAnalyticsDetailsForCards= function(laneSelected, zoneSelected, timeSelected) {
  var defered = Q.defer();
  var start = new Date();
  var end = new Date();
  start.setSeconds(start.getSeconds() - timeSelected);
  VehicleAnalyticsModel.aggregate(
    [
      {
        $match: {
          dateTime: {$gt: start, $lt: end},
          "vehicleAnalytics.cameraExtrinsics.cameraId": zoneSelected
        }
      },
      {$unwind: "$vehicleAnalytics.vehicleDetectionEventsByLane"},
      {
        $match: {
          "vehicleAnalytics.vehicleDetectionEventsByLane.laneIndex": {$in: laneSelected}
        }
      },
      {
        $group: {
          _id: null,
          speed: {$avg: "$vehicleAnalytics.vehicleDetectionEventsByLane.speed"},
          timeHeadway: {$avg: "$vehicleAnalytics.vehicleDetectionEventsByLane.headway.timeHeadway"},
          spaceHeadway: {$avg: "$vehicleAnalytics.vehicleDetectionEventsByLane.headway.spaceHeadway"},
          volume: {$sum: 1}
        }
      },
      {$sort: {_id: 1}},
      {
        $project: {
          _id: 0,
          speed: "$speed",
          timeHeadway: "$timeHeadway",
          spaceHeadway: "$spaceHeadway",
          volume: "$volume"
        }
      }
    ],
    function (err, result) {

      var data;
      if (err) {
        defered.reject(err);
      }
      if (result.length > 0) {
        data = result[0];
        defered.resolve(data);
      } else {
        data = {
          speed: 0,
          timeHeadway: 0,
          spaceHeadway: 0,
          volume: 0
        }
        defered.resolve(data);

      }
    });

  return defered.promise;
}


module.exports={
  sendVehicleAnalyticsDetailsForCards:sendVehicleAnalyticsDetailsForCards
}


/**
 * Created by zendynamix on 01-06-2016.
 */

var vehicleAnalytics= require('./vehicleAnalyticsAggregation.js');
var wholeZoneAnalytics=require('./wholezoneAnalyticsAggregation.js')



module.exports={
  vehicleAnalytics:vehicleAnalytics,
  wholeZoneAnalytics:wholeZoneAnalytics

}

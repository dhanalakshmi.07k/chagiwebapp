/**
 * Created by Suhas on 1/15/2016.
 */
var mongoose = require('mongoose'),
  VehicleAnalytics = require('../models/vehicleAnalytics'),
  config = require('../../config/config');

/*mongoose.connect(config.db);
var db = mongoose.connection;*/

function aggregationMethod(){
  //console.log(db)
  var time = new Date();
  VehicleAnalytics.aggregate([
    { $unwind: "$vehicleAnalytics" }
  ], function (err, result) {
    if (err) {
      console.log(err);
      return;
    }
    console.log(result);
  });
}
aggregationMethod();

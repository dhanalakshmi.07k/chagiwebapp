/**
 * Created by Suhas on 1/23/2016.
 */
var express = require('express'),
  router = express.Router(),
  mongoose = require('mongoose')
  smsSender =require('../smsNotifier'),
  notificationModel = mongoose.model('notificationModel'),
  async = require("async");
mongoose.Promise = global.Promise;
var notificationValidationPeriod=300;

var isNotificationExist = function(notificationObj,uniqueData,callback){
  console.log("Validation Period"+notificationValidationPeriod)
  var start = new Date();
  var end = new Date();
  start.setSeconds(start.getSeconds()-notificationValidationPeriod);
  var vehicleIncidentDetection = uniqueData;
  var currentIncidentObjArray= [];
  var count=0;
  var promises =[]
  var cameraId= notificationObj.vehicleAnalytics.cameraExtrinsics.cameraId;
  vehicleIncidentDetection.forEach(function(vehicleIncident){
    count ++;
    var currentIncidentObj =vehicleIncident;
    var laneIndex = vehicleIncident.laneIndex;
    var segmentInLane = vehicleIncident.segmentInLane;
    var incidentType = vehicleIncident.incidentType;

    var p = notificationModel.aggregate(/**/
      [
        {$match: {"dateTime" : {$gt:start,$lt:end},"vehicleAnalytics.cameraExtrinsics.cameraId":cameraId}},
        {$unwind: "$vehicleAnalytics.incidentDetection"},
        {$match:{"vehicleAnalytics.incidentDetection.laneIndex":laneIndex,"vehicleAnalytics.incidentDetection.segmentInLane":segmentInLane,
                 "vehicleAnalytics.incidentDetection.incidentType":incidentType}},
        {$group: {"_id":null,count:{$sum:1}}}
      ],
    function (err,result) {
      if (err) {
        console.log(err);
        return;
      }else{
        console.log(result)
        if(result.length>0){
          console.log("Duplicate Notification Entry")
        }else if(result.length==0){
          console.log("Notification Entry")
          currentIncidentObjArray.push(currentIncidentObj);
        }
      }
    });
    promises.push(p);
  }) ///end of foreach
  Promise.all(promises).then(function(values) {
    callback(notificationObj,currentIncidentObjArray)
  });
  }
var setNotificationValidationPeriod = function(timeInSeconds){
  notificationValidationPeriod = timeInSeconds;
}

module.exports = {
  isNotificationExist:isNotificationExist,
  setNotificationValidationPeriod:setNotificationValidationPeriod
};

/**
 * Created by zendynamix on 27-04-2016.
 */

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
var random = require('mongoose-simple-random');
var cameraMarkerSchema = new mongoose.Schema(
  {
    id:Number,
    "cameraMarkerArray":[
      {
        "lat": Number,
        "lng": Number,
      }
    ],
    cameraMarkerTime:{type:Date,default:Date.now},
  },{collection: "cameraMarkerDetails"});
cameraMarkerSchema.plugin(random);
module.exports = mongoose.model('cameraMarkerDetails',cameraMarkerSchema);

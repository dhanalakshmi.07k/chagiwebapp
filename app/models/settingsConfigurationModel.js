/**
 * Created by Suhas on 4/29/2016.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var settingsConfiguration = new mongoose.Schema(
  {
    id:Number,
    settingTabConfiguration:{
      showSimulatorTab:Boolean,
      showDataDownloadTab:Boolean,
      showUserDetailsTab:Boolean
    },
    notificationSettings:{
      notificationFilterPeriod:Number
    }

  },{collection: "settingsConfiguration"});

module.exports =mongoose.model('settingsConfiguration',settingsConfiguration);


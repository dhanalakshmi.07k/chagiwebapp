/**
 * Created by zendynamix on 22-11-2015.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var mapSchema = new mongoose.Schema(
  {
    pointId:Number,
    title: String,
    lat:String,
    lng:String,
    description: String,
    plotTime:{type:Date,default:Date.now},
  },{collection: "mapDetails"});

mongoose.model('plotMap',mapSchema);

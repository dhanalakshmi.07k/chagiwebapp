var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
var random = require('mongoose-simple-random');
var notifySchema = new mongoose.Schema(
  {
    numberOfVechicle:String,
    typeOfVehicle : String,
    message: String,
    messageTime:{type:Date,default:Date.now},
  },{collection: "notify"});
notifySchema.plugin(random);
module.exports = mongoose.model('notify',notifySchema);

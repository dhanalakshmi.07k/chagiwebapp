/**
 * Created by Suhas on 1/21/2016.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var smsPhoneNoForNotificationSchema = new Schema({
 phoneNo:Number,
  phoneId:Number
},{collection: "smsPhoneNoNotification"});

module.exports = mongoose.model('smsPhoneNoNotificationModel',smsPhoneNoForNotificationSchema);

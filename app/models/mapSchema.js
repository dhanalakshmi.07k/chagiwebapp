/**
 * Created by zendynamix on 28-12-2015.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
var mapSchema2 = new mongoose.Schema(
  {
    pointId:Number,
    title: String,
    lat:Number,
    lng:Number,
    description: String,
    plotTime:{type:Date,default:Date.now},
  },{collection: "mapDetailsNew"});
module.exports = mongoose.model('mapDetailsNew',mapSchema2);



/**
 * Created by Suhas on 1/19/2016.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
var random = require('mongoose-simple-random');
/*var terminalSchema = new mongoose.Schema(
  {
    "terminalId":Number,
    "zonesSelected":Array,
    "lanesSelected":Array
  },{collection: "terminalZOneANdLaneDetails"});*/

/*
var terminalSchema = new mongoose.Schema(
{
  "terminal": {
    "terminalId" : Number,
    "cameraId" : Array,
  "zoneDetails": [
    {
      "zoneId": Number
    }
  ],
  "lanedetails": [
    {
      "laneIndex": Number
    }
  ]
  }
})*/

var terminalSchema = new mongoose.Schema(
  {
    "terminal": {
      "terminalId" : Number,
      "zoneDetails":[{
        "cameraId" : Array,
        "zoneId": Number,
        "laneIndex": Array
      }]


    }
  },{collection: "terminalZOneANdLaneDetails"});

module.exports = mongoose.model('terminalModel',terminalSchema);

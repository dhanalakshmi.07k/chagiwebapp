/**
 * Created by zendynamix on 27-04-2016.
 */

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
var random = require('mongoose-simple-random');
var zoneSegmentInLaneSchema = new mongoose.Schema(
  {
    id:Number,
    "zoneSegmentArray":[
      {
        "lat": Number,
        "lng": Number,
      }
    ],
    zoneSegmentLaneTime:{type:Date,default:Date.now},
  },{collection: "zoneSegmentInLaneDetails"});
zoneSegmentInLaneSchema.plugin(random);
module.exports = mongoose.model('zoneSegmentInLaneDetails',zoneSegmentInLaneSchema);

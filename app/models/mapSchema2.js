var mongoose = require('mongoose'),
  config = require('../../config/config');
Schema = mongoose.Schema;
var mapSchema2 = new mongoose.Schema(
  {
    pointId:Number,
    title: String,
    source:{
      lat:Number,
      lng:Number
    },
    destination:{
      lat:Number,
      lng : Number,
    },
    description: String,
    plotTime:{type:Date,default:Date.now},
  },{collection: "mapDetails2"});


module.exports = mongoose.model('plotMap2',mapSchema2);;

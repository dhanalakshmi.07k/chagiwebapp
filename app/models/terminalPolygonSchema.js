/**
 * Created by zendynamix on 23-01-2016.
 */

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
var random = require('mongoose-simple-random');
var polygonSchema = new mongoose.Schema(
  {
    id:String,
    "polygonobj":
      {
        "corordinat1Lat": Number,
        "corordinat1Lan": Number,
        "corordinat2Lat": Number,
        "corordinat2Lan": Number
      },
    threshold:Number,
   polygonTime:{type:Date,default:Date.now},
  },{collection: "polygonDetails"});
polygonSchema.plugin(random);
module.exports = mongoose.model('polygonDetails',polygonSchema);

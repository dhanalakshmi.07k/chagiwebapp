/**
 * Created by Suhas on 12/22/2015.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
var random = require('mongoose-simple-random');
var vehicleAnalyticsSchema = new Schema(
  {
    "vehicleAnalytics" : {
      "roadInfo" : {
        "roadId" : Number,
        "roadName" : String,
        "laneCount" : Number
      },
      "cameraExtrinsics" : {
        "cameraId" : Number,
        "cameraName" : String,
        "cameraDirection" : String,
        "cameraLatitude" : Number,
        "cameraLongitude" : Number,
        "cameraHeight" : Number  // in feet from ground
      },
      "cameraIntrinsics" : {
        "cameraFocalLength" : Number, // in mm
        "cameraAngle" : Number, // in degrees wrt horizontal plane
        "imageWidth" : String,
        "imageHeight" : String,
        "frameRate" : Number // Frames per second
      },
      vehicleDetectionEventsByLane: [
        {
          laneIndex: Number,
          timeStamp: String,
          speed: Number,
          headway: {
            spaceHeadway: Number,
            timeHeadway: Number
          },
          timeVehicleSeenOverDetectorArea: Number
        }
      ]
    },
    "dateTime":Date
  },{collection: "vehicleAnalyticsPublisher"});
vehicleAnalyticsSchema.plugin(random);
module.exports = mongoose.model('vehicleAnalyticsPublisher',vehicleAnalyticsSchema);


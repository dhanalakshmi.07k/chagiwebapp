var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
var random = require('mongoose-simple-random');
var notifySchema = new mongoose.Schema(
  {
    "vehicleAnalytics": {
      "roadInfo" : {
        "roadId" : Number,
        "roadName" : String,
        "laneCount" : Number
      },
      "cameraExtrinsics" : {
        "cameraId" : Number,
        "cameraName" : String,
        "cameraDirection" : String,
        "cameraLatitude" : Number,
        "cameraLongitude" : Number,
        "cameraHeight" : Number
      },
      "cameraIntrinsics" : {
        "cameraFocalLength" : Number,
        "cameraAngle" : Number,
        "imageWidth" : String,
        "imageHeight" : String,
        "frameRate" : Number
      },
      //"timeStamp": “7:34:45 PM EST”,
      "timeStamp": String,
      "incidentDetection": [
        {
          "laneIndex": Number,
          "segmentInLane": Number,
          "incidentType": String,
          "incidentImage": String
        }
      ]
    },
    "dateTime":Date
  },{collection: "notificationModel"});
notifySchema.plugin(random);
module.exports = mongoose.model('notificationModel',notifySchema);

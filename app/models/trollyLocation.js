var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
var trolleyLocationSchema = new mongoose.Schema(
  {
    x:String,
    y : String,
    macAddress: String,
    refreshDateTime:{type:Date,default:Date.now},
  },{collection: "trolleyLocation"});

module.exports = mongoose.model('TrolleyLocation',trolleyLocationSchema);

/**
 * Created by zendynamix on 08-01-2016.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
var random = require('mongoose-simple-random');
var wholeZoneTrafficPublisherInfoSchema = new Schema(
  {
    "vehicleAnalytics" : {
      "roadInfo" : {
        "roadId" : Number,
        "roadName" : String,
        "laneCount" : Number
      },
      "cameraExtrinsics" : {
        "cameraId" : Number,
        "cameraName" : String,
        "cameraDirection" : String,
        "cameraLatitude" : Number,
        "cameraLongitude" : Number,
        "cameraHeight" : Number  // in feet from ground
      },
      "cameraIntrinsics" : {
        "cameraFocalLength" : Number, // in mm
        "cameraAngle" : Number, // in degrees wrt horizontal plane
        "imageWidth" : String,
        "imageHeight" : String,
        "frameRate" : Number // Frames per second
      },
      "wholeZoneTrafficInformation" : [
        {
          "laneIndex" : Number,
          "zoneOccupancy" : Number, // in %
          "zoneSpeed" : Number, // in kmph
          "levelofService" : Number // 0, 1, 2, 3, 4 (see LTA LOS Document)
        }
      ]
    },
    "dateTime":String
  },{collection: "publisherWholeZoneTraffic"});
wholeZoneTrafficPublisherInfoSchema.plugin(random);
module.exports = mongoose.model('publisherWholeZoneTraffic',wholeZoneTrafficPublisherInfoSchema);

/**
 * Created by rranjan on 1/15/16.
 */
"use strict";
var mongoose = require('mongoose'),
  vehicleAnalyticsModel = mongoose.model('wholeZoneTrafficInfo'),
  wholeZoneDetailsDetectedNotifier = require('../rtGraphNotifier/wholeZoneAnalytics'),
  SocketIo = require('../../config/socketIo').getSocketIoServer(),
  notifyClientsRTDetails = require("../rtGraphNotifier/multipleSocketRTDataPusher"),
    publishWholeZoneData=require("../rawDataPublisher")


  var handle = function(message){

    var inputDataFormZmq=message;
    publishWholeZoneData.rawWholeZoneDataPublisher.publishRawWholeZoneAnalyticsData(message)
  notifyClientsRTDetails.wholeZoneNotifier.notifyWholeZoneDetected(inputDataFormZmq);

  var AnalyticsObjBywholeZone = vehicleAnalyticsModel();
  var roadInfo =message.vehicleAnalytics.roadInfo;
  var cameraExtrinsics = message.vehicleAnalytics.cameraExtrinsics;
  var cameraIntrinsics = message.vehicleAnalytics. cameraIntrinsics;
  for(var j=0;j<message.vehicleAnalytics.wholeZoneTrafficInformation.length;j++){
    var vehicleObjByWholeZone = {
      laneIndex:message.vehicleAnalytics.wholeZoneTrafficInformation[j].laneIndex,
      zoneOccupancy: message.vehicleAnalytics.wholeZoneTrafficInformation[j].zoneOccupancy,
      zoneSpeed: message.vehicleAnalytics.wholeZoneTrafficInformation[j].zoneSpeed,
      levelofService: message.vehicleAnalytics.wholeZoneTrafficInformation[j].levelofService
    }
    AnalyticsObjBywholeZone.vehicleAnalytics.wholeZoneTrafficInformation.push(vehicleObjByWholeZone);
  }

  AnalyticsObjBywholeZone.vehicleAnalytics.roadInfo=roadInfo;
  AnalyticsObjBywholeZone.vehicleAnalytics.cameraExtrinsics=cameraExtrinsics;
  AnalyticsObjBywholeZone.vehicleAnalytics.cameraIntrinsics=cameraIntrinsics;
  AnalyticsObjBywholeZone.dateTime = new Date();
  /*notifyWholeZoneDetailsDetected(AnalyticsObjBywholeZone);*/
  AnalyticsObjBywholeZone.save(function(err,result){
    if(err){
      console.log(err);
    }

    /*console.log("saving Whole Zone Analytics Data");*/
  })

}
function notifyWholeZoneDetailsDetected(message){
  wholeZoneDetailsDetectedNotifier.wholeZoneNotifierMethod(message,function(err,data){
    /*console.log(data)*/
    /*SocketIo.emit('vehicleDetectionEventNotifier',data)*/
  })
}
module.exports= handle;

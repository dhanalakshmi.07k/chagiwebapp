/**
 * Created by rranjan on 1/15/16.
 */
  "use strict"
var express = require('express'),
  router = express.Router(),
  mongoose = require('mongoose'),
  notificationModel = mongoose.model('notificationModel'),
  notificationVerifier = require('../vehicleNotificationValidation/index'),
  smsSender =require('../smsNotifier'),
  smsPhoneNoForNotification = mongoose.model('smsPhoneNoNotificationModel'),
  smsNotificationFlag = false,
  SocketIo = require('../../config/socketIo').getSocketIoServer();

module.exports = function (app) {
  app.use('/', router);
};
var handle = function(message) {

  var vehicleIncidentDetection = message.vehicleAnalytics.incidentDetection;
  for(var i =0;i<vehicleIncidentDetection.length;i++){
      var laneIndex =vehicleIncidentDetection[i].laneIndex;
      var segmentInLane =vehicleIncidentDetection[i].segmentInLane;
    var incidentType =vehicleIncidentDetection[i].incidentType;

      for(var j =0;j<vehicleIncidentDetection.length;j++){
        var laneIndexJ =vehicleIncidentDetection[j].laneIndex;
        var segmentInLaneJ =vehicleIncidentDetection[j].segmentInLane;
        var incidentTypeJ =vehicleIncidentDetection[j].incidentType;

        if(i!=j){

          if(laneIndex==laneIndexJ && segmentInLane==segmentInLaneJ && incidentType==incidentTypeJ){

            vehicleIncidentDetection.splice(j, 1);
            /*console.log("same");*/
          }else{
            /*console.log("different");*/
          }
        }
        if(i==vehicleIncidentDetection.length-1 && j==vehicleIncidentDetection.length-1){
          saveAndPushNotification(message,vehicleIncidentDetection)
         /* message.vehicleAnalytics.incidentDetection = vehicleIncidentDetection;
          notificationVerifier.isNotificationExist(message,vehicleIncidentDetection,saveData);*/
        }
      }
  }

}

function saveAndPushNotification(message,notifyData){
 
  if(notifyData.length>0){
    var msgArray = [];
    for(var i=0;i<notifyData.length;i++){
      var data = {
        notificationType:message.vehicleAnalytics.incidentDetection[0].incidentType,
        noOfVehicle: 0,
        zoneIndex: message.vehicleAnalytics.cameraExtrinsics.cameraId,
        laneIndex: notifyData[i].laneIndex,
        segmentInLane: notifyData[i].segmentInLane,
        snapShot: notifyData[i].incidentImage,
        messageTime:new Date()
      }
      var msg = "Vehicle Stopped at segment in lane "+data.segmentInLane+"of Zone"+data.zoneIndex;
      msgArray.push(msg);
      if(i==notifyData.length-1){
        sendNotification(msgArray);
      }
      SocketIo.emit('notificationMsg', data);

      if(i==notifyData.length-1){
        var notificationObj = notificationModel();
        notificationObj.vehicleAnalytics.roadInfo = message.vehicleAnalytics.roadInfo;
        notificationObj.vehicleAnalytics.cameraExtrinsics = message.vehicleAnalytics.cameraExtrinsics;
        notificationObj.vehicleAnalytics.cameraIntrinsics = message.vehicleAnalytics.cameraIntrinsics;
        notificationObj.vehicleAnalytics.timeStamp = message.vehicleAnalytics.timeStamp;
        notificationObj.vehicleAnalytics.incidentDetection = notifyData;
        notificationObj.dateTime = new Date();
        notificationObj.save(function (err) {
          if (err) {
            console.log(err);
          }else{
            console.log("Data Saved")
          }
        })
      }
    }
  }

}
/*function saveData(message,notifyData){

  if(notifyData.length>0){
    var msgArray = [];
    for(var i=0;i<notifyData.length;i++){
      var data = {
        notificationType:message.vehicleAnalytics.incidentDetection[0].incidentType,
        noOfVehicle: 0,
        zoneIndex: message.vehicleAnalytics.cameraExtrinsics.cameraId,
        laneIndex: notifyData[i].laneIndex,
        segmentInLane: notifyData[i].segmentInLane,
        snapShot: notifyData[i].incidentImage,
        messageTime:new Date()
      }
      var msg = "Vehicle Stopped at segment in lane "+data.segmentInLane+"of Zone"+data.zoneIndex;
      msgArray.push(msg);
      if(i==notifyData.length-1){
        sendNotification(msgArray);
      }
      SocketIo.emit('notificationMsg', data);

      if(i==notifyData.length-1){
        var notificationObj = notificationModel();
        notificationObj.vehicleAnalytics.roadInfo = message.vehicleAnalytics.roadInfo;
        notificationObj.vehicleAnalytics.cameraExtrinsics = message.vehicleAnalytics.cameraExtrinsics;
        notificationObj.vehicleAnalytics.cameraIntrinsics = message.vehicleAnalytics.cameraIntrinsics;
        notificationObj.vehicleAnalytics.timeStamp = message.vehicleAnalytics.timeStamp;
        notificationObj.vehicleAnalytics.incidentDetection = notifyData;
        notificationObj.dateTime = new Date();
        notificationObj.save(function (err) {
          if (err) {
            console.log(err);
          }else{
            console.log("Data Saved")
          }
        })
      }
    }
  }else{
  }
 /!* var notificationObj = notificationModel();
  notificationObj.vehicleAnalytics.roadInfo = message.vehicleAnalytics.roadInfo;
  notificationObj.vehicleAnalytics.cameraExtrinsics = message.vehicleAnalytics.cameraExtrinsics;
  notificationObj.vehicleAnalytics.cameraIntrinsics = message.vehicleAnalytics.cameraIntrinsics;
  notificationObj.vehicleAnalytics.timeStamp = message.vehicleAnalytics.timeStamp;
  notificationObj.vehicleAnalytics.incidentDetection = message.vehicleAnalytics.incidentDetection;
  notificationObj.dateTime = new Date();
  notificationObj.save(function (err, result) {
    if (err) {
      console.log(err);
    }else{
      console.log("notification data saved")
    }
  })
  var msgArray = [];
  var notifyObjj= message.vehicleAnalytics.incidentDetection;
  console.log(notifyObjj.length)
  for(var i=0;i<notifyObjj.length;i++){
    var data = {
      notificationType:notifyObjj[0].incidentType,
      noOfVehicle: 0,
      zoneIndex: message.vehicleAnalytics.cameraExtrinsics.cameraId,
      laneIndex: notifyObjj[i].laneIndex,
      segmentInLane: notifyObjj[i].segmentInLane,
      snapShot: notifyObjj[i].incidentImage,
      messageTime:new Date()
    }
    var msg = "Vehicle Stopped at lane "+data.laneIndex+" of Zone"+data.zoneIndex;
    msgArray.push(msg);
    if(i==notifyData.length-1){
      sendNotification(msgArray);
    }
    SocketIo.emit('notificationMsg', data);

  }*!/
}*/

function sendNotification(msg){
    smsPhoneNoForNotification.findOne({"phoneId":1},function(err,result){
      if(err){
        res.send(err);
      }else if(result){
        for(var j=0;j<msg.length;j++){
          smsSender.autoSmsNotifier(result.phoneNo,msg[j]);
        }
      }
    })
}
module.exports= handle;

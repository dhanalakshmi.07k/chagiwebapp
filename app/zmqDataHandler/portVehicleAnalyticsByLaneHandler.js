/**
 * Created by rranjan on 1/15/16.
 */

var mongoose = require('mongoose'),
  VehicleAnalytics = require('../models/vehicleAnalytics.js'),
  vehicleDetectedNotifier = require('../rtGraphNotifier/vehicleAnalytics'),
  SocketIo = require('../../config/socketIo').getSocketIoServer(),
  notifyClientsRTDetails = require("../rtGraphNotifier/multipleSocketRTDataPusher");
  publishVehicleData=require("../rawDataPublisher");
  rawVehicleAnalyticsClonedObject=require("../utility/objectCloneUtility")


var handle = function(message) {
  var rawVehicleAnalyticsData=rawVehicleAnalyticsClonedObject.getObjectClone(message);
  var vehicleAnalyticsObj = VehicleAnalytics();
  vehicleAnalyticsObj.vehicleAnalytics = message.vehicleAnalytics;
  vehicleAnalyticsObj.dateTime = new Date();

  publishVehicleData.rawVehicleDataPublisher.publishRawVehicleAnanalyticsData(rawVehicleAnalyticsData);
  notifyClientsRTDetails.vehicleNotifier.notifyVehiclesDetected(message);
  vehicleAnalyticsObj.save(function (err, result) {
    if (err) {
      console.log(err);
    }
    console.log("saving Vehicle Detection Data");
  })
}
function notifyVehiclesDetected(message){
  vehicleDetectedNotifier.vehiclesNotifierMethod(message,function(err,data){
    /*SocketIo.emit('vehicleDetectionEventNotifier',data)*/
  })
}
module.exports= handle;


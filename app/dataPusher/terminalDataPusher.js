/**
 * Created by Suhas on 4/26/2016.
 */
var socketConnectionDetailsCollection = require("../multipleSocket");
var SocketIo = require('../../config/socketIo').getSocketIoServer();
var publisherList=[];
var express = require('express');
var Q = require("q");
    router = express.Router(),
    SocketIo = require('../../config/socketIo').getSocketIoServer(),
    VehicleAnalyticsModel = require("../models/vehicleAnalytics"),
    TerminalZonesAndLaneDetails = require("../models/terminalGenerator"),
    wholeZoneTrafficInfoModel = require('../models/wholeZoneTrafficInfo');

  SocketIo.on('connection', function (newSocket) {
  if (newSocket) {
      newSocket.on("setAnalyticsConfiguration", function (configDetails) {
          var roomList = newSocket.rooms;
          var newRoom = "card_t"+configDetails.terminalDetails.terminalSelected+""+configDetails.terminalDetails.timeSelected;
          for(var i=1;i<roomList.length;i++){
              var previousRoom =roomList[i];
              var roomNameFirstLetter = previousRoom.substring(0,5);
              if(roomNameFirstLetter ==='card_'){
                  if(previousRoom===newRoom){
                  }else{
                      newSocket.leave(previousRoom)
                      newSocket.join(newRoom)
                      console.log("client "+newSocket.id+" left room "+previousRoom+" " +"and Joined "+newRoom+" room")
                  }
                  var connId= newSocket.id;
                  publisherList=[];
                  socketConnectionDetailsCollection.updateSocketArrayObj(connId,configDetails);
                  var publisherDetails = {
                      "publisherName":newRoom,
                      "terminalSelected":configDetails.terminalDetails.terminalSelected,
                      "aggregationPeriod":configDetails.terminalDetails.timeSelected,
                      "noOfClients":1
                  }
                  vehicleAnalyticsCardDataPublisher(publisherDetails);
                  wholeZoneAnalyticsCardDataPublisher(publisherDetails);
                  emitTerminalData();
              }
          }
      })
    newSocket.on("disconnect", function (configDetails) {
      publisherList=[];
      emitTerminalData();
    })
  }
});
  function emitTerminalData(){
  var socketDetails = socketConnectionDetailsCollection.getSocketArrayDetails();
  if(socketDetails.length!=0){
      for(var i=0;i<socketDetails.length;i++){
        var terminalDetails = socketDetails[i].details.terminalDetails;
        var terminalId = terminalDetails.terminalSelected;
        var aggregationPeriod = terminalDetails.timeSelected;
        var publisherName = "card_t"+terminalId+""+aggregationPeriod;
        var publisherObj = {
          "publisherName":publisherName,
          "terminalSelected":terminalId,
          "aggregationPeriod":aggregationPeriod,
          "noOfClients":1
        }
        if(publisherList.length==0){
          publisherList.push(publisherObj)
        }else if(publisherList.length!=0 && !isPublisherAlreadyExist(publisherList,publisherObj)){
          publisherList.push(publisherObj)
        }
    }
  }
}

  /*vehicle related card data pusher(time-headway,speed-headway,speed,volume)*/
  function vehicleAnalyticsCardDataPublisher(publisherDetails) {
  TerminalZonesAndLaneDetails.findOne({"terminal.terminalId": publisherDetails.terminalSelected},
    function (err, result) {
      if (err) {
        res.send(err)
      } else {
        var promises = [];
        var valueCount = 0;
        var timeSelected = publisherDetails.aggregationPeriod;
        for (var k = 0; k < result.terminal.zoneDetails.length; k++) {
          var laneSelected = result.terminal.zoneDetails[k].laneIndex;
          var zoneSelected = result.terminal.zoneDetails[k].zoneId;
          var publisherName = publisherDetails.publisherName;
          var promise = aggregatingVehicleAnalyticsDetailsForCards(laneSelected, zoneSelected, timeSelected,publisherName).then(function (data) {
            return Q(data);
          });
          promises.push(promise);
        }
        Q.all(promises).then(function (resData) {
          var avgSpeed = 0, avgTimeHeadway = 0, avgSpaceHeadway = 0, avgvolume = 0;
          for (var j = 0; j < resData.length; j++) {
            if (resData[j].speed != 0) {
              valueCount++;
            }
            avgSpeed += resData[j].speed;
            avgTimeHeadway += resData[j].timeHeadway;
            avgSpaceHeadway += resData[j].spaceHeadway;
            avgvolume += resData[j].volume;

          }
          var data = {}
          if (valueCount === 0) {
            data.speed = avgSpeed;
            data.timeHeadway = avgTimeHeadway;
            data.spaceHeadway = avgSpaceHeadway;
            data.volume = avgvolume;
          }
          else {
            data.speed = avgSpeed / valueCount;
            data.timeHeadway = avgTimeHeadway / valueCount;
            data.spaceHeadway = avgSpaceHeadway / valueCount;
            data.volume = avgvolume / valueCount;
          }
          SocketIo.to(publisherName).emit("vehicleCardData", data);
        });

      }
    });
}
  function aggregatingVehicleAnalyticsDetailsForCards(laneSelected, zoneSelected, timeSelected) {
  var defered = Q.defer();
  var start = new Date();
  var end = new Date();
  start.setSeconds(start.getSeconds() - timeSelected);
  VehicleAnalyticsModel.aggregate(
    [
      {
        $match: {
          dateTime: {$gt: start, $lt: end},
          "vehicleAnalytics.cameraExtrinsics.cameraId": zoneSelected
        }
      },
      {$unwind: "$vehicleAnalytics.vehicleDetectionEventsByLane"},
      {
        $match: {
          "vehicleAnalytics.vehicleDetectionEventsByLane.laneIndex": {$in: laneSelected}
        }
      },
      {
        $group: {
          _id: null,
          speed: {$avg: "$vehicleAnalytics.vehicleDetectionEventsByLane.speed"},
          timeHeadway: {$avg: "$vehicleAnalytics.vehicleDetectionEventsByLane.headway.timeHeadway"},
          spaceHeadway: {$avg: "$vehicleAnalytics.vehicleDetectionEventsByLane.headway.spaceHeadway"},
          volume: {$sum: 1}
        }
      },
      {$sort: {_id: 1}},
      {
        $project: {
          _id: 0,
          speed: "$speed",
          timeHeadway: "$timeHeadway",
          spaceHeadway: "$spaceHeadway",
          volume: "$volume"
        }
      }
    ],
    function (err, result) {
      var data;
      if (err) {
        defered.reject(err);
      }
      if (result.length > 0) {
        data = result[0];
        defered.resolve(data);
      } else {
        data = {
          speed: 0,
          timeHeadway: 0,
          spaceHeadway: 0,
          volume: 0
        }
        defered.resolve(data);

      }
    });

  return defered.promise;
}


  /*wholeZone related card data pusher(occupancy,levelOfService)*/
  function wholeZoneAnalyticsCardDataPublisher(publisherDetails) {
 TerminalZonesAndLaneDetails.findOne({"terminal.terminalId":
    publisherDetails.terminalSelected}, function (err, result) {
      if (err) {
        res.send(err)
      } else {
        var promisesWholeZone = [];
        var valueCountWholeZoneAnalytics = 0;
        var timeSelected = publisherDetails.aggregationPeriod;
        for (var l = 0; l < result.terminal.zoneDetails.length; l++) {
          var laneSelected = result.terminal.zoneDetails[l].laneIndex;
          var zoneSelected = result.terminal.zoneDetails[l].zoneId;
          var promiseWholeZoneAnalytics = aggregatingWholeZoneAnalyticsDataForCards(laneSelected, zoneSelected, timeSelected).then(function (data) {
            return Q(data);
          });
          promisesWholeZone.push(promiseWholeZoneAnalytics);
        }
        Q.all(promisesWholeZone).then(function (resData) {
          var avgOccupancy = 0;
          var maxLevelOfService = [];
          for (var j = 0; j < resData.length; j++) {
            if (resData[j].occupancy != 0) {
              valueCountWholeZoneAnalytics++;
            }
            avgOccupancy += resData[j].occupancy;
            maxLevelOfService.push(resData[j].levelOfService)
          }
          var data = {};
          var maxLOS=0;
          if(maxLevelOfService.length!=0){
            maxLOS=maxLevelOfService.max();
          }
          if (valueCountWholeZoneAnalytics === 0) {
            data.occupancy = avgOccupancy;
            data.levelOfService = maxLOS;
            data.levelServiceArray=maxLevelOfService;
          }
          else {
            data.occupancy = avgOccupancy / valueCountWholeZoneAnalytics;
            data.levelOfService = maxLOS;
            data.levelServiceArray=maxLevelOfService;
          }
          console.log("Emitting Whole Zone Data To "+publisherDetails.publisherName+" room")
          SocketIo.to(publisherDetails.publisherName).emit('wholeZoneCardData', data)
          /*SocketIo.to(connId).emit('getTerminalValue', data)*/
        });
      };
    })
  }
  function aggregatingWholeZoneAnalyticsDataForCards(laneSelected,
       zoneSelected, timeSelected) {
  var deferedWholeZone = Q.defer();

  var start = new Date();
  var end = new Date();
  start.setSeconds(start.getSeconds() - timeSelected);
  var lanesSelected = laneSelected;
  var zonesSelected = zoneSelected;
  wholeZoneTrafficInfoModel.aggregate(
    [
      /*matching through dateTime,laneIndex,zoneIndex*/
      {
        $match: {
          dateTime: {$gt: start, $lt: end},
          "vehicleAnalytics.cameraExtrinsics.cameraId": zoneSelected
        }
      },
      {$unwind: "$vehicleAnalytics.wholeZoneTrafficInformation"},
      {
        $match: {
          "vehicleAnalytics.wholeZoneTrafficInformation.laneIndex": {$in: laneSelected}
        }
      },
      {
        $sort: {
          "dateTime":-1
        }
      },
      {
        $group: {
          _id: null,
          levelOfService: {$last: "$vehicleAnalytics.wholeZoneTrafficInformation.levelofService"},
          occupancy: {$avg: "$vehicleAnalytics.wholeZoneTrafficInformation.zoneOccupancy"}
        }
      },
      {$sort: {_id: 1}},
      {$project: {_id: 0, levelOfService: "$levelOfService", occupancy: "$occupancy"}}
    ],

    function (err, result) {
      var data;
      if (err) {
        deferedWholeZone.reject(err);
      }
      if (result && result.length > 0) {
        data = result[0];
        deferedWholeZone.resolve(data);
      } else if(!result){
        data = {
          occupancy: 0,
          levelOfService: 0
        }
        deferedWholeZone.resolve(data);
      }else{
        data = {
          occupancy: 0,
          levelOfService: 0
        }
        deferedWholeZone.resolve(data);
      }
    });
  return deferedWholeZone.promise;


}
  Array.prototype.max = function() {
    return Math.max.apply(null, this);
  };

  function refreshAggregationCalculations() {
    /*console.log(publisherList)*/
  if (publisherList && publisherList.length>0) {
    for (var j = 0; j < publisherList.length; j++) {
      var publisherDetails = publisherList[j];
      vehicleAnalyticsCardDataPublisher(publisherDetails);
      wholeZoneAnalyticsCardDataPublisher(publisherDetails);
    }
  }
}
  emitTerminalData();
  function isPublisherAlreadyExist(array,publisherObjToMatch){
  for(var i=0;i<array.length;i++){
    var publisherObj = array[i];
    var publisherName =publisherObj.publisherName;
    if(publisherName===publisherObjToMatch.publisherName){
      array[i].noOfClients=array[i].noOfClients+1;
      publisherList = array;
      return true;
      break;
    }
    if(i==array.length-1){
      return false;
    }
  }
}
  module.exports={
  refreshAggregationCalculations:refreshAggregationCalculations
}

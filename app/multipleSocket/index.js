/**
 * Created by Suhas on 2/5/2016.
 */
var socketArray = [],
SocketIo = require('../../config/socketIo');
var setSocketArray = function(connectionObj){
  socketArray.push(connectionObj);
}
var getSocketArray = function(){
  return socketArray
}
var updateSocketArrayObj = function(connId,configDetails){
  var data = socketArray;
  if (data) {
    for (var i = 0; i < data.length; i++) {
      if (data[i].id == connId) {
        data[i].details.terminalDetails = configDetails.terminalDetails;
        data[i].details.realTimeGraphDetails.parameterSelected = configDetails.realTimeGraphDetails.parameterSelected;
        data[i].details.realTimeGraphDetails.zoneSelected = configDetails.realTimeGraphDetails.zoneSelected;
      }
      if(i==data.length-1){
        socketArray = data;
      }
    }
  }
}
var removeObjectFromSocketArray = function(connId){
  for(var i=0; i<socketArray.length; ++i ) {
    var p = socketArray[i];
    if(p.id == connId){
      socketArray.splice(i,1);
      break;
    }
  }
}
var getSocketObjByConnId = function(connId){
  for(var i=0; i<socketArray.length; ++i ) {
    var socketObj = socketArray[i];
    if(socketObj.id == connId){
      return socketObj;
    }
  }
}

module.exports={
  getSocketArrayDetails:getSocketArray,
  setSocketArrayDetails:setSocketArray,
  updateSocketArrayObj:updateSocketArrayObj,
  removeObjectFromSocketArray:removeObjectFromSocketArray,
  getSocketObjByConnId:getSocketObjByConnId
}

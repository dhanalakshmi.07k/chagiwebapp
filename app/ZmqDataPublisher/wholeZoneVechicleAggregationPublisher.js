/**
 * Created by zendynamix on 28-05-2016.
 */

var Q = require("q"),
  mongoose = require('mongoose'),
  VehicleAnalyticsModel = require("../models/vehicleAnalytics"),
  TerminalZonesAndLaneDetails = require("../models/terminalGenerator"),
  wholeZoneTrafficInfoModel = require('../models/wholeZoneTrafficInfo'),
  config = require('../../config/config'),
  pConfig = require('../../config/publishDataConfig'),
  publishDataConfig = pConfig.config;
  vehicleWholeZoneAnalyticsAggregate = require('../aggregators');

var dataPublishZmqSocket = pConfig.dataPublishZmqSocket;


var objArray = [];
var trafficMetricsObj = {};

var startPublish = function () {
  if (publishDataConfig.aggregateData.isEnable) {

    setInterval(function () {
      aggregationForAllTerminal(publishDataConfig);
    },publishDataConfig.aggregateData.aggregationInterval);
  } else {
    /*console.log(`publishDataConfig.aggregateData.isEnable= ${publishDataConfig.aggregateData.isEnable}`)*/
  }


}


function aggregationForAllTerminal(publishAggregationConfigDetails) {

  var promises = [];
  var valueCount = 0;

  for (var k = 0; k < publishAggregationConfigDetails.aggregateData.terminals.length; k++) {
    var configDetails = {};
    configDetails.terminalSelected = publishAggregationConfigDetails.aggregateData.terminals[k];
    configDetails.timeSelected = publishAggregationConfigDetails.aggregateData.aggregationSpan;
    var start = new Date();
    var end = new Date();
    start.setSeconds(start.getSeconds() - publishAggregationConfigDetails.aggregateData.aggregationSpan);
    configDetails.start = start;
    configDetails.end = end;

    getTerminalWholeZoneDetails(configDetails);

  }
}

function getTerminalWholeZoneDetails(configDetails) {
  TerminalZonesAndLaneDetails.findOne({"terminal.terminalId": configDetails.terminalSelected},
    function (err, result) {
      if (err) {
        res.send(err)
      } else {
        var promises = [];
        var valueCount = 0;
        timeSelected = configDetails.timeSelected;

        for (var k = 0; k < result.terminal.zoneDetails.length; k++) {
          var laneSelected = result.terminal.zoneDetails[k].laneIndex;
          var zoneSelected = result.terminal.zoneDetails[k].zoneId;

          var promise = vehicleWholeZoneAnalyticsAggregate.vehicleAnalytics.sendVehicleAnalyticsDetailsForCards(laneSelected, zoneSelected, timeSelected).then(function (data) {

            return Q(data);
          });

          promises.push(promise);
        }
        Q.all(promises).then(function (resData) {

          var avgSpeed = 0, avgTimeHeadway = 0, avgSpaceHeadway = 0, avgvolume = 0;
          for (var j = 0; j < resData.length; j++) {
            if (resData[j].speed != 0) {
              valueCount++;
            }
            avgSpeed += resData[j].speed;
            avgTimeHeadway += resData[j].timeHeadway;
            avgSpaceHeadway += resData[j].spaceHeadway;
            avgvolume += resData[j].volume;

          }

          var data = {}
          if (valueCount === 0) {
            data.speed = avgSpeed;
            data.timeHeadway = avgTimeHeadway;
            data.spaceHeadway = avgSpaceHeadway;
            data.volume = avgvolume;

          }
          else {
            data.speed = avgSpeed / valueCount;
            data.timeHeadway = avgTimeHeadway / valueCount;
            data.spaceHeadway = avgSpaceHeadway / valueCount;
            data.volume = avgvolume / valueCount;

          }
          //call whole zone data is data
          getWholeZoneDetails(configDetails, data)

        });

      }
    });
}
function getWholeZoneDetails(configDetails, resultantObj) {
  TerminalZonesAndLaneDetails.findOne({
    "terminal.terminalId": configDetails.terminalSelected
  }, function (err, result) {

    if (err) {
      res.send(err)
    } else {
      var promisesWholeZone = [];
      var valueCountWholeZoneAnalytics = 0;
      timeSelected = configDetails.timeSelected;

      for (var l = 0; l < result.terminal.zoneDetails.length; l++) {
        var laneSelected = result.terminal.zoneDetails[l].laneIndex;
        var zoneSelected = result.terminal.zoneDetails[l].zoneId;
        var promiseWholeZoneAnalytics = vehicleWholeZoneAnalyticsAggregate.wholeZoneAnalytics.getAverageDataOfWholeZoneAnalyticsForTerminal(laneSelected, zoneSelected, timeSelected).then(function (data) {

          return Q(data);
        });
        promisesWholeZone.push(promiseWholeZoneAnalytics);
      }
      Q.all(promisesWholeZone).then(function (resData) {

        var avgOccupancy = 0;
        var maxLevelOfService = [];
        for (var j = 0; j < resData.length; j++) {

          if (resData[j].occupancy != 0) {
            valueCountWholeZoneAnalytics++;
          }
          avgOccupancy += resData[j].occupancy;
          maxLevelOfService.push(resData[j].levelOfService)
        }

        var data = {};
        var maxLOS = 0;
        if (maxLevelOfService.length != 0) {
          maxLOS = maxLevelOfService.max();
        }
        if (valueCountWholeZoneAnalytics === 0) {
          data.occupancy = avgOccupancy;
          data.levelOfService = maxLOS;
          data.levelServiceArray = maxLevelOfService;
        }
        else {
          data.occupancy = avgOccupancy / valueCountWholeZoneAnalytics;
          data.levelOfService = maxLOS;
          data.levelServiceArray = maxLevelOfService;
        }
        var start = configDetails.start;
        var end = configDetails.end;

        var aggregationObj = {};
        aggregationObj.terminalId = configDetails.terminalSelected;
        aggregationObj.speed = resultantObj.speed;
        aggregationObj.timeHeadway = resultantObj.timeHeadway;
        aggregationObj.spaceHeadway = resultantObj.spaceHeadway;
        aggregationObj.volume = resultantObj.volume;
        aggregationObj.occupancy = data.occupancy;
        aggregationObj.levelOfService = data.levelOfService;
        aggregationObj.aggregationStartDate = start;
        aggregationObj.aggregationEndDate = end;
        finalAggregationObj(aggregationObj)


      });

    }
  });
}


function finalAggregationObj(aggregationObj) {
  objArray.push(aggregationObj);
  if (objArray.length === publishDataConfig.aggregateData.terminals.length) {
    trafficMetricsObj.trafficMetrics = objArray;
     //console.log(trafficMetricsObj)
    dataPublishZmqSocket.send([publishDataConfig.aggregateData.pubSubFilter, JSON.stringify(objArray)]);
    objArray = [];
  }

}
Array.prototype.max = function () {
  return Math.max.apply(null, this);
};

module.exports = {startPublish: startPublish};

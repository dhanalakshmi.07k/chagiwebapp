/**
 * Created by zendynamix on 01-06-2016.
 */
var publishDataConfig = require('../../config/publishDataConfig'),
  pConfig = require('../../config/publishDataConfig'),
  publishDataConfig = pConfig.config;

var dataPublishZmqSocket= pConfig.dataPublishZmqSocket;


var publishRawVehicleAnanalyticsData = function(data){

  if(publishDataConfig.rawData.isEnable){
    delete data.vehicleAnalytics.cameraIntrinsics;
    delete data.vehicleAnalytics.cameraExtrinsics;
    dataPublishZmqSocket.send( [publishDataConfig.rawData.pubSubFilter.vehicleAnalyticsRawData, JSON.stringify(data)]);
  }


}
module.exports = {
  publishRawVehicleAnanalyticsData:publishRawVehicleAnanalyticsData
}

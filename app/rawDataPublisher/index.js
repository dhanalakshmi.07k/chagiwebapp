/**
 * Created by zendynamix on 01-06-2016.
 */

var rawVehicleDataPublisher = require('./rawVehicleDataPublisher');
var rawWholeZoneDataPublisher=require('./rawWholeZoneDataPublisher')

module.exports = {
  rawVehicleDataPublisher: rawVehicleDataPublisher,
  rawWholeZoneDataPublisher:rawWholeZoneDataPublisher

}

/**
 * Created by zendynamix on 03-06-2016.
 */


var publishDataConfig = require('../../config/publishDataConfig'),
  pConfig = require('../../config/publishDataConfig'),
  publishDataConfig = pConfig.config;
var dataPublishZmqSocket= pConfig.dataPublishZmqSocket;
var rawWholeZoneAnalyticsClonedObject=require("../utility/objectCloneUtility");


var publishRawWholeZoneAnalyticsData = function(rawMessage){
  var data=rawWholeZoneAnalyticsClonedObject.getObjectClone(rawMessage);

  if(publishDataConfig.rawData.isEnable){
    delete data.vehicleAnalytics.cameraIntrinsics;
    delete data.vehicleAnalytics.cameraExtrinsics;
    /*console.log("*************")
    console.log(data)
    console.log("******publishRawWholeZoneAnalyticsData*******")*/

    dataPublishZmqSocket.send( [publishDataConfig.rawData.pubSubFilter.wholeZoneAnalyticsRawData, JSON.stringify(data)]);
  }


}
module.exports = {
  publishRawWholeZoneAnalyticsData:publishRawWholeZoneAnalyticsData
}

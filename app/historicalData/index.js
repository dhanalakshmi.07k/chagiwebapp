/**
 * Created by Suhas on 5/27/2016.
 */
var vehicleAggregate = require('./vehicleAnalytics.js');
var wholeZoneAggregate = require('./wholeZoneAnalytics.js');
module.exports={
  vehicleAggregate:vehicleAggregate,
  wholeZoneAggregate:wholeZoneAggregate
}

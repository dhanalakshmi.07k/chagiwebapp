/**
 * Created by Suhas on 5/27/2016.
 */

var VehicleAnalyticsModel = require("../models/vehicleAnalytics"),
    mongoose=require('mongoose');

var Q = require("q");
var getAggregatedHistoricalData = function(startDate,endDate,parameter,zoneId,lanesSelected,callBack){
  var avgData ='';
  if(parameter==="spaceHeadway" || parameter==="timeHeadway"){
    avgData ="$vehicleAnalytics.vehicleDetectionEventsByLane.headway."+parameter;
  }else{
    avgData ="$vehicleAnalytics.vehicleDetectionEventsByLane."+parameter;
  }
  VehicleAnalyticsModel.aggregate(
      // Pipeline
      [
        // Stage 1
        {
          $match: {
            "dateTime" : {
              "$gt" : startDate,
              "$lt" : endDate
            },
            "vehicleAnalytics.cameraExtrinsics.cameraId":zoneId
          }
        },

        // Stage 2
        {
          $unwind: "$vehicleAnalytics.vehicleDetectionEventsByLane"
        },

        // Stage 3
        {
          $match:  {
            "vehicleAnalytics.vehicleDetectionEventsByLane.laneIndex": {$in: lanesSelected}
          }
        },

        // Stage 4
        {
          $group: {
            "_id" : {
              "minute" : {
                "$minute" : "$dateTime"
              },
              "laneIndex": "$vehicleAnalytics.vehicleDetectionEventsByLane.laneIndex"
            },
            "occupancy" : {
              "$avg" : avgData
            },
          }
        },

        // Stage 5
        {
          $sort: {
            "_id.minute": 1
          }
        },

        // Stage 6
        {
          $group: {
            "_id" :"$_id.laneIndex",
            "occupancy":{
              "$push":{
                "occupancy":"$occupancy",
                "minutes":"$_id.minute"
              }
            }
          }
        },

        // Stage 7
        {
          $sort: {
            "_id": 1
          }
        }

      ],function(err,result){
      if(err){
        console.log(err.stack)
      }else if(result && result.length>0){
        callBack(result);
      }else if(result.length==0){
        callBack(result);
      }
    }
  );

}
var getAggregatedHistoricalDataOverSeconds = function(startDate,endDate,seriesSelected,parameter,zoneId,
                                                      lanesSelected,callBack){
  var promises = [];
  var aggregationStartDate = new Date(startDate);
  var aggregationEndDate = new Date(endDate);
  aggregationStartDate.setSeconds(aggregationStartDate.getSeconds()-seriesSelected)
  for(var seriesPoint=0;seriesPoint<60;seriesPoint++){
    if(seriesPoint==0){
      /*startDate.setSeconds(endDate.getSeconds()-seriesSelected)*/
    }else{
      aggregationEndDate.setSeconds(aggregationEndDate.getSeconds()-seriesSelected)
      aggregationStartDate.setSeconds(aggregationStartDate.getSeconds()-seriesSelected)
    }
    var promise =getAggregatedHistoricalDataOverRangeOfDate(aggregationStartDate,aggregationEndDate
      ,parameter,zoneId,lanesSelected).then(function (data) {
      var occupancyDetails = new Array(lanesSelected.length);
      occupancyDetails.fill(null)
      if(data && data.length>0){
        for(var i=0;i<data.length;i++){
          occupancyDetails[i]=data[i].occupancy;
          if(i==data.length-1){
            return Q(occupancyDetails);
          }
        }
      }else{
        return Q(occupancyDetails);
      }
    });
    promises.push(promise)
  }
  Q.all(promises).then(function (resData) {
    if(resData && resData.length>0){
      var aggregatedArray = new Array(lanesSelected.length);
      var emptyArray = [];
      aggregatedArray.fill(emptyArray)
      for(var j=0;j<resData.length;j++){
        var resObj = resData[j];
        if(resObj && resObj.length>0){
            for(var k=0;k<resObj.length;k++){
              if(aggregatedArray[k].length==0){
                aggregatedArray[k] = new Array(resData.length);
                aggregatedArray[k].fill(null)
              }
              if(resObj[k]!=null){
                aggregatedArray[k][j]=Math.round(resObj[k]);
              }else{
                aggregatedArray[k][j]=resObj[k];
              }

                if(k==resObj.length-1 && j==resData.length-1){
                  /*callBack(aggregatedArray)*/
                  var array = [];
                  var finalResultArray = aggregatedArray;
                  for(var f=0;f<finalResultArray.length;f++){
                    var laneObjectArray = finalResultArray[f].reverse();
                    array.push(laneObjectArray);
                    if(array.length==aggregatedArray.length){
                      callBack(array)
                    }
                  }
              }
            }
        }else{

        }
      }
    }
    /*callBack(resData)*/
  });

}

function getAggregatedHistoricalDataOverRangeOfDate(startDate,endDate,parameter,zoneId,lanesSelected){
  var defered = Q.defer();
  var avgData ='';
  var aggregationStartDate = new Date(startDate);
  var aggregationEndDate = new Date(endDate);
  if(parameter==="spaceHeadway" || parameter==="timeHeadway"){
    avgData ="$vehicleAnalytics.vehicleDetectionEventsByLane.headway."+parameter;
  }else{
    avgData ="$vehicleAnalytics.vehicleDetectionEventsByLane."+parameter;
  }
  VehicleAnalyticsModel.aggregate(
    // Pipeline
    [
      // Stage 1
      {
        $match: {
          "dateTime" : {
            "$gt" : aggregationStartDate,
            "$lt" : aggregationEndDate
          },
          "vehicleAnalytics.cameraExtrinsics.cameraId":zoneId
        }
      },

      // Stage 2
      {
        $unwind: "$vehicleAnalytics.vehicleDetectionEventsByLane"
      },

      // Stage 3
      {
        $match:  {
          "vehicleAnalytics.vehicleDetectionEventsByLane.laneIndex": {$in: lanesSelected}
        }
      },

      // Stage 4
      {
        $group: {
          "_id" : {
            "laneIndex": "$vehicleAnalytics.vehicleDetectionEventsByLane.laneIndex"
          },
          "occupancy" : {
            "$avg" : avgData
          },
        }
      },

      // Stage 5
      {
        $sort: {
          "_id.laneIndex": 1
        }
      },

    ],function(err,result){
      if(err){
        console.log(err.stack)
      }else if(result && result.length>0){
        defered.resolve(result);
      }else if(result.length==0){
        defered.resolve(result);
      }
    }
  );
  return defered.promise;
}
module.exports={
  getAggregatedHistoricalData:getAggregatedHistoricalData,
  getAggregatedHistoricalDataOverSeconds:getAggregatedHistoricalDataOverSeconds
}

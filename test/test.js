/*var zmq = require('zmq');*/
var express = require('express'),
  router = express.Router(),
  mongoose = require('mongoose'),
  Article = mongoose.model('Article'),
  Notify = mongoose.model('notify'),
  plotMap = mongoose.model('plotMap'),
  highChartVal = mongoose.model('highChartVal'),
  VehicleAnalyticsModel = mongoose.model('vehicleAnalytics'),
  WholeZoneTrafficInfoModel = mongoose.model('wholeZoneTrafficInfo');
var random = require('mongoose-simple-random');
Array.prototype.divide = function (arr) {
  var sum = [];
  if (arr != null && this.length == arr.length) {
    for (var i = 0; i < arr.length; i++) {
      sum.push(this[i]/arr[i]);
    }
  }
  return sum;
}
Array.prototype.AvgArray = function (length) {
  var avgArray = [];
  if (this.length != null) {
    for (var i = 0; i < this.length; i++) {
      this[i] = this[i] / length;
      avgArray.push(this[i]);
    }
  }
  return avgArray;
}
module.exports = function (app) {
  app.use('/', router);
};
/*
 io.on('connection', function(socket){
 socket.emit('an event', { some: 'data' });
 });
 */
router.get('/', function (req, res, next) {
  Article.find(function (err, articles) {
    if (err) return next(err);
    res.render('index', {
      title: 'Generator-Express MVC',
      articles: articles
    });
  });
});
/*var minutes = 1, the_interval = 60 * 1000;
 setInterval(function(req) {
 var io = req.io;
 Notify.find({},function(err,notification){
 io.emit('msg',{
 data:notification
 })
 })
 console.log("I am doing my 5 minutes check");
 // do your stuff here
 }, the_interval);*/
router.get('/newMsg', function (req, res, next) {
  var io = req.io;
  var msgId = [1,2,3,4,5,6,7,8];
  Notify.findOne({notifyId:msgId[Math.floor(Math.random()*msgId.length)]},function(err,notification){
    io.emit('msg',{
      //data:notification
      data:getMsg()
    })
    //console.log("--------------------------------------------");
    //console.log(notification);
  })

  res.send('ok');
});


var getMsg= function(){
  var noOfCar = randomInt(20,100)
  var lane = randomInt(1,4)
  var terminal = randomInt(1,3)
  var iconClass = noOfCar >50 ?"red":"yellow"

  var msg = {
    "message" : "Around "+noOfCar+" cars in Lane "+lane+" near T" +terminal+ " Exit",
    "iconTextPart2" : "Cars",
    "iconClass" : "symbol "+iconClass,
    "iconTextPart1" : noOfCar,
    "messageTime" : new Date()
  }
  return msg
}
function randomInt (low, high) {
  return Math.floor(Math.random() * (high - low) + low);
}
/*router.get('/newPlotMsg', function (req, res, next) {
 var io = req.io;
 io.emit('plotData',{
 "title": "point5",
 "lat": "1.353606",
 "lng": "103.986361",
 "description": "uuu"
 })
 res.send('success');
 });*/


router.get('/newMsg', function (req, res) {
  var io = req.io;
  var msgId = [1,2,3,4,5,6,7,8];
  Notify.findOne({notifyId:msgId[Math.floor(Math.random()*msgId.length)]},function(err,notification){
    io.emit('msg',{
      //data:notification
      data:getMsg()
    })
    //console.log("--------------------------------------------");
    //console.log(notification);
  })

  res.send('ok');
});

router.get('/newPlotMsg', function (req, res, next) {
  var io = req.io;
  var val = ['one','two','three','four'];
  var data = val[Math.floor(Math.random()*val.length)];
  var plot=[];
  if(data=="one"){
    plot = [0,1,2,3,4,5,6,7,8,9];
    generateRandomPlot(plot);
  }else if(data=="two"){
    /* plot = [7,8,9,10];*/
    plot = [10,11,12,13,14,15,16,17,18,19];
    generateRandomPlot(plot);
  }else if(data=="three"){
    /* plot = [11,12,13,14,15,16,17];*/
    plot = [20,21,22,23,24,25,26];
    generateRandomPlot(plot);
  }else if(data=="four"){
    plot = [27,28,29,30,31,32];
    generateRandomPlot(plot);
  }
  function generateRandomPlot(plotparameter){
    console.log("!!!!!!!!!!!!");
    console.log(plotparameter)
    plotMap.find({pointId:{ $in:plotparameter}},function(err,resplotData){
      io.emit('plotData',{
        data:resplotData
      })
    })
    res.send('data generated');
  }

});
router.get('/highChartVal/start/:start/end/:end', function (req, res) {/*{$gte: req.params.start, $lt: req.params.end}*//*{ $in: [1,2,3]}*/
  highChartVal.find({valId:{$gte: req.params.start, $lt: req.params.end}},function(err,highChartVal){
    res.send(highChartVal);
  })
});


// Find a single random document of vehicleAnalytics
router.get('/randomAnalytics', function (req, res) {
  var io = req.io;
  VehicleAnalyticsModel.findOneRandom(function(err,result){
    var VehicleAnalyticsArray = result.vehicleAnalytics.vehicleDetectionEventsByLane;
    for(var i=0;i<VehicleAnalyticsArray.length;i++){
      VehicleAnalyticsArray[i].timeStamp = new Date();
      if(i==VehicleAnalyticsArray.length-1){
        result.vehicleAnalytics.vehicleDetectionEventsByLane = VehicleAnalyticsArray;
        var updatedObj = result;
        saveData(updatedObj);
        res.send(updatedObj);
        /* io = req.io;*/
      }
    }
    /* require('../../config/express')(app,config,io);
     require('../../config/zmqPushPull').zmqPushPull(config,io);*/
    /*var payLoad = {
     data:result,
     timeStamp:new Date()
     }
     io.emit('analyticsData',{
     //data:notification
     data:payLoad
     })*/
  })
})



/* function to save datat to db*/
function saveData(message) {
  var vehicleAnalyticsObj = VehicleAnalyticsModel();
  vehicleAnalyticsObj.vehicleAnalytics = message.vehicleAnalytics;
  vehicleAnalyticsObj.dateTime = new Date();
  vehicleAnalyticsObj.save(function(err,result){
    if(err){
      console.log(err);
    }
    /*res.send(result)*/
    /* console.log(result);*/
  })
}
/*get road analytics by date range*/
router.post('/roadAnalyticsByDate', function (req, res) {
  var start = new Date(req.body.start);
  var end = new Date(req.body.end);
  start.setHours(0);
  end.setHours(23);
  VehicleAnalyticsModel.find({dateTime: {$gt:start,$lt:end}}, function (err, result) {
    if (err) {
      res.send(err);
    }
    else{
      var vehicleAnalyticsforLane1 = [];
      var vehicleAnalyticsforLane2 = [];
      var vehicleAnalyticsforLane3 = [];
      var vehicleAnalyticsforLane4 = [];
      var vehicleAnalyticsforLane5 = [];
      var laneNo1 = 0;
      var laneNo2 = 0;
      var laneNo3 = 0;
      var laneNo4 = 0;
      var laneNo5 = 0;
      var laneHourCount = {
        lane1:[],
        lane2:[],
        lane3:[],
        lane4:[],
        lane5:[]
      }
      if (result.length>0) {
        var analyticsForDateRange = result;
        for (var i = 0; i < analyticsForDateRange.length; i++) {
          var vehicleDetectedArray = analyticsForDateRange[i].vehicleAnalytics.vehicleDetectionEventsByLane;
          var hour = analyticsForDateRange[i].dateTime.getHours();
          for (var j = 0; j < vehicleDetectedArray.length; j++) {
            if (vehicleDetectedArray[j].laneIndex == 1) {
              var previousSpeed;
              laneNo1++;
              var previouslaneCount= 0;
              if(laneHourCount.lane1[hour]){
                laneHourCount.lane1[hour] = laneHourCount.lane1[hour]+1;
              }else{
                laneHourCount.lane1[hour] = 1;
              }
              if (vehicleAnalyticsforLane1[hour]) {
                previousSpeed = vehicleAnalyticsforLane1[hour];
              } else {
                previousSpeed = 0;
              }
              vehicleAnalyticsforLane1[hour] = previousSpeed + vehicleDetectedArray[j].Speed;
            }
            else if (vehicleDetectedArray[j].laneIndex == 2) {
              var previousSpeed;
              laneNo2++;
              var previouslaneCount= 0;
              if(laneHourCount.lane2[hour]){
                laneHourCount.lane2[hour] = laneHourCount.lane2[hour]+1;
              }else{
                laneHourCount.lane2[hour] = 1;
              }
              if (vehicleAnalyticsforLane2[hour]) {
                previousSpeed = vehicleAnalyticsforLane2[hour];
              } else {
                previousSpeed = 0;
              }
              vehicleAnalyticsforLane2[hour] = previousSpeed + vehicleDetectedArray[j].Speed;
            }
            else if (vehicleDetectedArray[j].laneIndex == 3) {
              var previousSpeed
              laneNo3++;
              var previouslaneCount= 0;
              if(laneHourCount.lane3[hour]){
                laneHourCount.lane3[hour] = laneHourCount.lane3[hour]+1;
              }else{
                laneHourCount.lane3[hour] = 1;
              }
              if (vehicleAnalyticsforLane3[hour]) {
                previousSpeed = vehicleAnalyticsforLane3[hour];
              } else {
                previousSpeed = 0;
              }
              vehicleAnalyticsforLane3[hour] = previousSpeed + vehicleDetectedArray[j].Speed;
            }
            else if (vehicleDetectedArray[j].laneIndex == 4) {
              var previousSpeed
              laneNo4++;
              var previouslaneCount= 0;
              if(laneHourCount.lane4[hour]){
                laneHourCount.lane4[hour] = laneHourCount.lane4[hour]+1;
              }else{
                laneHourCount.lane4[hour] = 1;
              }
              if (vehicleAnalyticsforLane4[hour]) {
                previousSpeed = vehicleAnalyticsforLane4[hour];
              } else {
                previousSpeed = 0;
              }
              vehicleAnalyticsforLane4[hour] = previousSpeed + vehicleDetectedArray[j].Speed;
            }
            else if (vehicleDetectedArray[j].laneIndex == 5) {
              var previousSpeed
              laneNo5++;
              var previouslaneCount= 0;
              if(laneHourCount.lane5[hour]){
                laneHourCount.lane5[hour] = laneHourCount.lane5[hour]+1;
              }else{
                laneHourCount.lane5[hour] = 1;
              }
              var hour = parseInt(vehicleDetectedArray[j].timeStamp) % 24;
              if (vehicleAnalyticsforLane5[hour]) {
                previousSpeed = vehicleAnalyticsforLane5[hour];
              } else {
                previousSpeed = 0;
              }

              vehicleAnalyticsforLane5[hour] = previousSpeed + vehicleDetectedArray[j].Speed;
            }
            if (i == analyticsForDateRange.length - 1 && j == vehicleDetectedArray.length - 1) {
              var speedAnalyticsObject = {
                lane1: vehicleAnalyticsforLane1.divide(laneHourCount.lane1),
                lane2: vehicleAnalyticsforLane2.divide(laneHourCount.lane2),
                lane3: vehicleAnalyticsforLane3.divide(laneHourCount.lane3),
                lane4: vehicleAnalyticsforLane4.divide(laneHourCount.lane4),
                lane5: vehicleAnalyticsforLane5.divide(laneHourCount.lane5)
              }
              res.send(speedAnalyticsObject);
              /* console.log(i+"((((((((((("+j+"))))))))))"+vehicleDetectedArray.length);*/
            }
          }
        }
      } else {
        res.send(req.body)
      }
    }
  })
})
router.get('/roadAnalyticsByAverageSpeed/:seconds', function (req, res) {
  var endSeconds = req.params.seconds;
  var start = new Date();
  var startTimeSeconds = start.getSeconds();
  var end = new Date();
  end.setSeconds(startTimeSeconds-endSeconds);
  VehicleAnalyticsModel.find({dateTime:{$gt : end, $lt : start}},function(err,result){
    var io = req.io;
    if(result.length>0){
      if(err){
        res.send(err);
      }else{
        var vehicleAnalyticsforLane1 = 0;
        var vehicleAnalyticsforLane2 = 0;
        var vehicleAnalyticsforLane3 = 0;
        var vehicleAnalyticsforLane4 = 0;
        var vehicleAnalyticsforLane5 = 0;
        var laneNo1 = 0;
        var laneNo2 = 0;
        var laneNo3 = 0;
        var laneNo4 = 0;
        var laneNo5 = 0;
        var analyticsForDateRange = result;
        for (var i = 0; i < analyticsForDateRange.length; i++) {
          var vehicleDetectedArray = analyticsForDateRange[i].vehicleAnalytics.vehicleDetectionEventsByLane;
          for (var j = 0; j <vehicleDetectedArray.length; j++) {
            if (vehicleDetectedArray[j].laneIndex == 1) {
              laneNo1++;
              vehicleAnalyticsforLane1 = vehicleAnalyticsforLane1 + vehicleDetectedArray[j].Speed;
            }
            else if (vehicleDetectedArray[j].laneIndex == 2) {
              laneNo2++;
              vehicleAnalyticsforLane2 = vehicleAnalyticsforLane2 + vehicleDetectedArray[j].Speed;
            }
            else if (vehicleDetectedArray[j].laneIndex == 3) {
              laneNo3++;
              vehicleAnalyticsforLane3 = vehicleAnalyticsforLane3 + vehicleDetectedArray[j].Speed;
            }
            else if (vehicleDetectedArray[j].laneIndex == 4) {
              laneNo4++;
              vehicleAnalyticsforLane4 = vehicleAnalyticsforLane4 + vehicleDetectedArray[j].Speed;
            }
            else if (vehicleDetectedArray[j].laneIndex == 5) {
              laneNo5++;
              vehicleAnalyticsforLane5 = vehicleAnalyticsforLane5 + vehicleDetectedArray[j].Speed;
            }
            if (i == analyticsForDateRange.length-1 && j == vehicleDetectedArray.length-1) {
              var speedAnalyticsObject = {
                lane1: vehicleAnalyticsforLane1/laneNo1,
                lane2: vehicleAnalyticsforLane2/laneNo2,
                lane3: vehicleAnalyticsforLane3/laneNo3,
                lane4: vehicleAnalyticsforLane4/laneNo4,
                lane5: vehicleAnalyticsforLane5/laneNo5
              }
              var payLoad = {
                data:speedAnalyticsObject,
                timeStamp:new Date()
              }
              io.emit('analyticsData',{
                data:payLoad
              })
              res.send("msg Sent");
            }
          }
        }
      }
    }else{
      res.send("no data")
    }
  })
})
/*router.get('/roadAnalyticsByDate/:minutes', function (req, res) {
 var start = new Date();
 var end = req.params.end;
 VehicleAnalyticsModel.find({dateTime:{$gt : start, $lt : end}},function(err,result){
 if(err)
 res.send(err);
 res.send(result);
 console.log("Getting analytics data for date Range from"+start+" to "+end);
 })
 })*/

/*
 // Find a single random document of wholeZoneAnalytics
 router.get('/randomWholeZoneAnalytics', function (req, res) {
 VehicleAnalyticsModel.findOneRandom(function(err,result){
 var VehicleAnalyticsArray = result.vehicleAnalytics.vehicleDetectionEventsByLane;
 console.log(VehicleAnalyticsArray);
 for(var i=0;i<VehicleAnalyticsArray.length;i++){
 VehicleAnalyticsArray[i].timeStamp = new Date();
 if(i==VehicleAnalyticsArray.length-1){
 result.vehicleAnalytics.vehicleDetectionEventsByLane = VehicleAnalyticsArray;
 var updatedObj = result;
 res.send(updatedObj);
 }
 }
 })
 })*/
/*get road analytics by date*/
router.post('/wholeZoneTrafficInfoByDate', function (req, res) {
  var start = new Date(req.body.start);
  var end = new Date(req.body.end);
  start.setHours(23);
  end.setHours(0);
  end.setMonth(11);
  WholeZoneTrafficInfoModel.find({dateTime:{$gt:end,$lt:start}},function(err,result){
    if(err){
      res.send(err);
    }
    else{
      var wholeAnalyticsSpeedData = {
        speed:{
          zone1 :[],
          zone2 :[],
          zone3 :[]
        },
        occupancy:{
          zone1 :[],
          zone2 :[],
          zone3 :[]
        }
      }
      var wholeAnalyticsOccupacyData = {
        speed:{
          zone1 :[],
          zone2 :[],
          zone3 :[]
        },
        occupancy:{
          zone1 :[],
          zone2 :[],
          zone3 :[]
        }
      }
      var wholeAnalyticsDataCount= {
        speed:{
          zone1 :[],
          zone2 :[],
          zone3 :[]
        },
        occupancy:{
          zone1 :[],
          zone2 :[],
          zone3 :[]
        }

      }
      if (result.length>0) {
        var wholeZoneAnalyticsArray = result;
        for (var i = 0;i<wholeZoneAnalyticsArray.length;i++) {
          var zone = wholeZoneAnalyticsArray[i].vehicleAnalytics.cameraExtrinsics.cameraId;
          var wholeZoneDetectedArray = wholeZoneAnalyticsArray[i].vehicleAnalytics.wholeZoneTrafficInformation;
          var hour = new Date(wholeZoneAnalyticsArray[i].dateTime).getHours();
          for (var j = 0;j<wholeZoneDetectedArray.length;j++) {
            if(zone==1){
              var previousZoneSpeedVal=0,previousCount= 0,previousOccuZoneVal=0,previousOccCount=0;
              if(wholeAnalyticsDataCount.speed.zone1[hour]){
                previousCount=wholeAnalyticsDataCount.speed.zone1[hour]
              }
              if(wholeAnalyticsDataCount.occupancy.zone1[hour]){
                previousOccCount=wholeAnalyticsDataCount.occupancy.zone1[hour]
              }
              if(wholeAnalyticsSpeedData.speed.zone1[hour]){
                previousZoneSpeedVal = wholeAnalyticsSpeedData.speed.zone1[hour]
              }
              if(wholeAnalyticsSpeedData.occupancy.zone1[hour]){
                previousOccuZoneVal = wholeAnalyticsSpeedData.occupancy.zone1[hour]
              }
              wholeAnalyticsDataCount.speed.zone1[hour]=previousCount+1;
              wholeAnalyticsDataCount.occupancy.zone1[hour]=previousOccCount+1;
              wholeAnalyticsSpeedData.speed.zone1[hour] = previousZoneSpeedVal + wholeZoneDetectedArray[j].zoneSpeed;
              wholeAnalyticsSpeedData.occupancy.zone1[hour] = previousOccuZoneVal + wholeZoneDetectedArray[j].zoneOccupancy;
            }else if(zone==2){
              var previousZoneSpeedVal=0,previousCount= 0,previousOccuZoneVal=0,previousOccCount=0;
              if(wholeAnalyticsDataCount.speed.zone2[hour]){
                previousCount=wholeAnalyticsDataCount.speed.zone2[hour]
              }
              if(wholeAnalyticsDataCount.occupancy.zone2[hour]){
                previousOccCount=wholeAnalyticsDataCount.occupancy.zone2[hour]
              }
              if(wholeAnalyticsSpeedData.speed.zone1[hour]){
                previousZoneSpeedVal = wholeAnalyticsSpeedData.speed.zone2[hour]
              }
              if(wholeAnalyticsSpeedData.occupancy.zone1[hour]){
                previousOccuZoneVal = wholeAnalyticsSpeedData.occupancy.zone2[hour]
              }
              wholeAnalyticsDataCount.speed.zone2[hour]=previousCount+1;
              wholeAnalyticsDataCount.occupancy.zone2[hour]=previousOccCount+1;
              wholeAnalyticsSpeedData.speed.zone2[hour] = previousZoneSpeedVal + wholeZoneDetectedArray[j].zoneSpeed;
              wholeAnalyticsSpeedData.occupancy.zone2[hour] = previousOccuZoneVal + wholeZoneDetectedArray[j].zoneOccupancy;
            }else if(zone==3){
              var previousZoneSpeedVal=0,previousCount= 0,previousOccuZoneVal=0,previousOccCount=0;
              if(wholeAnalyticsDataCount.speed.zone3[hour]){
                previousCount=wholeAnalyticsDataCount.speed.zone3[hour]
              }
              if(wholeAnalyticsDataCount.occupancy.zone3[hour]){
                previousOccCount=wholeAnalyticsDataCount.occupancy.zone3[hour]
              }
              if(wholeAnalyticsSpeedData.speed.zone3[hour]){
                previousZoneSpeedVal = wholeAnalyticsSpeedData.speed.zone3[hour]
              }
              if(wholeAnalyticsSpeedData.occupancy.zone3[hour]){
                previousOccuZoneVal = wholeAnalyticsSpeedData.occupancy.zone3[hour]
              }
              wholeAnalyticsDataCount.speed.zone3[hour]=previousCount+1;
              wholeAnalyticsDataCount.occupancy.zone3[hour]=previousOccCount+1;
              wholeAnalyticsSpeedData.speed.zone3[hour] = previousZoneSpeedVal + wholeZoneDetectedArray[j].zoneSpeed;
              wholeAnalyticsSpeedData.occupancy.zone3[hour] = previousOccuZoneVal + wholeZoneDetectedArray[j].zoneOccupancy;
            }
            if (i == wholeZoneAnalyticsArray.length - 1 && j == wholeZoneDetectedArray.length - 1) {
              function createData(callBack){
                var wholeZoneSpeedAnalyticsObject = {
                  zone1: wholeAnalyticsSpeedData.speed.zone1.divide(wholeAnalyticsDataCount.speed.zone1),
                  zone2: wholeAnalyticsSpeedData.speed.zone2.divide(wholeAnalyticsDataCount.speed.zone2),
                  zone3: wholeAnalyticsSpeedData.speed.zone3.divide(wholeAnalyticsDataCount.speed.zone3)
                }
                var wholeZoneOccupancyAnalyticsObject = {
                  zone1: wholeAnalyticsSpeedData.occupancy.zone1.divide(wholeAnalyticsDataCount.occupancy.zone1),
                  zone2: wholeAnalyticsSpeedData.occupancy.zone2.divide(wholeAnalyticsDataCount.occupancy.zone2),
                  zone3: wholeAnalyticsSpeedData.occupancy.zone3.divide(wholeAnalyticsDataCount.occupancy.zone3)
                }
                var data = {
                  speed:wholeZoneSpeedAnalyticsObject,
                  occupancy:wholeZoneOccupancyAnalyticsObject
                }
                sendResponse(data);
              }
              createData(sendResponse);
              function sendResponse(data){
                res.send(data);
              }

            }
          }
        }
      } else {
        res.send("no data")
      }
    }
  })
})

router.get('/wholeZoneTrafficInfoByDate', function (req, res){
  var start = new Date();
  start.setMonth(11);
  var end = new Date();
  start.setHours(23);
  end.setHours(0);
  WholeZoneTrafficInfoModel.find({dateTime:{$gt:end,$lt:start}},function(err,result){
    res.send(result)
  })
})
/**
 * Created by Suhas on 12/27/2015.
 */
